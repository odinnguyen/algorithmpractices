﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.LinkedList
{
    public static class LinkedListHelpers
    {
        /// <summary>
        /// Convert a list into linked list
        /// </summary>
        /// <typeparam name="T">item value</typeparam>
        /// <param name="list">list</param>
        /// <returns>linked list head</returns>
        public static ListNode<T> ConvertToLinkedList<T>(this IList<T> list, bool isCircular = false)
        {
            return ConvertToLinkedList<T, ListNode<T>>(list, isCircular);
        }

        /// <summary>
        /// Convert a list into linked list
        /// </summary>
        /// <typeparam name="T">item value</typeparam>
        /// <typeparam name="K">List Node sub-class</typeparam>
        /// <param name="list">list</param>
        /// <param name="isCircular">indicate if this is circular list</param>
        /// <returns>linked list head</returns>
        public static K ConvertToLinkedList<T, K>(this IList<T> list, bool isCircular = false) where K: ListNode<T>, new()
        {
            if (list.Count == 0)
                return null;

            var head = new K(); 
            head.Value = list[0];

            var curr = head;
            for(int i = 1; i < list.Count; i++)
            {
                var next = new K();
                curr.Next = next;
                curr.Next.Value = list[i];
                curr = next;
            }

            if (isCircular)
                curr.Next = head;

            return head;
        }

        /// <summary>
        /// Convert List Node into enumerable of values
        /// </summary>
        /// <typeparam name="T">value type</typeparam>
        /// <param name="node">linked list node</param>
        /// <returns>enumerable of values</returns>
        public static IEnumerable<T> ToEnumerable<T>(this ListNode<T> node)
        {
            return node.ToEnumerable<T, ListNode<T>>();
        }

        /// <summary>
        /// Convert List Node into enumerable of values
        /// </summary>
        /// <typeparam name="T">value type</typeparam>
        /// <typeparam name="K">linked list node type</typeparam>
        /// <param name="head">linked list node</param>
        /// <returns>enumerable of values</returns>
        public static IEnumerable<T> ToEnumerable<T, K> (this K head) where K: ListNode<T>
        {
            if (head == null)
                yield break;

            var curr = head;
            do
            {
                yield return curr.Value;
                curr = curr.Next as K;
            } while (curr != head && curr != null);
        }

        /// <summary>
        /// Merged two sorted linked list into a sorted linked list
        /// </summary>
        /// <typeparam name="T">Value type</typeparam>
        /// <param name="head1">increasing sorted list 1</param>
        /// <param name="head2">increasing sorted list 2</param>
        /// <returns>increasing sorted list</returns>
        public static ListNode<T> MergeTwoSorted<T>(ListNode<T> head1, ListNode<T> head2) where T: IComparable
        {
            if (head1 == null && head2 == null) return null;
            if (head1 == null) return head2;
            if (head2 == null) return head1;

            if (head2.Value.CompareTo(head1.Value) > 0)
            {
                head1.Next = MergeTwoSorted(head1.Next, head2);
                return head1;
            }
            else
            {
                head2.Next = MergeTwoSorted(head1, head2.Next);
                return head2;
            }
        }

        /// <summary>
        /// Merged two sorted linked list into a sorted linked list
        /// </summary>
        /// <typeparam name="T">Value type</typeparam>
        /// <param name="head1">increasing sorted list 1</param>
        /// <param name="head2">increasing sorted list 2</param>
        /// <returns>increasing sorted list</returns>
        public static K MergeTwoSorted<T, K>(K head1, K head2)
            where K: ListNode<T>
            where T : IComparable
        {
            if (head1 == null && head2 == null) return null;
            if (head1 == null) return head2;
            if (head2 == null) return head1;

            if (head2.Value.CompareTo(head1.Value) > 0)
            {
                head1.Next = MergeTwoSorted(head1.Next, head2);
                return head1;
            }
            else
            {
                head2.Next = MergeTwoSorted(head1, head2.Next);
                return head2;
            }
        }
    }
}
