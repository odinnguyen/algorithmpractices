﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.LinkedList
{
    public class ListNode<T>
    {
        public T Value;
        public ListNode<T> Next;

        public ListNode()
        {
        }

        public ListNode(T val, ListNode<T> next = null)
            : this()
        {
            this.Value = val;
            this.Next = next;
        }
    }
}
