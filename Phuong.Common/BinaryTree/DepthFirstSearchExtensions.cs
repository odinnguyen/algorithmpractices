﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.BinaryTree
{
    public static class DepthFirstSearchExtensions
    {
        /// <summary>
        /// Return list of nodes' data in inorder traversal
        /// </summary>
        /// <returns>nodes</returns>
        public static IEnumerable<TreeNode<T>> InorderTraversal<T>(this TreeNode<T> node)
        {
            if (node.Left != null)
                foreach (var data in InorderTraversal<T>(node.Left))
                    yield return data;
            
            yield return node;

            if (node.Right != null)
                foreach (var data in InorderTraversal<T>(node.Right))
                    yield return data;
        }

        /// <summary>
        /// Return list of nodes' data in preorder traversal
        /// </summary>
        /// <returns>nodes</returns>
        public static IEnumerable<TreeNode<T>> preorderTraversal<T>(this TreeNode<T> node)
        {
            yield return node;

            if (node.Left != null)
                foreach (var data in preorderTraversal<T>(node.Left))
                    yield return data;

            if (node.Right != null)
                foreach (var data in preorderTraversal<T>(node.Right))
                    yield return data;
        }

        /// <summary>
        /// Return list of nodes' data in postorder traversal
        /// </summary>
        /// <returns>nodes</returns>
        public static IEnumerable<TreeNode<T>> postorderTraversal<T>(this TreeNode<T> node)
        {
            if (node.Left != null)
                foreach (var data in postorderTraversal<T>(node.Left))
                    yield return data;

            if (node.Right != null)
                foreach (var data in postorderTraversal<T>(node.Right))
                    yield return data;

            yield return node;
        }
    }
}
