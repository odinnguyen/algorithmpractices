﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.BinaryTree
{
    using Phuong.Common.Helpers;

    public static class BreadthFirstSearchExtensions
    {
        /// <summary>
        /// Build Binary Tree using Breadth first Search
        /// </summary>
        /// <typeparam name="T">tree node data type</typeparam>
        /// <param name="root">root node</param>
        /// <param name="indexes">
        /// List of (left, right) values in breadth first search order. -1 to denote emmpty branch.
        /// Example:  (-1, 7): node have empty left branch
        /// </param>
        /// <returns></returns>
        public static void BuildBinaryTree<T>(this TreeNode<T> root, Queue<(T Left, T Right)> indexes)
        {
            var queue = new Queue<TreeNode<T>>();
            queue.Enqueue(root);

            while (queue.Count > 0 && indexes.Count > 0)
            {
                var node = queue.Dequeue();
                var index = indexes.Dequeue();

                if (!index.Left.Equals(-1))
                {
                    node.Left = new TreeNode<T>(index.Left);
                    queue.Enqueue(node.Left);
                }

                if (!index.Right.Equals(-1))
                {
                    node.Right = new TreeNode<T>(index.Right);
                    queue.Enqueue(node.Right);
                }
            }
        }

        /// <summary>
        /// Return list of nodes' data in breadth first search order
        /// </summary>
        /// <returns>tree's data</returns>
        public static IEnumerable<TreeNode<T>> BreadthFirstSearch<T>(this TreeNode<T> root)
        {
            var queue = new Queue<TreeNode<T>>();
            queue.Enqueue(root);

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                yield return node;

                if (node.Left != null)
                    queue.Enqueue(node.Left);

                if (node.Right != null)
                    queue.Enqueue(node.Right);
            }
        }

        /// <summary>
        /// Serialize a binary tree into string using Breadth First Search
        /// Empty node will be represented by "null"
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="root">root node</param>
        /// <returns>serialized string</returns>
        public static string Serialize<T>(this TreeNode<T> root) where T : struct
        {
            if (root == null)
                return string.Empty;

            var list = new List<T?>();

            var queue = new Queue<TreeNode<T>>();
            queue.Enqueue(root);
            list.Add(root.Data);

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();

                if (node.Left != null)
                {
                    queue.Enqueue(node.Left);
                    list.Add(node.Left.Data);
                }
                else
                    list.Add(null);

                if (node.Right != null)
                {
                    queue.Enqueue(node.Right);
                    list.Add(node.Right.Data);
                }
                else
                    list.Add(null);
            }

            var indexEnd = list.Count - 1;
            for (; indexEnd >= 0; indexEnd--)
                if (list[indexEnd].HasValue)
                    break;


            var serializedString = string.Empty;
            for (var i = 0; i <= indexEnd; i++)
                serializedString += $"{list[i]} ";

            return serializedString.TrimEnd(' ');
        }
    }
}
