﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.BinaryTree
{
    public class TreeNode<T>
    {
        /// <summary>
        /// Id represent this node
        /// </summary>
        public Guid Id { get; private set; } = Guid.NewGuid();

        /// <summary>
        /// Parent Node
        /// </summary>
        public TreeNode<T> Parent { get; protected set; }

        /// <summary>
        /// Private property for left node
        /// </summary>
        private TreeNode<T> _Left { get; set; }

        /// <summary>
        /// Left child node
        /// </summary>
        public TreeNode<T> Left 
        { 
            get
            {
                return _Left;
            }
            set
            {
                _Left = value;
                if (_Left != null)
                    _Left.Parent = this;
            }
        }

        /// <summary>
        /// Private property for right node
        /// </summary>
        private TreeNode<T> _Right { get; set; }

        /// <summary>
        /// Right child node
        /// </summary>
        public TreeNode<T> Right
        {
            get
            {
                return _Right;
            }
            set
            {
                _Right = value;
                if (_Right != null)
                    _Right.Parent = this;
            }
        }

        /// <summary>
        /// Node Value
        /// </summary>
        public T Data { get; protected set; }

        /// <summary>
        /// Construct a TreeNode
        /// </summary>
        /// <param name="data">node value</param>
        public TreeNode([DisallowNull] T data)
        {
            Data = data;
        }

        /// <summary>
        /// Construct a TreeNode
        /// </summary>
        /// <param name="data">node value</param>
        public TreeNode([DisallowNull] T data, TreeNode<T> left = null, TreeNode<T> right = null)
            : this(data)
        {
            this.Left = left;
            this.Right = right;
        }

        /// <summary>
        /// Swap Left and Right node
        /// </summary>
        public void Swap()
        {
            var temp = _Left;
            _Left = _Right;
            _Right = temp;
        }

        /// <summary>
        /// Get depth of this node
        /// Note: Depth of root node is 1
        /// </summary>
        /// <returns>A node depth</returns>
        public int GetDepth()
        {
            if (this.Parent == null)
                return 1;
            else 
                return this.Parent.GetDepth() + 1;
        }

        /// <summary>
        /// Check if this is leaf node
        /// </summary>
        /// <returns></returns>
        public bool IsLeafNode()
        {
            return _Left == null && _Right == null;
        }

        /// <summary>
        /// Get string represent this node
        /// </summary>
        /// <returns>String represent this node</returns>
        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
