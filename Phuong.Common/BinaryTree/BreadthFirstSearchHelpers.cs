﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.BinaryTree
{
    using Phuong.Common.Helpers;

    public static class BreadthFirstSearchHelpers
    {
        /// <summary>
        /// Build binary tree base from Breadth first search array. 
        /// Empty node has value 'null'
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="list">breadth first search list</param>
        /// <returns>root node</returns>
        public static TreeNode<T> BuildBinaryTree<T>(List<T?> list) where T : struct
        {
            if (!list.Any())
                return null;

            if (!list.First().HasValue)
                return null;

            var root = new TreeNode<T>(list.Dequeue().Value);
            var queue = new Queue<TreeNode<T>>();
            queue.Enqueue(root);
            while (list.Any())
            {
                var node = queue.Dequeue();

                //Left child
                if (list.First().HasValue)
                {
                    node.Left = new TreeNode<T>(list.First().Value);
                    queue.Enqueue(node.Left);
                }
                list.Dequeue();

                //right child
                if (list.Any())
                {
                    if (list.First().HasValue)
                    {
                        node.Right = new TreeNode<T>(list.First().Value);
                        queue.Enqueue(node.Right);
                    }
                    list.Dequeue();
                }
            }

            return root;
        }
    }
}
