﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Phuong.Common.BinaryTree
{
    public class TreeNodeComparer<T> : IEqualityComparer<TreeNode<T>>
    {
        /// <summary>
        /// Compare two node
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Equals(TreeNode<T> x, TreeNode<T> y)
        {
            return x.Id.Equals(y.Id);
        }

        public int GetHashCode([DisallowNull] TreeNode<T> node)
        {
            return node.Id.GetHashCode();
        }
    }
}
