﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Helpers
{
    public class PrimeHelper
    {
        public static IEnumerable<int> Primes
        {
            get
            {
                yield return 2;
                yield return 3;
                int? n = 3;
                while (n < int.MaxValue)
                {
                    n = GetNextPrime(n.Value);
                    if (!n.HasValue)
                        yield break;
                    else
                        yield return n.Value;
                }
            }
        }

        public static int? GetNextPrime(int number)
        {
            if (number < 2)
                return 2;

            if (number == 2)
                return 3;

            var start = number % 2 == 0 ? number + 1 : number + 2;
            for (var i = start; i < Int32.MaxValue; i++)
                if (i.IsPrime())
                    return i;

            return null;
        }
    }
}
