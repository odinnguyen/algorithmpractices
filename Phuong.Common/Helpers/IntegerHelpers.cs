﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Helpers
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Return all prime factor(s), includes duplicates
        /// </summary>
        /// <param name="number">the subject number</param>
        /// <returns>prime factors with duplications</returns>
        public static IEnumerable<int> GetPrimeFactors(this long number)
        {
            //Get '2' factor(s)
            while (number % 2 == 0)
            {
                number /= 2;
                yield return 2;
            }

            //Get other prime factor(s)
            //Since all primes are odd number except for '2', we can safely increase counter by 2 each time
            var divisiveFactor = 3;
            while (number > 1)
            {
                while (number % divisiveFactor == 0)
                {
                    number /= divisiveFactor;
                    yield return divisiveFactor;
                }
                divisiveFactor += 2;
            }
        }

        /// <summary>
        /// Check if a number is prime
        /// </summary>
        /// <param name="number">subject number</param>
        /// <returns>Is prime?</returns>
        public static bool IsPrime(this int number)
        {
            if (number < 2)
                return false;

            if (number == 2)
                return true;

            if (number % 2 == 0)
                return false;

            var squareRoot = Math.Sqrt(number);
            for (var i = 3; i <= squareRoot; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }

        /// <summary>
        /// Convert an integer to English representation
        /// </summary>
        /// <param name="num">integer number</param>
        /// <returns>English words representation</returns>
        public static string ToEnglishString(this int num)
        {
            string Zero = "Zero";
            string[] LessThanTwenty = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
            string[] Tens = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
            string Hundred = "Hundred";
            string[] Thousands = { "", "Thousand", "Million", "Billion", "Trillion", "Quadrillion", "Quintillion", "Sextillion" };

            if (num == 0)
                return Zero;

            var stack = new Stack<string>();
            var thousandsCount = 0;
            while (num > 0)
            {
                if(num % 1000 != 0)
                    stack.Push(Thousands[thousandsCount]);

                var lessthanHundred = num % 100; num = num / 100;

                if (lessthanHundred < 20)
                    stack.Push(LessThanTwenty[lessthanHundred]);
                else
                {
                    stack.Push(LessThanTwenty[lessthanHundred % 10]);
                    stack.Push(Tens[lessthanHundred / 10]);
                }

                var hundred = num % 10; num = num / 10;
                if (hundred > 0)
                {
                    stack.Push(Hundred);
                    stack.Push(LessThanTwenty[hundred]);
                }

                thousandsCount++;
            }

            return string.Join(" ", stack.Where(item => !string.IsNullOrWhiteSpace(item)));
        }
    }
}
