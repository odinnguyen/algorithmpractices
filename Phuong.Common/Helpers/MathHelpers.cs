﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Helpers
{
    public class MathHelpers
    {
        /// <summary>
        /// Get max of the list of item
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="array">list</param>
        /// <returns>max</returns>
        public static T Max<T>(params T[] array) where T:IComparable
        {
            if (array == null || !array.Any())
                throw new ArgumentException("array is null or empty.");

            T max = array.First();
            foreach (var item in array)
                if (item.CompareTo(max) > 0)
                    max = item;

            return max;
        }

        /// <summary>
        /// Get min of the list of item
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="array">list</param>
        /// <returns>min</returns>
        public static T Min<T>(params T[] array) where T : IComparable
        {
            if (array == null || !array.Any())
                throw new ArgumentException("array is null or empty.");

            T min = array.First();
            foreach (var item in array)
                if (item.CompareTo(min) < 0)
                    min = item;

            return min;
        }
    }
}
