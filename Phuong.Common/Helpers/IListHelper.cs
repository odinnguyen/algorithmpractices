﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Helpers
{
    public static class IListExtensions
    {
        /// <summary>
        /// Check if the list contains consecutive numbers 
        /// </summary>
        /// <param name="numbers">list</param>
        /// <returns>true if contains consecutive numbers</returns>
        public static bool IsConsecutive(this IList<int> numbers)
        {
            var orderedNumbers = numbers.OrderBy(n => n).ToList();
            for (int i = 0; i < orderedNumbers.Count() - 1; i++)
                if (orderedNumbers[i] + 1 != orderedNumbers[i + 1])
                    return false;

            return true;
        }

        /// <summary>
        /// Compare two list if all element are equal
        /// </summary>
        /// <typeparam name="T">type of element: IComparable</typeparam>
        /// <param name="list1">list 1</param>
        /// <param name="list2">list 2</param>
        /// <returns>true if elements of two list are equal</returns>
        public static bool IsEqual<T>(this IList<T> list1, IList<T> list2) where T: IComparable
        {
            if (list1.Count != list2.Count)
                return false;

            for (var i = 0; i < list1.Count; i++)
                if (list1[i].CompareTo(list2[i]) != 0)
                    return false;

            return true;
        }

        /// <summary>
        /// Compare two list if all element are equal
        /// </summary>
        /// <typeparam name="T">type of element: IComparable</typeparam>
        /// <param name="list1">list 1</param>
        /// <param name="list2">list 2</param>
        /// <returns>true if elements of two list are equal</returns>
        public static bool IsEqual<T>(this IList<IList<T>> list1, IList<IList<T>> list2) where T : IComparable
        {
            if (list1.Count != list2.Count)
                return false;

            for (var i = 0; i < list1.Count; i++)
                if (!list1[i].IsEqual(list2[i]))
                    return false;

            return true;
        }

        /// <summary>
        /// Swap value of two indices
        /// </summary>
        /// <param name="list">array</param>
        /// <param name="index1">index 1</param>
        /// <param name="index2">index 2</param>
        public static void Swap<T>(this IList<T> list, int index1, int index2)
        {
            var temp = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }

        /// <summary>
        /// https://en.wikipedia.org/wiki/Median#Basic_procedure
        /// The median of a list of numbers can be found by first sorting the numbers ascending. 
        /// - If there is an odd number of values, the middle one is picked. 
        /// - If there is an even number of values, the median is then defined to be the average of the two middle values.
        /// </summary>
        /// <param name="list">list of numbers</param>
        /// <returns>Median = Value/Factor. We return both Value and Factor, to avoid the problem with integer division</returns>
        public static (long Value, int Factor) GetMedian(this IList<int> list)
        {
            if (list.Count % 2 == 0)
                return (list[0 + (list.Count - 1) / 2] + list[list.Count / 2], 2);
            else
                return (list[0 + (list.Count - 1) / 2], 1);
        }

        /// <summary>
        /// Convert list to Queue
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="list">list</param>
        /// <returns>queue</returns>
        public static Queue<T> ToQueue<T>(this IList<T> list)
        {
            var queue = new Queue<T>();
            foreach (var item in list)
                queue.Enqueue(item);

            return queue;
        }

        /// <summary>
        /// Dequeue first item from the list
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="list">list</param>
        /// <returns>first item</returns>
        public static T Dequeue<T>(this IList<T> list)
        {
            if (list.Any())
            {
                var item = list.First();
                list.RemoveAt(0);
                return item;
            }

            return default(T);
        }

        /// <summary>
        /// Find the index of an element in the list
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="list">list</param>
        /// <returns>Index of an element in the list</returns>
        public static int? IndexOf<T>(this IEnumerable<T> list, T item) where T: IComparable
        {
            if (list == null)
                return null;

            var index = 0;
            foreach (var element in list)
                if (element.CompareTo(item) == 0)
                    return index;
                else
                    index++;

            return null;
        }

        /// <summary>
        /// Take out the highest value element using comparer
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <param name="list">list</param>
        /// <param name="comparer">comparer</param>
        /// <returns>max element</returns>
        public static T Poll<T>(this List<T> list, IComparer<T> comparer)
        {
            var max = list.First();
            var maxIndex = 0;
            for (var i = 1; i < list.Count; i++)
                if (comparer.Compare(list[i], max) > 0)
                {
                    max = list[i];
                    maxIndex = i;
                }

            list.RemoveAt(maxIndex);
            return max;
        }

        /// <summary>
        /// Use binary search to add element into an ordered list
        /// </summary>
        /// <param name="list">list</param>
        /// <param name="element">new element</param>
        public static void BinaryAdd<T>(this IList<T> list, T item) where T : IComparable
        {
            if (!list.Any()) list.Add(item);
            else list.BinaryAdd(item, 0, list.Count - 1);
        }

        /// <summary>
        /// Use binary search to add element into an ordered list
        /// </summary>
        /// <param name="list">list</param>
        /// <param name="element">new element</param>
        /// <param name="start">start index</param>
        /// <param name="end">end index</param>
        static void BinaryAdd<T>(this IList<T> list, T item, int start, int end) where T: IComparable
        {
            if (start > end)
                return;

            if (start == end)
            {
                if (list[start].CompareTo(item) >= 0)
                    list.Insert(start, item);
                else if (start + 1 < list.Count)
                    list.Insert(start + 1, item);
                else
                    list.Add(item);

                return;
            }

            var mid = (end + start) / 2;
            if (list[mid].CompareTo(item) >= 0)
                list.BinaryAdd(item, start, mid);
            else
                list.BinaryAdd(item, mid + 1, end);
        }

        /// <summary>
        /// Use binary search to remove element from an ordered list
        /// </summary>
        /// <param name="list">list</param>
        /// <param name="element">new element</param>
        public static void BinaryRemove<T>(this IList<T> list, T item) where T : IComparable
        {
            if (!list.Any())
                return;

            list.BinaryRemove(item, 0, list.Count - 1);
        }

        /// <summary>
        /// Use binary search to remove element from an ordered list
        /// </summary>
        /// <param name="list">list</param>
        /// <param name="element">new element</param>
        /// <param name="start">start index</param>
        /// <param name="end">end index</param>
        static void BinaryRemove<T>(this IList<T> list, T item, int start, int end) where T : IComparable
        {
            if (start > end)
                return;

            if (start == end)
            {
                if (list[start].CompareTo(item) == 0)
                    list.RemoveAt(start);
                return;
            }

            var mid = (end + start) / 2;
            if (list[mid].CompareTo(item) >= 0)
                list.BinaryRemove(item, start, mid);
            else
                list.BinaryRemove(item, mid + 1, end);
        }
    }
}
