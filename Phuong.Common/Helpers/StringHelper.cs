﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Helpers
{
    public static class StringHelper
    {
        /// <summary>
        /// Convert string to dictionary
        /// </summary>
        /// <param name="s">the string</param>
        /// <returns>the dictionary</returns>
        public static Dictionary<char, int> ToAlphabetDictionary(this string s)
        {
            var dictionary = new Dictionary<char, int>();
            foreach (var c in s)
            {
                if (dictionary.ContainsKey(c))
                    dictionary[c]++;
                else
                    dictionary[c] = 1;
            }

            return dictionary;
        }

        /// <summary>
        /// Convert string to dictionary of counts of alphabet characters
        /// </summary>
        /// <param name="s">the string</param>
        /// <returns>the dictionary</returns>
        public static Dictionary<int, List<char>> ToCountDictionary(this string s)
        {
            var dictionnary = new Dictionary<int, List<char>>();
            var groups = s.GroupBy(c => c);
            foreach (var group in groups)
            {
                var count = group.Count();
                if (dictionnary.ContainsKey(count))
                    dictionnary[count].Add(group.Key);
                else
                    dictionnary[count] = new List<char> { group.Key };
            }

            return dictionnary;
        }
    }
}
