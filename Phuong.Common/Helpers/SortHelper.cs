﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Helpers
{
    public static class SortHelper
    {
        /// <summary>
        /// Merge sort the list
        /// </summary>
        /// <typeparam name="T">type of element: IComparable</typeparam>
        /// <param name="list">list</param>
        /// <returns>number of swaps</returns>
        public static long MergeSort<T>(this IList<T> list) where T: IComparable
        {
            if (!list.Any())
                return 0;

            return MergeSort(list, 0, list.Count - 1);
        }

        /// <summary>
        /// Merge sort the list
        /// </summary>
        /// <typeparam name="T">type of element: IComparable</typeparam>
        /// <param name="list">list</param>
        /// <param name="start">start of range</param>
        /// <param name="end">end of range</param>
        /// <returns>number of swaps</returns>
        public static long MergeSort<T>(this IList<T> list, int start, int end) where T : IComparable
        {
            long count = 0;
            var mid = (end + start) / 2;
            if (start < mid) count += MergeSort(list, start, mid);
            if (mid + 1 < end) count += MergeSort(list, mid + 1, end);

            var currA = 0;
            var firstHalf = list.Skip(start).Take(mid - start + 1).ToList();

            var currB = 0;
            var lastHalf = list.Skip(mid + 1).Take(end - mid).ToList();

            while (currA < firstHalf.Count || currB < lastHalf.Count)
            {
                if (currA == firstHalf.Count)
                    list[start + currA + currB] = lastHalf[currB++];
                else if (currB == lastHalf.Count)
                    list[start + currA + currB] = firstHalf[currA++];
                else if (firstHalf[currA].CompareTo(lastHalf[currB]) > 0)
                {
                    //This can be consider as bubble swaping from index=(mid + 1 + currB) to index=(start + currA + currB),
                    //to bring the element at (mid + 1 + currB) (aka lastHalf[currB]) to the correct position
                    list[start + currA + currB] = lastHalf[currB];
                    count += (mid + 1 + currB) - (start + currA + currB);
                    currB++;
                }
                else
                    list[start + currA + currB] = firstHalf[currA++];
            }

            return count;
        }


        /// <summary>
        /// Bubble sort the list
        /// </summary>
        /// <typeparam name="T">type of element: IComparable</typeparam>
        /// <param name="list">list</param>
        /// <returns>number of swaps</returns>
        public static long BubbleSort<T>(this IList<T> list) where T : IComparable
        {
            return BubbleSort(list, 0, list.Count - 1);
        }

        /// <summary>
        /// Bubble sort the list
        /// </summary>
        /// <typeparam name="T">type of element: IComparable</typeparam>
        /// <param name="list">list</param>
        /// <param name="start">start of range</param>
        /// <param name="end">end of range</param>
        /// <returns>number of swaps</returns>
        public static long BubbleSort<T>(this IList<T> list, int start, int end) where T : IComparable
        {
            if (start == end)
                return 0;

            long numberOfSwaps = 0;
            for (int i = 0; i < list.Count(); i++)
                for (int j = 0; j < list.Count() - i - 1; j++)//Each iteration, the largest unsorted member will move to the end
                {
                    // Swap adjacent elements if they are in decreasing order
                    if (list[j].CompareTo(list[j + 1]) > 0)
                    {
                        list.Swap(j, j + 1);
                        numberOfSwaps++;
                    }
                }

            return numberOfSwaps;
        }
    }
}
