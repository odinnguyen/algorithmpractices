﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Trie
{
    /// <summary>
    /// A Prefix tree
    /// </summary>
    public partial class Trie
    {
        /// <summary>
        /// Root node of the trie
        /// </summary>
        TrieNode Root { get; set; } = new TrieNode();

        /// <summary>
        /// Add a string into trie
        /// </summary>
        /// <param name="str">the string</param>
        public void Add(string str)
        {
            var curr = Root;
            for (var i = 0; i < str.Length; i++)
            {
                curr.PutChildIfAbsent(str[i]);
                curr = curr.GetChild(str[i]);
                curr.Size++;
            }
        }

        /// <summary>
        /// Count number of string the trie has with prefix
        /// </summary>
        /// <param name="prefix">the prefix</param>
        /// <returns></returns>
        public int Count(string prefix)
        {
            var curr = Root;
            for (var i = 0; i < prefix.Length; i++)
            {
                curr = curr.GetChild(prefix[i]);
                if (curr == null)
                    return 0;
            }

            return curr.Size;
        }
    }
}
