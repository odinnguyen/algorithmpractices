﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Trie
{
    public partial class Trie
    {
        /// <summary>
        /// A Prefix tree node
        /// </summary>
        class TrieNode
        {
            /// <summary>
            /// List of all children trie node
            /// </summary>
            private Dictionary<char, TrieNode> Children = new Dictionary<char, TrieNode>();

            /// <summary>
            /// The char value this trie node represent
            /// </summary>
            public char Value { get; private set; }

            /// <summary>
            /// The number of string this trie node contains
            /// </summary>
            public int Size { get; set; } = 0;

            public TrieNode(char c = '\0')
            {
                Value = c;
            }

            /// <summary>
            /// Add a child node if it doesn't exist
            /// </summary>
            /// <param name="ch">representative character</param>
            public void PutChildIfAbsent(char ch)
            {
                if (!Children.ContainsKey(ch))
                    Children[ch] = new TrieNode(ch);
            }

            /// <summary>
            /// Get a child node if it exists
            /// </summary>
            /// <param name="ch">representative character</param>
            /// <returns>child node. null if not found</returns>
            public TrieNode GetChild(char ch)
            {
                if (!Children.ContainsKey(ch))
                    return null;
                return Children[ch];
            }
        }
    }
}
