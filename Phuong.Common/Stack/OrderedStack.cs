﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phuong.Common.Stack
{
    using Phuong.Common.Helpers;
    public class OrderedStack<T> : Stack<T>
               where T : IComparable
    {
        /// <summary>
        /// Ordered list of all elements
        /// </summary>
        private List<T> Elements = new List<T>();

        /// <summary>
        /// Get min element from stack
        /// </summary>
        /// <returns>min element</returns>
        public T GetMin()
        {
            return Elements.First();
        }

        /// <summary>
        /// Get max element from stack
        /// </summary>
        /// <returns>max element</returns>
        public T GetMax()
        {
            return Elements.Last();
        }

        /// <summary>
        /// Removes and returns element at top of stack 
        /// </summary>
        public T Pop()
        {
            var element = base.Pop();
            Elements.BinaryRemove(element);
            return element;
        }

        /// <summary>
        /// Inserts element at top of the stack
        /// </summary>
        /// <param name="val"></param>
        public void Push(T val)
        {
            base.Push(val);
            Elements.BinaryAdd(val);
        }
    }
}
