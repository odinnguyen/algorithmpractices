using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;
    using Phuong.Common.Helpers;

    [TestClass]
    public class SearchTests
    {
        [TestMethod]
        public void HashTables_IceCreamParlor()
        {
            var result = Search.WhatFlavors(new List<int> { 1, 4, 5, 3, 2 }, 4);
            Assert.AreEqual((0, 3), result);

            result = Search.WhatFlavors(new List<int> { 2, 2, 4, 3 }, 4);
            Assert.AreEqual((0, 1), result);

            result = Search.WhatFlavors(new List<int> { 1, 2, 3, 5, 6 }, 5);
            Assert.AreEqual((1, 2), result);

            result = Search.WhatFlavors(new List<int> { 4, 3, 2, 5, 7 }, 8);
            Assert.AreEqual((1, 3), result);

            result = Search.WhatFlavors(new List<int> { 7, 2, 5, 4, 11 }, 12);
            Assert.AreEqual((0, 2), result);
        }


        [TestMethod]
        public void BinaryTreeSwapNodes()
        {
            var result = Search.SwapNodes(1, new List<(int, int)> { (2, 3), (-1, -1), (-1, -1) }, new List<int> { 1, 1 });
            Assert.IsTrue(result[0].IsEqual(new List<int> { 3, 1, 2 }));
            Assert.IsTrue(result[1].IsEqual(new List<int> { 2, 1, 3 }));

            result = Search.SwapNodes(1,
                new List<(int, int)> {
                    (2, 3),
                    (4, -1),
                    (5, -1),
                    (6, -1),
                    (7, 8),
                    (-1, 9),
                    (-1, -1),
                    (10, 11),
                    (-1, -1),
                    (-1, -1),
                    (-1, -1)},
                new List<int> { 2, 4 });
            Assert.IsTrue(result[0].IsEqual(new List<int> { 2, 9, 6, 4, 1, 3, 7, 5, 11, 8, 10 }));
            Assert.IsTrue(result[1].IsEqual(new List<int> { 2, 6, 9, 4, 1, 3, 7, 5, 10, 8, 11 }));

            result = Search.SwapNodes(1,
                new List<(int, int)> {
                    (2, 3),
                    (4, 5),
                    (6, -1),
                    (-1, 7),
                    (8, 9),
                    (10, 11),
                    (12, 13),
                    (-1, 14),
                    (-1, -1),
                    (15, -1),
                    (16, 17),
                    (-1, -1),
                    (-1, -1),
                    (-1, -1),
                    (-1, -1),
                    (-1, -1),
                    (-1, -1)},
                new List<int> { 2, 3 });
            Assert.IsTrue(result[0].IsEqual(new List<int> { 14, 8, 5, 9, 2, 4, 13, 7, 12, 1, 3, 10, 15, 6, 17, 11, 16 }));
            Assert.IsTrue(result[1].IsEqual(new List<int> { 9, 5, 14, 8, 2, 13, 7, 12, 4, 1, 3, 17, 11, 16, 6, 10, 15 }));
        }

        [TestMethod]
        public void Pairs()
        {
            var result = Search.CountPairs(2, new List<int> { 1, 5, 3, 4, 2 });
            Assert.AreEqual(3, result);

            result = Search.CountPairs(1, new List<int> { 363374326, 364147530, 61825163, 1073065718, 1281246024, 1399469912, 428047635, 491595254, 879792181, 1069262793 });
            Assert.AreEqual(0, result);

            result = Search.CountPairs(2, new List<int> { 1, 3, 5, 8, 6, 4, 2 });
            Assert.AreEqual(5, result);
        }
    }
}
