using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;
    using Phuong.Common.Helpers;

    [TestClass]
    public class StacksAndQueuesTests
    {
        [TestMethod]
        public void BalancedBrackets()
        {
            var result = StacksAndQueues.IsBalanced("{[()]}");
            Assert.AreEqual(true, result);

            result = StacksAndQueues.IsBalanced("{[(])}");
            Assert.AreEqual(false, result);

            result = StacksAndQueues.IsBalanced("{{[[(())]]}}");
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void ATaleOfTwoStacks()
        {
            var result = StacksAndQueues.ATaleOfTwoStacks(
                new List<List<int>> {
                    new List<int> { 1, 42 },
                    new List<int> { 2 },
                    new List<int> { 1, 14 },
                    new List<int> { 3 },
                    new List<int> { 1, 28 },
                    new List<int> { 3 },
                    new List<int> { 1, 60 },
                    new List<int> { 1, 78 },
                    new List<int> { 2 },
                    new List<int> { 2 } }).ToList();
            Assert.IsTrue(result.IsEqual(new List<int> { 14, 14 }));
        }
    }
}
