using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;

    [TestClass]
    public class SortingTests
    {
        [TestMethod]
        public void CountBubbleSortSwaps()
        {
            var result = Sorting.CountBubbleSortSwaps(new List<int> { 1, 2, 3 });
            Assert.AreEqual((0, 1, 3), result);

            result = Sorting.CountBubbleSortSwaps(new List<int> { 3, 2, 1 });
            Assert.AreEqual((3, 1, 3), result);

            result = Sorting.CountBubbleSortSwaps(new List<int> { 4, 2, 3, 1 });
            Assert.AreEqual((5, 1, 4), result);
        }

        [TestMethod]
        public void MarkAndToys()
        {
            var result = Sorting.MaximumToys(new List<int> { 1, 12, 5, 111, 200, 1000, 10 }, 50);
            Assert.AreEqual(4, result);

            result = Sorting.MaximumToys(new List<int> { 1, 2, 3, 4 }, 7);
            Assert.AreEqual(3, result);

            result = Sorting.MaximumToys(new List<int> { 3, 7, 2, 9, 4 }, 15);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void FraudulentActivityNotifications()
        {
            var result = Sorting.ActivityNotifications(new List<int> { 2, 3, 4, 2, 3, 6, 8, 4, 5 }, 5);
            Assert.AreEqual(2, result);

            result = Sorting.ActivityNotifications(new List<int> { 1, 2, 3, 4, 4 }, 4);
            Assert.AreEqual(0, result);

            result = Sorting.ActivityNotifications(new List<int> { 10, 20, 30, 40, 50 }, 3);
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void CountMergeSortInversions()
        {
            var result = Sorting.CountMergeSortInversions(new List<int> { 1, 1, 1, 2, 2 });
            Assert.AreEqual(0, result);

            result = Sorting.CountMergeSortInversions(new List<int> { 2, 1, 3, 1, 2 });
            Assert.AreEqual(4, result);

            result = Sorting.CountMergeSortInversions(new List<int> { 1, 5, 3, 7 });
            Assert.AreEqual(1, result);

            result = Sorting.CountMergeSortInversions(new List<int> { 1, 3, 5, 7 });
            Assert.AreEqual(0, result);

            result = Sorting.CountMergeSortInversions(new List<int> { 3, 2, 1 });
            Assert.AreEqual(3, result);
        }
    }
}
