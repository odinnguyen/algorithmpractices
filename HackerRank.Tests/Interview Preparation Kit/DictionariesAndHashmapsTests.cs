using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;
    using Phuong.Common.Helpers;

    [TestClass]
    public class DictionariesAndHashmapsTests
    {
        [TestMethod]
        public void RansomNote()
        {
            var result = DictionariesAndHashmaps.CheckMagazine(
                new List<string> { "give", "me", "one", "grand", "today", "night" }, 
                new List<string> { "give", "one", "grand", "today" });
            Assert.AreEqual(true, result);

            result = DictionariesAndHashmaps.CheckMagazine(
                new List<string> { "two", "times", "three", "is", "not", "four" },
                new List<string> { "two", "times", "two", "is", "four" });
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TwoStrings()
        {
            var result = DictionariesAndHashmaps.TwoStrings("hello", "world");
            Assert.AreEqual(true, result);

            result = DictionariesAndHashmaps.TwoStrings("hi", "world");
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void SherlockAndAnagrams()
        {
            var result = DictionariesAndHashmaps.SherlockAndAnagrams("abba");
            Assert.AreEqual(4, result);

            result = DictionariesAndHashmaps.SherlockAndAnagrams("abcd");
            Assert.AreEqual(0, result);

            result = DictionariesAndHashmaps.SherlockAndAnagrams("ifailuhkqq");
            Assert.AreEqual(3, result);

            result = DictionariesAndHashmaps.SherlockAndAnagrams("kkkk");
            Assert.AreEqual(10, result);

            result = DictionariesAndHashmaps.SherlockAndAnagrams("cdcd");
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void CountTriplets()
        {
            var result = DictionariesAndHashmaps.CountTriplets(new List<long> { 1, 2, 2, 4 }, 2);
            Assert.AreEqual(2, result);

            result = DictionariesAndHashmaps.CountTriplets(new List<long> { 1, 3, 9, 9, 27, 81 }, 3);
            Assert.AreEqual(6, result); 
            
            result = DictionariesAndHashmaps.CountTriplets(new List<long> { 1, 5, 5, 25, 125 }, 5);
            Assert.AreEqual(4, result);
        }


        [TestMethod]
        public void FrequencyQueries()
        {
            var result = DictionariesAndHashmaps.FrequencyQueries(
                new List<List<int>> {
                    new List<int> { 1, 5 },
                    new List<int> { 1, 6 },
                    new List<int> { 3, 2 },
                    new List<int> { 1, 10 },
                    new List<int> { 1, 10 },
                    new List<int> { 1, 6 },
                    new List<int> { 2, 5 },
                    new List<int> { 3, 2 }});
            Assert.IsTrue(result.IsEqual(new List<int> { 0, 1 }));

            result = DictionariesAndHashmaps.FrequencyQueries(
                new List<List<int>> {
                    new List<int> { 3, 4 },
                    new List<int> { 2, 1003},
                    new List<int> { 1, 16 },
                    new List<int> { 3, 1}});
            Assert.IsTrue(result.IsEqual(new List<int> { 0, 1 }));

            result = DictionariesAndHashmaps.FrequencyQueries(
                new List<List<int>> {
                    new List<int> { 1, 3 },
                    new List<int> { 2, 3 },
                    new List<int> { 3, 2 },
                    new List<int> { 1, 4 },
                    new List<int> { 1, 5 },
                    new List<int> { 1, 5 },
                    new List<int> { 1, 4 },
                    new List<int> { 3, 2 },
                    new List<int> { 2, 4 },
                    new List<int> { 3, 2 } });
            Assert.IsTrue(result.IsEqual(new List<int> { 0, 1, 1 }));
        }
    }
}
