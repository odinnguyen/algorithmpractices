using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;

    [TestClass]
    public class WarmUpChallengesTests
    {
        [TestMethod]
        public void SockMerchant()
        {
            var result = WarmUpChallenges.SockMerchant(new List<int> { 1, 1, 3, 1, 2, 1, 3, 3, 3, 3 });
            Assert.AreEqual(4, result);

            result = WarmUpChallenges.SockMerchant(new List<int> { 10, 20, 20, 10, 10, 30, 50, 10, 20 });
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void CountingValleys()
        {
            var result = WarmUpChallenges.CountingValleys("UDDDUDUU");
            Assert.AreEqual(1, result);

            result = WarmUpChallenges.CountingValleys("DDUUDDUDUUUD");
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void JumpingOnClouds()
        {
            var result = WarmUpChallenges.JumpingOnClouds(new List<int> { 0, 0, 1, 0, 0, 1, 0 });
            Assert.AreEqual(4, result);

            result = WarmUpChallenges.JumpingOnClouds(new List<int> { 0, 0, 0, 1, 0, 0 });
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void RepeatedString()
        {
            var result = WarmUpChallenges.RepeatedString("aba", 10);
            Assert.AreEqual(7, result);

            result = WarmUpChallenges.RepeatedString("a", 1000000000000);
            Assert.AreEqual(1000000000000, result);
        }
    }
}
