using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;
    using System.Linq;

    [TestClass]
    public class StringManipulationTests
    {
        [TestMethod]
        public void MakingAnagrams()
        {
            var result = StringManipulation.MakeAnagram("cde", "dcf");
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void AlternatingCharacters()
        {
            var result = StringManipulation.AlternatingCharacters("AABAAB");
            Assert.AreEqual(2, result);

            result = StringManipulation.AlternatingCharacters("A");
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void IsValid()
        {
            var result = StringManipulation.IsValid("abc");
            Assert.AreEqual("YES", result);

            result = StringManipulation.IsValid("abcc");
            Assert.AreEqual("YES", result);

            result = StringManipulation.IsValid("abcdefghhgfedecba");
            Assert.AreEqual("YES", result);

            result = StringManipulation.IsValid("abcdefghhgfedecba");
            Assert.AreEqual("YES", result);

            result = StringManipulation.IsValid("aaabbbcccdddeeefffgggh");
            Assert.AreEqual("YES", result);

            result = StringManipulation.IsValid("abccc");
            Assert.AreEqual("NO", result);

            result = StringManipulation.IsValid("abccdd");
            Assert.AreEqual("NO", result);
        }

        [TestMethod]
        public void SpecialSubstringCount()
        {
            var result = StringManipulation.SpecialSubstringCount("mnonopoo");
            Assert.AreEqual(12, result);
        }

        [TestMethod]
        public void CommonChild()
        {
            var result = StringManipulation.CommonChild("ABCD", "ABDC");
            Assert.AreEqual(3, result);

            result = StringManipulation.CommonChild("ABCD", "ABD");
            Assert.AreEqual(3, result);

            result = StringManipulation.CommonChild("ACEGG", "AABDG");
            Assert.AreEqual(2, result);

            result = StringManipulation.CommonChild("WEWOUCUIDGCGTRMEZEPXZFEJWISRSBBSYXAYDFEJJDLEBVHHKS", "FDAGCXGKCTKWNECHMRXZWMLRYUCOCZHJRRJBOAJOQJZZVUYXIC");
            Assert.AreEqual(15, result);
        }
    }
}
