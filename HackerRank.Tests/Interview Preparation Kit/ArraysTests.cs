using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview_Preparation_Kit;
    using Phuong.Common.Helpers;

    [TestClass]
    public class ArraysTests
    {
        [TestMethod]
        public void HourglassSum()
        {
            var result = Arrays.HourglassSum(
                new List<List<int>> {
                    new List<int>{ 1, 1, 1, 0, 0, 0 },
                    new List<int>{ 0, 1, 0, 0, 0, 0 },
                    new List<int>{ 1, 1, 1, 0, 0, 0 },
                    new List<int>{ 0, 0, 2, 4, 4, 0 },
                    new List<int>{ 0, 0, 0, 2, 0, 0 },
                    new List<int>{ 0, 0, 1, 2, 4, 0 }
                    }); ;
            Assert.AreEqual(19, result);
        }

        [TestMethod]
        public void RotateLeft()
        {
            var result = Arrays.RotateLeft(new List<int> { 33, 47, 70, 37, 8, 53, 13, 93, 71, 72, 51, 100, 60, 87, 97 }, 13);
            Assert.IsTrue(result.IsEqual(new List<int> { 87, 97, 33, 47, 70, 37, 8, 53, 13, 93, 71, 72, 51, 100, 60 }));

            result = Arrays.RotateLeft(new List<int> { 33, 47, 70 }, 10);
            Assert.IsTrue(result.IsEqual(new List<int> { 47, 70, 33 }));
        }

        [TestMethod]
        public void NewYearChaos()
        {
            var result = Arrays.MinimumBribes(new List<int> { 5, 1, 2, 3, 7, 8, 6, 4 });
            Assert.AreEqual("Too chaotic", result);

            result = Arrays.MinimumBribes(new List<int> { 1, 2, 5, 3, 7, 8, 6, 4 });
            Assert.AreEqual("7", result);
        }

        [TestMethod]
        public void MinimumSwaps()
        {
            var result = Arrays.MinimumSwaps(new int[] { 1, 3, 5, 2, 4, 6, 7 });
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void ArrayManipulation()
        {
            var result = Arrays.ArrayManipulation(10, 
                new List<List<int>> {   new List<int> { 2, 6, 8 },
                                        new List<int> { 3, 5, 7 },
                                        new List<int> { 1, 8, 1 },
                                        new List<int> { 5, 9, 15}} 
                );
            Assert.AreEqual(31, result);
        }
    }
}
