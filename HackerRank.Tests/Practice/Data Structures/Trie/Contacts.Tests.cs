using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Practice.Data_Structures.Trie.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class ContactsTests
    {
        void RunTest(Contacts.ISolution solution)
        {
            IList<int> result;
            List<List<string>> queries;

            queries = new List<List<string>> { 
                new List<string>{ "add","ed" },
                new List<string>{ "add", "eddie" },
                new List<string>{ "add", "edward" },
                new List<string>{ "find", "ed" },
                new List<string>{ "add", "edwina" },
                new List<string>{ "find", "edw" },
                new List<string>{ "find", "a" },
            };
            result = solution.RunQueries(queries);
            Assert.IsTrue(result.IsEqual(new List<int> { 3, 2, 0 }));

            queries = new List<List<string>> {
                new List<string>{ "add", "hackerrank" },
                new List<string>{ "add", "hacker" },
                new List<string>{ "find", "hac" },
                new List<string>{ "find", "hak" },
            };
            result = solution.RunQueries(queries);
            Assert.IsTrue(result.IsEqual(new List<int> { 2, 0 }));
        }

        [TestMethod]
        public void Contacts_MySolution1()
        {
            var solution = new Contacts.MySolution1();
            RunTest(solution);
        }

        [TestMethod]
        public void Contacts_MySolution2()
        {
            var solution = new Contacts.MySolution2();
            RunTest(solution);
        }
    }
}
