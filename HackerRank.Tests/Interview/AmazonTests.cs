using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace HackerRank.Tests.Interview_Preparation_Kit
{
    using HackerRank.Interview;
    using Phuong.Common.Helpers;

    [TestClass]
    public class AmazonTests
    {
        [TestMethod]
        public void SearchSuggestions()
        {
            var result = Amazon.SearchSuggestions(
            new List<string>
            {
                "abbS",
                "abc",
                "Abs",
                "bcs",
                "bdsa",
                "cdde",
                "rgb",
                "yjmm",
                "xxmm",
                "zeee",
            }, "abbs");
        }

        [TestMethod]
        public void MatchShoppingList()
        {
            int result;

            result = Amazon.MatchShoppingList(
            new List<string>
            {
                "orange",
                "apple apple",
                "banana orange apple",
                "banana"
            },
            new List<string>
            {
                "orange",
                "apple",
                "apple",
                "banana",
                "orange",
                "apple",
                "banana"
            });

            Assert.AreEqual(1, result);

            result = Amazon.MatchShoppingList(
            new List<string>
            {
                "orange",
                "apple apple",
                "banana anything apple",
                "banana"
            },
            new List<string>
            {
                "orange",
                "apple",
                "apple",
                "banana",
                "kiwi",
                "apple",
                "banana"
            });

            Assert.AreEqual(1, result);

            result = Amazon.MatchShoppingList(
            new List<string>
            {
                "anything anything",
                "anything anything anything",
                "anything",
                "anything anything anything anything",
                "anything"
            },
            new List<string>
            {
                "orange",
                "mango",
                "banana",
                "apricot",
                "apricot"
            });

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void MinimumGiftBox()
        {
            var result = Amazon.MinimumGiftBox(new List<int> { 5, 7, 12, 6, 3, 2, 8, 4 });
            Assert.IsTrue(result.IsEqual(new List<int> { 7, 8, 12 }));
        }

        [TestMethod]
        public void RelatedGroupCount()
        {
            var result = Amazon.RelatedGroupCount(new List<string> { "1100", "1110", "0110", "0001"});
            Assert.AreEqual(2, result);
        }
    }
}
