﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
    using Phuong.Common.Helpers;

    /// <summary>
    /// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
    /// Find the sum of all the primes below two million.
    /// </summary>
    internal class Problem10
    {
        public static void Run()
        {
            long sum = 2;
            for (var i = 3; i < 2000000; i += 2)
                if (i.IsPrime())
                    sum += i;
            Console.WriteLine(sum);
        }
    }
}
