﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
    using Phuong.Common.Helpers;

    /// <summary>
    /// By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
    /// 
    /// By replacing the 3rd and 4th digits of 56**3 with the same digit, 
    /// this 5-digit number is the first example having seven primes among the ten generated numbers, 
    /// yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. 
    /// Consequently 56003, being the first member of this family, is the smallest prime with this property.
    /// 
    /// Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
    /// </summary>
    internal class Problem51
    {
        const int NUMBER_OF_PRIME_IN_FAMILY = 8;
        public static void Run()
        {
            foreach(var prime in PrimeHelper.Primes)
            { 
                //Splits the number into a list of it's digits
                var digits = new List<int>();
                var temp = prime;
                while(temp > 0)
                {
                    digits.Insert(0, temp % 10);
                    temp = temp / 10;
                }

                //since there are a number of digits.Count digits. There are 2^(digits.Count) possible replacement
                for (int i = 1; i < Math.Pow(2, digits.Count); i++)
                {
                    var countPrime = 0;
                    var smallestPrime = int.MaxValue;
                    var binary = Convert.ToString(i, 2);

                    //Using binary to determine which digits we want to replace, a.k.a binary digits at that position is '1'
                    //Example: if binary is 10010, we want to replace 1st and 4th digits
                    for (int k = 0; k <= 9; k++)
                    {
                        var numberString = "";
                        for (int j = 0; j < digits.Count - binary.Length; j++)
                            numberString += digits[j];

                        for (int j = digits.Count - binary.Length; j < digits.Count; j++)
                        {
                            if (binary[j - (digits.Count - binary.Length)] == '0')
                                numberString += digits[j];
                            else
                                numberString += k;
                        }

                        //If the replace result into a less digits number, that replace doesn't count. ->IMPORTANT!!!<-
                        if (numberString.TrimStart('0').Length == digits.Count)
                        {
                            var number = Convert.ToInt32(numberString);
                            if (number.IsPrime())
                            {
                                countPrime++;
                                if (smallestPrime > number)
                                    smallestPrime = number;
                            }
                        }
                    }

                    if(countPrime == NUMBER_OF_PRIME_IN_FAMILY)
                    {
                        Console.WriteLine(smallestPrime);
                        return;
                    }
                }
            }
        }
    }
}
