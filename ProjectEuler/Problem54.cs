﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
    using Phuong.Common.Helpers;

    /// <summary>
    /// In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:
    ///
    /// High Card: Highest value card.
    /// One Pair: Two cards of the same value.
    /// Two Pairs: Two different pairs.
    /// Three of a Kind: Three cards of the same value.
    /// Straight: All cards are consecutive values.
    /// Flush: All cards of the same suit.
    /// Full House: Three of a kind and a pair.
    /// Four of a Kind: Four cards of the same value.
    /// Straight Flush: All cards are consecutive values of same suit.
    /// Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
    /// The cards are valued in the order:
    /// 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
    /// 
    /// If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives(see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared(see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.
    /// 
    /// Consider the following five hands dealt to two players:
    /// 
    /// Hand    Player 1	 	    Player 2	 	    Winner
    /// 1	 	5H 5C 6S 7S KD      2C 3S 8S 8D TD      Player 2
    ///         Pair of Fives       Pair of Eights
    ///         
    /// 2	 	5D 8C 9S JS AC      2C 5C 7D 8S QH      Player 1
    ///         Highest card Ace    Highest card Queen
    ///         
    /// 3        2D 9C AS AH AC      3D 6D 7D TD QD      Player 2
    ///         Three Aces          Flush with Diamonds
    ///         
    /// 4       4D 6S 9H QH QC      3D 6D 7H QD QS      Player 1
    ///         Pair of Queens      Pair of Queens
    ///         Highest card Nine   Highest card Seven
    ///         
    /// 5       2H 2D 4C 4D 4S      3C 3D 3S 9S 9D      Player 1
    ///         Full House          Full House
    ///         With Three Fours    with Three Threes
    ///         
    /// The file, poker.txt, contains one-thousand random hands dealt to two players.
    /// Each line of the file contains ten cards(separated by a single space): 
    /// - The first five are Player 1's cards
    /// - The last five are Player 2's cards.
    /// 
    /// You can assume that 
    /// - All hands are valid (no invalid characters or repeated cards)
    /// - Each player's hand is in no specific order
    /// - In each hand there is a clear winner.
    ///
    /// How many hands does Player 1 win?
    /// </summary>
    internal class Problem54
    {
        enum EnumCardValues
        {
            AceLow = 1,
            Two = 2,
            Three = 3,
            Four = 4,
            Five = 5,
            Six = 6,
            Seven = 7,
            Eight = 8,
            Nine = 9,
            Ten = 10,
            J = 11,
            Q = 12,
            K = 13,
            A = 14
        }

        const string FILE_NAME = @"Resources\p054_poker.txt";

        public static void Run() 
        {
            string line;
            var countPlayer1Win = 0;

            // Read the file and display it line by line.  
            using (var file = new System.IO.StreamReader(FILE_NAME))
                while ((line = file.ReadLine()) != null)
                {
                    var allCards = line.Split(' ');
                    var player1Cards = allCards.Take(5).ToArray();
                    var player2Cards = allCards.Skip(5).ToArray();

                    var hasFlushPlayer1 = HasFlush(player1Cards);
                    var straightCardsPlayer1 = GetStraight(player1Cards);

                    var hasFlushPlayer2 = HasFlush(player2Cards);
                    var straightCardsPlayer2 = GetStraight(player2Cards);

                    //
                    //Check Royal Flush
                    //
                    if (hasFlushPlayer1 && straightCardsPlayer1.First.HasValue && straightCardsPlayer1.Fifth == EnumCardValues.A) 
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (hasFlushPlayer2 && straightCardsPlayer2.First.HasValue && straightCardsPlayer2.Fifth == EnumCardValues.A)
                        continue;

                    //
                    //Check Straight Flush
                    //
                    if (hasFlushPlayer1 && straightCardsPlayer1.First.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    if (hasFlushPlayer2 && straightCardsPlayer2.First.HasValue)
                        continue;

                    //
                    //Check Four Of A Kind
                    //
                    var fourOfAKindCardPlayer1 = GetFourOfAKind(player1Cards);
                    var fourOfAKindCardPlayer2 = GetFourOfAKind(player2Cards);
                    if (fourOfAKindCardPlayer1.HasValue && !fourOfAKindCardPlayer2.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!fourOfAKindCardPlayer1.HasValue && fourOfAKindCardPlayer2.HasValue)
                        continue;
                    else if (fourOfAKindCardPlayer1.HasValue && fourOfAKindCardPlayer2.HasValue)
                    {
                        if(fourOfAKindCardPlayer1.Value > fourOfAKindCardPlayer2.Value)
                        {
                            countPlayer1Win++;
                            continue;
                        }
                        else if (fourOfAKindCardPlayer1.Value < fourOfAKindCardPlayer2.Value)
                            continue;
                        else if (fourOfAKindCardPlayer1.Value == fourOfAKindCardPlayer2.Value)
                        {
                            var highestCardValuePlayer1 = GetCardValues(player1Cards).Where(v => v != (int)fourOfAKindCardPlayer1.Value).First();
                            var highestCardValuePlayer2 = GetCardValues(player2Cards).Where(v => v != (int)fourOfAKindCardPlayer2.Value).First();
                            if (highestCardValuePlayer1 > highestCardValuePlayer2)
                                countPlayer1Win++;

                            continue;//Since the assumption is there is always clear winner
                        }
                    }

                    //
                    //Check Full House
                    //
                    var fullHousePlayer1 = GetFullHouse(player1Cards);
                    var fullHousePlayer2 = GetFullHouse(player2Cards);
                    if (fullHousePlayer1.Pair.HasValue && fullHousePlayer1.Triplet.HasValue && !fullHousePlayer2.Pair.HasValue && !fullHousePlayer2.Triplet.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!fullHousePlayer1.Pair.HasValue && !fullHousePlayer1.Triplet.HasValue && fullHousePlayer2.Pair.HasValue && fullHousePlayer2.Triplet.HasValue)
                        continue;
                    else if (fullHousePlayer1.Pair.HasValue && fullHousePlayer1.Triplet.HasValue && fullHousePlayer2.Pair.HasValue && fullHousePlayer2.Triplet.HasValue)
                    {
                        if (fullHousePlayer1.Triplet.Value > fullHousePlayer2.Triplet.Value)
                        {
                            countPlayer1Win++;
                            continue;
                        }
                        else if (fullHousePlayer1.Triplet.Value < fullHousePlayer2.Triplet.Value)
                            continue;
                        else if (fullHousePlayer1.Triplet.Value == fullHousePlayer2.Triplet.Value)
                        {
                            if (fullHousePlayer1.Pair.Value > fullHousePlayer2.Pair.Value)
                                countPlayer1Win++;

                             continue;
                        }
                    }

                    var highestOfFiveCardsPlayer1 = GetHighestCardValues(player1Cards);
                    var highestOfFiveCardsPlayer2 = GetHighestCardValues(player2Cards);

                    //
                    //Check Flush
                    //
                    if (hasFlushPlayer1 && !hasFlushPlayer2)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!hasFlushPlayer1 && hasFlushPlayer2)
                        continue;
                    else if (hasFlushPlayer1 && hasFlushPlayer2)
                    {
                        if (highestOfFiveCardsPlayer1 > highestOfFiveCardsPlayer2)
                            countPlayer1Win++;

                        continue;
                    }

                    //
                    //Check Straight
                    //
                    if (straightCardsPlayer1.First.HasValue && !straightCardsPlayer2.First.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!straightCardsPlayer1.First.HasValue && straightCardsPlayer2.First.HasValue)
                        continue;
                    else if (straightCardsPlayer1.First.HasValue && straightCardsPlayer2.First.HasValue)
                    {
                        if (straightCardsPlayer1.Fifth > straightCardsPlayer2.Fifth)
                            countPlayer1Win++;

                        continue;
                    }

                    //
                    //Check Three Of A Kind
                    //
                    var threeOfAKindCardPlayer1 = GetThreeOfAKind(player1Cards);
                    var threeOfAKindCardPlayer2 = GetThreeOfAKind(player2Cards);
                    if (threeOfAKindCardPlayer1.HasValue && !threeOfAKindCardPlayer2.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!threeOfAKindCardPlayer1.HasValue && threeOfAKindCardPlayer2.HasValue)
                        continue;
                    else if (threeOfAKindCardPlayer1.HasValue && threeOfAKindCardPlayer2.HasValue)
                    {
                        if (threeOfAKindCardPlayer1.Value > threeOfAKindCardPlayer2.Value)
                        {
                            countPlayer1Win++;
                            continue;
                        }
                        else if (threeOfAKindCardPlayer1.Value < threeOfAKindCardPlayer2.Value)
                            continue;
                        else if (threeOfAKindCardPlayer1.Value == threeOfAKindCardPlayer2.Value)
                        {
                            var highestCardsValuePlayer1 = GetCardValues(player1Cards).Where(v => v != (int)threeOfAKindCardPlayer1.Value).OrderByDescending(v => v).ToList();
                            var highestCardsValuePlayer2 = GetCardValues(player2Cards).Where(v => v != (int)threeOfAKindCardPlayer2.Value).OrderByDescending(v => v).ToList();
                            for(int i = 0; i < highestCardsValuePlayer1.Count; i++)
                            {
                                if (highestCardsValuePlayer1[i] > highestCardsValuePlayer2[i])
                                {
                                    countPlayer1Win++;
                                    break;
                                }
                                else if (highestCardsValuePlayer1[i] < highestCardsValuePlayer2[i])
                                    break;
                                else if (highestCardsValuePlayer1[i] == highestCardsValuePlayer2[i])
                                    continue;
                            }

                            continue;//Since the assumption is there is always clear winner
                        }
                    }

                    //
                    //Check two pairs
                    //
                    var twoPairPlayer1 = GetTwoPair(player1Cards);
                    var twoPairPlayer2 = GetTwoPair(player2Cards);
                    if (twoPairPlayer1.Large.HasValue && twoPairPlayer1.Small.HasValue && !twoPairPlayer2.Large.HasValue && !twoPairPlayer2.Small.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!twoPairPlayer1.Large.HasValue && !twoPairPlayer1.Small.HasValue && twoPairPlayer2.Large.HasValue && twoPairPlayer2.Small.HasValue)
                        continue;
                    else if (twoPairPlayer1.Large.HasValue && twoPairPlayer1.Small.HasValue && twoPairPlayer2.Large.HasValue && twoPairPlayer2.Small.HasValue)
                    {
                        if (twoPairPlayer1.Large > twoPairPlayer2.Large)
                        {
                            countPlayer1Win++;
                            continue;
                        }
                        else if (twoPairPlayer1.Large < twoPairPlayer2.Large)
                            continue;
                        else if (twoPairPlayer1.Large == twoPairPlayer2.Large)
                        {
                            if (twoPairPlayer1.Small > twoPairPlayer2.Small)
                            {
                                countPlayer1Win++;
                                continue;
                            }
                            else if (twoPairPlayer1.Small < twoPairPlayer2.Small)
                                continue;
                            else if (twoPairPlayer1.Small == twoPairPlayer2.Small)
                            {
                                var highestCardValuePlayer1 = GetCardValues(player1Cards).Where(v => v != (int)twoPairPlayer1.Large.Value && v != (int)twoPairPlayer1.Small.Value).First();
                                var highestCardValuePlayer2 = GetCardValues(player2Cards).Where(v => v != (int)twoPairPlayer2.Large.Value && v != (int)twoPairPlayer2.Small.Value).First();
                                if (highestCardValuePlayer1 > highestCardValuePlayer2)
                                    countPlayer1Win++;

                                continue;//Since the assumption is there is always clear winner
                            }
                        }
                    }

                    //
                    //Check one pairs
                    //
                    var onePairPlayer1 = GetOnePair(player1Cards);
                    var onePairPlayer2 = GetOnePair(player2Cards);
                    if (onePairPlayer1.HasValue && !onePairPlayer2.HasValue)
                    {
                        countPlayer1Win++;
                        continue;
                    }
                    else if (!onePairPlayer1.HasValue && onePairPlayer2.HasValue)
                        continue;
                    else if (onePairPlayer1.HasValue && onePairPlayer2.HasValue)
                    {
                        if (onePairPlayer1.Value > onePairPlayer2.Value)
                        {
                            countPlayer1Win++;
                            continue;
                        }
                        else if (onePairPlayer1.Value < onePairPlayer2.Value)
                            continue;
                        else if (onePairPlayer1.Value == onePairPlayer2.Value)
                        {
                            var highestCardsValuePlayer1 = GetCardValues(player1Cards).Where(v => v != (int)onePairPlayer1.Value).OrderByDescending(v => v).ToList();
                            var highestCardsValuePlayer2 = GetCardValues(player2Cards).Where(v => v != (int)onePairPlayer2.Value).OrderByDescending(v => v).ToList();
                            for (int i = 0; i < highestCardsValuePlayer1.Count; i++)
                            {
                                if (highestCardsValuePlayer1[i] > highestCardsValuePlayer2[i])
                                {
                                    countPlayer1Win++;
                                    break;
                                }
                                else if (highestCardsValuePlayer1[i] < highestCardsValuePlayer2[i])
                                    break;
                                else if (highestCardsValuePlayer1[i] == highestCardsValuePlayer2[i])
                                    continue;
                            }

                            continue;//Since the assumption is there is always clear winner
                        }
                    }

                    //
                    //Check Highest card
                    //
                    var highestCardValueOfFivePlayer1 = GetCardValues(player1Cards).OrderByDescending(v => v).First();
                    var highestCardValueOfFivePlayer2 = GetCardValues(player2Cards).OrderByDescending(v => v).First();
                    if (highestCardValueOfFivePlayer1 > highestCardValueOfFivePlayer2)
                        countPlayer1Win++;
                }

            Console.WriteLine($"Player 1 won {countPlayer1Win} time(s).");
        }

        /// <summary>
        /// Check if all card belongs to the same suit
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns>Is Flush?</returns>
        static bool HasFlush(params string[] cards)
        {
            for (int i = 0; i < cards.Length - 1; i++)
                if (cards[i][1] != cards[i + 1][1])
                    return false;
            return true;
        }

        /// <summary>
        /// Get Straight Cards
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static (EnumCardValues? First, EnumCardValues? Second, EnumCardValues? Third, EnumCardValues? Fourth, EnumCardValues? Fifth) GetStraight(params string[] cards)
        {
            var cardValues = GetCardValues(cards);

            var cardValuesToNumbersAHigh = new List<int>();
            var cardValuesToNumbersALow = new List<int>();
            foreach (var cardValue in cardValues)
            {
                if (cardValue == (int)EnumCardValues.A)
                {
                    cardValuesToNumbersAHigh.Add(cardValue);
                    cardValuesToNumbersALow.Add((int)EnumCardValues.AceLow);
                }
                else
                {
                    cardValuesToNumbersAHigh.Add(cardValue);
                    cardValuesToNumbersALow.Add(cardValue);
                }
            }

            if (cardValuesToNumbersAHigh.IsConsecutive())
            {
                cardValuesToNumbersAHigh = cardValuesToNumbersAHigh.OrderBy(v => v).ToList();
                return ((EnumCardValues)cardValuesToNumbersAHigh[0], 
                    (EnumCardValues)cardValuesToNumbersAHigh[1], 
                    (EnumCardValues)cardValuesToNumbersAHigh[2], 
                    (EnumCardValues)cardValuesToNumbersAHigh[3], 
                    (EnumCardValues)cardValuesToNumbersAHigh[4]);
            }

            if (cardValuesToNumbersALow.IsConsecutive())
            {
                cardValuesToNumbersALow = cardValuesToNumbersALow.OrderBy(v => v).ToList();
                return ((EnumCardValues)cardValuesToNumbersALow[0],
                    (EnumCardValues)cardValuesToNumbersALow[1],
                    (EnumCardValues)cardValuesToNumbersALow[2],
                    (EnumCardValues)cardValuesToNumbersALow[3],
                    (EnumCardValues)cardValuesToNumbersALow[4]);
            }

            return (null, null, null, null, null);
        }

        /// <summary>
        /// Get 4 of a kind
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static EnumCardValues? GetFourOfAKind(params string[] cards)
        {
            var cardValues = GetCardValues(cards);
            var pairs = cardValues.GroupBy(v => v).Where(g => g.Count() == 4).Select(g => new { Value = g.Key, Count = g.Count() });
            if (pairs.Count() == 0)
                return null;
            else
                return (EnumCardValues)(pairs.First().Value);
        }

        /// <summary>
        /// Get 3 of a kind
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static EnumCardValues? GetThreeOfAKind(params string[] cards)
        {
            var cardValues = GetCardValues(cards);
            var pairs = cardValues.GroupBy(v => v).Where(g => g.Count() == 3).Select(g => new { Value = g.Key, Count = g.Count() });
            if (pairs.Count() == 0)
                return null;
            else
                return (EnumCardValues)(pairs.First().Value);
        }

        /// <summary>
        /// Get 1 pairs
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static EnumCardValues? GetOnePair(params string[] cards)
        {
            var cardValues = GetCardValues(cards);
            var pairs = cardValues.GroupBy(v => v).Where(g => g.Count() == 2).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(v => v);
            if (pairs.Count() == 0)
                return null;
            else
                return (EnumCardValues)(pairs.First().Value);
        }

        /// <summary>
        /// Get 2 pairs
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static (EnumCardValues? Large, EnumCardValues? Small) GetTwoPair(params string[] cards)
        {
            var cardValues = GetCardValues(cards);
            var pairs = cardValues.GroupBy(v => v).Where(g => g.Count() == 2).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(g => g.Value).ToList();
            if (pairs.Count() < 2)
                return (null, null);
            else
                return ((EnumCardValues)(pairs.First().Value), (EnumCardValues)(pairs.Skip(1).First().Value));
        }

        /// <summary>
        /// Get first 4 of a kind
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static (EnumCardValues? Pair, EnumCardValues? Triplet) GetFullHouse(params string[] cards)
        {
            var cardValues = GetCardValues(cards);
            var pairs = cardValues.GroupBy(v => v).Select(g => new { Value = g.Key, Count = g.Count() });

            if (pairs.Count() == 2 && pairs.Any(g => g.Count == 3) && pairs.Any(g => g.Count == 2))
                return ((EnumCardValues)(pairs.FirstOrDefault(g => g.Count == 2).Value), (EnumCardValues)(pairs.FirstOrDefault(g => g.Count == 2).Value));
            else
                return (null, null);
        }

        /// <summary>
        /// Get the highest card value
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns>highest card value</returns>
        static int GetHighestCardValues(params string[] cards)
        {
            return GetCardValues(cards).OrderByDescending(v => v).FirstOrDefault();
        }

        /// <summary>
        /// Extract cards' values        
        /// "2" => 2,
        /// "3" => 3,
        /// "4" => 4,
        /// "5" => 5,
        /// "6" => 6,
        /// "7" => 7,
        /// "8" => 8,
        /// "9" => 9,
        /// "T" => 10,
        /// "J" => 10,
        /// "Q" => 11,
        /// "K" => 12,
        /// "A" => 13
        /// </summary>
        /// <param name="cards">cards</param>
        /// <returns></returns>
        static IEnumerable<int> GetCardValues(params string[] cards)
        {
            return cards.Select(h => h.Substring(0, 1) switch
            {
                "2" => (int)EnumCardValues.Two,
                "3" => (int)EnumCardValues.Three,
                "4" => (int)EnumCardValues.Four,
                "5" => (int)EnumCardValues.Five,
                "6" => (int)EnumCardValues.Six,
                "7" => (int)EnumCardValues.Seven,
                "8" => (int)EnumCardValues.Eight,
                "9" => (int)EnumCardValues.Nine,
                "T" => (int)EnumCardValues.Ten,
                "J" => (int)EnumCardValues.J,
                "Q" => (int)EnumCardValues.Q,
                "K" => (int)EnumCardValues.K,
                "A" => (int)EnumCardValues.A,
                _ => throw new Exception($"Invalid card value '{h.Substring(0, 1)}'")
            });
        }
    }
}
