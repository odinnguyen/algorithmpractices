﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
    /// <summary>
    /// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
    /// a^2 + b^2 = c^2
    /// For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
    /// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    /// Find the product a*b*c.
    /// </summary>
    internal class Problem9
    {
        public static void Run() 
        {
            for (int a = 1; a <= 1000; a++)
                for (int b = 1; b <= 1000 - a; b++)
                {
                    if (a * a + b * b == (1000 - a - b) * (1000 - a - b))
                    {
                        Console.WriteLine($"a={a}, b={b}, c={1000 - a - b}, abc={a * b * (1000 - a - b)}");
                        return;
                    }
                }
        }
    }
}
