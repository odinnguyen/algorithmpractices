﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HackerRank.Interview
{
    public partial class Amazon
    {
        /// <summary>
        /// Let pretend customerQuery is dynamic field where the customer typing in.
        /// - We will only do keywords matching when customerQuery.Length >= 2
        /// - Matching is case in-sensitive.
        /// - Keyword must start with what in user's input
        /// </summary>
        /// <param name="repository">list of keywords</param>
        /// <param name="customerQuery">customer input</param>
        /// <returns>
        /// List of top 3 keywords matched, lower cased alphabetically ordered, 
        /// for each new character input into customerQuery starting from the second character
        /// </returns>
        public static List<List<string>> SearchSuggestions(List<string> repository, string customerQuery)
        {
            var result = new List<List<string>>();

            var suggestions = repository.Select(s => s.ToLowerInvariant()).ToList();
            suggestions.Sort(StringComparer.Ordinal);

            for (int i = 2; i <= customerQuery.Length; i++)//only start when query length >=2
            {
                var input = customerQuery.Substring(0, i);
                suggestions = suggestions.Where(keyword => keyword.StartsWith(input, StringComparison.OrdinalIgnoreCase)).ToList();//case in-sensitive
                result.Add(suggestions.Take(3).ToList());
            }

            return result;
        }

        /// <summary>
        /// shoppingCart must matches all promotion patterns, following codeList order.
        /// - "anything" can only match one fruit
        /// </summary>
        /// <param name="codeList">
        /// codeList contains all promotion patterns for fruits, order matters
        /// Example: ["orange", "apple apple", "banana anaything apple", "banana", "anything anything"]
        /// - "anything" can only match one fruit
        /// </param>
        /// <param name="shoppingCart">
        /// shoppingCart contains fruits added into carts, order matters.
        /// Example: ["orange", "apple", "apple", "banana", "orange", "apple", "banana"]
        /// </param>
        /// <returns>
        /// 0: not match all codelist
        /// 1: match all code list. 
        /// Must match following codeList order
        /// </returns>
        public static int MatchShoppingList(List<string> codeList, List<string> shoppingCart)
        {
            var index = 0;
            foreach (var code in codeList)
            {
                var fruits = code.Split(' ');
                var matched = false;
                for (; index + fruits.Length <= shoppingCart.Count; index++)
                {
                    matched = true;
                    var anythingMatch = string.Empty;
                    for (var i = 0; i < fruits.Length; i++)
                    {
                        if (fruits[i] == "anything")
                        {
                            if (string.IsNullOrWhiteSpace(anythingMatch))
                            {
                                anythingMatch = shoppingCart[index + i];
                                continue;
                            }
                            else if (anythingMatch == shoppingCart[index + i])
                                continue;
                            else
                            {
                                matched = false;
                                break;
                            }
                        }
                        else if (shoppingCart[index + i] != fruits[i])
                        {
                            matched = false;
                            break;
                        }
                    }

                    if (matched)
                        break;
                }

                if (!matched)
                    return 0;
            }
            return 1;
        }

        /// <summary>
        /// Using Regex wont match string that are overlapped
        /// 
        /// Example: regEx="banana (\w+) banana", string = "banana orange banana kiwi banana apple banana"
        /// It will only match "banana orange banana" and "banana apple banana"
        /// But, what we want is matching of "banana orange banana", "banana kiwi banana" and "banana apple banana"
        /// </summary>
        /// <param name="codeList"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public static int MatchShoppingList_Wrong(List<string> codeList, List<string> shoppingCart)
        {
            var shoppingChartString = shoppingCart.Aggregate((s1, s2) => s1 + " " + s2);

            var index = 0;
            foreach (var code in codeList)
            {
                var matchStr = code.Trim();
                matchStr = matchStr.Replace("anything", @"(\w+)");
                var regEx = new Regex(matchStr);
                var matches = regEx.Matches(shoppingChartString);
                bool found = false;
                foreach (Match match in matches)
                    if (match.Index >= index)
                    {
                        if (match.Groups.Count > 2)
                        {
                            var anythingMatchedTwoFruits = false;
                            for (int i = 1; i < match.Groups.Count - 1; i++)
                                if (match.Groups[i] != match.Groups[i + 1])
                                {
                                    anythingMatchedTwoFruits = true;
                                    break;
                                }

                            if (anythingMatchedTwoFruits)
                                continue;
                        }

                        index = match.Index;
                        found = true;
                        break;
                    }

                if (!found)
                    return 0;
            }

            return 1;
        }
    }
}
