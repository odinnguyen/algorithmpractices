﻿using Phuong.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HackerRank.Interview
{
    public partial class Amazon
    {
        /// <summary>
        /// Provided list of all items's weights. 
        /// Divide item into boxA and BoxB, where 
        /// - boxA.weight > boxB.weight
        /// - boxB.Count > boxA.Count
        /// - boxA.Count is the least of all possibilities
        /// 
        /// return list of boxA item' weights, order from lowest to highest
        /// </summary>
        /// <param name="arr">List of all item's weight</param>
        /// <returns>list of boxA item' weights, order from lowest to highest</returns>
        public static List<int> MinimumGiftBox(List<int> arr)
        {
            var orderedList = arr.OrderByDescending(weight => weight).ToList();

            long boxAWeight = 0;
            long boxBWeight = arr.Sum(weight => (long)weight);
            var boxA = new List<int>();

            foreach(var itemWeight in orderedList)
            {
                boxAWeight += itemWeight;
                boxBWeight -= itemWeight;
                boxA.Add(itemWeight);

                if (boxAWeight > boxBWeight)
                    break;
            }

            return boxA.OrderBy(weight => weight).ToList();
        }

        /// <summary>
        /// Given matrix of related people: 
        /// - person 1 related to person 2 if matrix[1][2] = 1
        /// - If person A related to person B, person A belong to the group that person B belong to
        /// 
        /// Return count of all groups
        /// </summary>
        /// <param name="related">List of binary string which can be use to build the matrix</param>
        /// <returns>count of all groups</returns>
        public static int RelatedGroupCount(List<string> related)
        {
            //build matrix
            var matrix = new int[related.Count, related.Count];
            for (var i = 0; i < related.Count; i++)
                for (var j = 0; j < related.Count; j++)
                    matrix[i,j] = related[i][j] == '0' ? 0 : 1;

            //List of all people that need to be mapped
            var remainingPeople = new List<int>();
            for (var i = 0; i < related.Count; i++)
                remainingPeople.Add(i);

            int groupCount = 0;
            while(remainingPeople.Count > 0)
            {
                //Using Breadth First Search to find all people of the group where the first person belong to
                var queue = new Queue<int>();
                queue.Enqueue(remainingPeople.Dequeue());
                while(queue.Count > 0)
                {
                    var index = 0;
                    var person = queue.Dequeue();
                    while (index < remainingPeople.Count)
                    {
                        var target = remainingPeople[index];
                        if (matrix[person, target] != 0)
                        {
                            remainingPeople.Remove(target);
                            queue.Enqueue(target);
                        }
                        else
                         index++;
                    }
                }

                //increase group count after removing all people from one group
                groupCount++;
            }

            return groupCount;
        }
    }
}
