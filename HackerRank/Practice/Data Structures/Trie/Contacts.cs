﻿using Phuong.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Practice.Data_Structures.Trie
{
    using Phuong.Common.Trie;
    /// <summary>
    /// We're going to make our own Contacts application! The application must perform two types of operations:
    /// - Add name, where name is a string denoting a contact name.This must store name as a new contact in the application.
    /// - Find partial, where partial is a string denoting a partial name to search the application for. 
    ///   It must count the number of contacts starting with partial and print the count on a new line.
    ///   
    /// Given sequential add and find operations, perform each operation in order.
    /// Example
    /// Operations are requested as follows:
    /// add ed
    /// add eddie
    /// add edward
    /// find ed
    /// add edwina
    /// find edw
    /// find a
    /// 
    /// Three add operations include the names 'ed', 'eddie', and 'edward'. 
    /// Next, find ed matches all 3 names. Note that it matches and counts the entire name 'ed'. 
    /// Next, add 'edwina' and then find 'edw'. 2 names match: 'edward' and 'edwina'. 
    /// In the final operation, there are 0 names that start with 'a'. 
    /// Return the array [3,2,0].
    /// 
    /// Function Description
    /// Complete the contacts function below.
    /// contacts has the following parameters:
    /// - string queries[n]: the operations to perform
    /// 
    /// Returns
    /// - int[]: the results of each find operation
    /// 
    /// Input Format:
    /// The first line contains a single integer, n, the number of operations to perform(the size of queries[]).
    /// Each of the following n lines contains a string, queries[i].
    /// 
    /// Constraint:
    /// 1 <= n <= 10^5
    /// 1 <= length of name <= 21
    /// 1 <= length of partial <= 21
    /// name and partial contains lowercase English letters only
    /// The input does not have any duplicate name for the add operation
    /// </summary>
    public class Contacts
    {
        public interface ISolution
        {
            List<int> RunQueries(List<List<string>> queries);
        }

        public class MySolution1 : ISolution
        {
            public List<int> RunQueries(List<List<string>> queries)
            {
                var trie = new Trie();
                var result = new List<int>();
                foreach (var query in queries)
                {
                    switch (query[0])
                    {
                        case "add":
                            trie.Add(query[1]);
                            break;
                        case "find":
                            result.Add(trie.Count(query[1]));
                            break;
                    }
                }

                return result;
            }
        }

        public class MySolution2 : ISolution
        {
            public List<int> RunQueries(List<List<string>> queries)
            {
                var dictionary = new Dictionary<string, int>();
                var result = new List<int>();
                foreach (var query in queries)
                {
                    switch (query[0])
                    {
                        case "add":
                            for (var i = 1; i <= query[1].Length; i++)
                            {
                                var subStr = query[1].Substring(0, i);
                                if (!dictionary.ContainsKey(subStr))
                                    dictionary[subStr] = 1;
                                else dictionary[subStr]++;
                            }
                            break;
                        case "find":
                            if (dictionary.ContainsKey(query[1]))
                                result.Add(dictionary[query[1]]);
                            else
                                result.Add(0);
                            break;
                    }
                }

                return result;
            }
        }
    }
}
