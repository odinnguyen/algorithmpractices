﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class WarmUpChallenges
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/counting-valleys/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup
        /// An avid hiker keeps meticulous records of their hikes. 
        /// During the last hike that took exactly "steps" steps, for every step it was noted if it was an uphill, U, or a downhill, D step. 
        /// Hikes always start and end at sea level, and each step up or down represents a 1 unit change in altitude. 
        /// 
        /// We define the following terms:
        /// - A mountain is a sequence of consecutive steps above sea level, starting with a step up from sea level and ending with a step down to sea level.
        /// - A valley is a sequence of consecutive steps below sea level, starting with a step down from sea level and ending with a step up to sea level.
        /// Given the sequence of up and down steps during a hike, find and print the number of valleys walked through.
        /// 
        /// Example: steps = [DDUUUUDD]
        /// The hiker first enters a valley 2 units deep. Then they climb out and up onto a mountain 2 units high. 
        /// Finally, the hiker returns to sea level and ends the hike.
        /// </summary>
        /// <param name="path">path</param>
        /// <returns>number of valleys</returns>
        public static int CountingValleys(string path)
        {
            var altitude = 0;
            var numberOfValleys = 0;
            foreach (var step in path)
            {
                if (altitude == -1 && step == 'U')
                    numberOfValleys++;

                altitude += step == 'U' ? 1 : -1;
            }

            return numberOfValleys;
        }
    }
}
