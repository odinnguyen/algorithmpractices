﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class WarmUpChallenges
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/repeated-string/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup
        /// There is a string, s, of lowercase English letters that is repeated infinitely many times. 
        /// Given an integer, n, find and print the number of letter a's in the first n letters of the infinite string.
        /// 
        /// Example: s='abcac', n=10
        /// The substring we consider is abcacabcac, the first 10 characters of the infinite string. There are 4 occurrences of 'a' in the substring.
        /// </summary>
        /// <param name="s">infinitely repeated string</param>
        /// <param name="n">length of the substring</param>
        /// <returns>number of letter 'a'</returns>
        public static long RepeatedString(string s, long n)
        {
            if (s.Length >= n)
            {
                s = s.Substring(0, (int)n);
                return s.Count(c => c == 'a');
            }
            else
                return s.Count(c => c == 'a') * (n / s.Length) + s.Substring(0, (int)(n % s.Length)).Count(c => c == 'a');
        }
    }
}
