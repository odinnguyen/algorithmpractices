﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class WarmUpChallenges
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/sock-merchant/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup
        /// There is a large pile of socks that must be paired by color. 
        /// Given an array of integers representing the color of each sock, determine how many pairs of socks with matching colors there are.
        /// 
        /// Example: ar = [1, 2, 1, 2, 1, 3, 2]
        /// There is one pair of color 1 and one of color 2. There are three odd socks left, one of each color. The number of pairs is 2.
        /// </summary>
        /// <param name="ar">array of sock's color</param>
        /// <returns>amount of same-color sock pairs</returns>
        public static int SockMerchant(List<int> ar)
        {
            var groups = ar.GroupBy(g => g);
            var numberOfPairs = 0;
            foreach (var g in groups)
                numberOfPairs += g.Count() / 2;

            return numberOfPairs;
        }
    }
}
