﻿using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class Sorting
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
        /// Given an array of integers, sort the array in ascending order using the Bubble Sort algorithm. 
        /// 
        /// Once sorted, print the following three lines:
        /// Array is sorted in {numSwaps} swaps.
        /// First Element: {firstElement}, where  is the first element in the sorted array.
        /// Last Element: {lastElement}, where  is the last element in the sorted array.
        /// 
        /// Where:
        /// - {numSwaps} is the number of swaps that took place.
        /// - {firstElement} is the first element in the sorted array
        /// - {lastElement} is the last element in the sorted array
        /// </summary>
        /// <param name="a"></param>
        public static (long NumberOfSwaps, int FirstElement, int LastElement) CountBubbleSortSwaps(List<int> a)
        {
            var numberOfSwaps = a.BubbleSort();
            return (numberOfSwaps, a.First(), a.Last());
        }
    }
}
