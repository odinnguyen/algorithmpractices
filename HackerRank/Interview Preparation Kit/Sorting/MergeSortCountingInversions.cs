﻿using Phuong.Common.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class Sorting
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/ctci-merge-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
        /// In an array, arr, the elements at indices i and j (where i < j) form an inversion if arr[i] > arr[j]. 
        /// In other words, inverted elements arr[i] and arr[j] are considered to be "out of order". To correct an inversion, we can swap adjacent elements.
        /// 
        /// Example: arr=[2,4,1]
        /// To sort the array, we must perform the following two swaps to correct the inversions: swap(arr[1], arr[2]) -> swap(arr[0], arr[1])
        /// The sort has two inversions: (4,1) and (2,1).
        /// Given an array arr, return the number of inversions to sort the array.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static long CountMergeSortInversions(List<int> arr)
        {
            return arr.MergeSort();
        }
    }
}
