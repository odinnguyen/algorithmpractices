﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class Sorting
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/fraudulent-activity-notifications/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
        /// HackerLand National Bank has a simple policy for warning clients about possible fraudulent account activity. 
        /// If the amount spent by a client on a particular day is greater than or equal to 2x the client's median spending for a trailing number of days, they send the client a notification about potential fraud. 
        /// The bank doesn't send the client any notifications until they have at least that trailing number of prior days' transaction data.
        /// 
        /// Given the number of trailing days d and a client's total daily expenditures for a period of n days, determine the number of times the client will receive a notification over all n days.
        /// 
        /// Example: expenditures = [10,20,30,40,50], d = 3
        /// 
        /// On the first three days, they just collect spending data.
        /// 
        /// At day 4, trailing expenditures are [10,20,30].
        /// The median is 20 and the day's expenditure is 40. Because 40 > 2*20, there will be a notice. 
        /// 
        /// The next day, trailing expenditures are [20,30,40] and the expenditures are 50. This is less than 2*30 so no notice will be sent. 
        /// 
        /// Over the period, there was 1 notice sent.
        /// 
        /// Note: The median of a list of numbers can be found by first sorting the numbers ascending. 
        /// - If there is an odd number of values, the middle one is picked. 
        /// - If there is an even number of values, the median is then defined to be the average of the two middle values.
        /// </summary>
        /// <param name="expenditure">daily expenditures log</param>
        /// <param name="d">trailing number of days</param>
        /// <returns>number of activity notification</returns>
        public static int ActivityNotifications(List<int> expenditure, int d)
        {
            if (expenditure.Count <= d)
                return 0;

            var count = 0;
            //Ordered trailing
            var trailingList = expenditure.Take(d).OrderBy(e => e).ToList();
            for (int i = d; i < expenditure.Count; i++)
            {
                var median = trailingList.GetMedian();
                if (expenditure[i] * median.Factor >= median.Value * 2)
                    count++;

                var index = trailingList.BinarySearch(expenditure[i - d]);
                trailingList.RemoveAt(index);
                index = trailingList.BinarySearch(expenditure[i]);
                if (index < 0)
                    index = ~index;
                trailingList.Insert(index, expenditure[i]);
            }

            return count;
        }
    }
}
