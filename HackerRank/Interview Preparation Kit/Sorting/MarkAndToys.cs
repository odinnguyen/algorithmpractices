﻿using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class Sorting
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/mark-and-toys/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
        /// Mark and Jane are very happy after having their first child. Their son loves toys, so Mark wants to buy some. 
        /// There are a number of different toys lying in front of him, tagged with their prices. 
        /// Mark has only a certain amount to spend, and he wants to maximize the number of toys he buys with this money. 
        /// Given a list of toy prices and an amount to spend, determine the maximum number of gifts he can buy.
        /// 
        /// Note: Each toy can be purchased only once.
        /// 
        /// Example: prices = [1,2,3,4], budget k =7
        /// The budget is 7 units of currency. He can buy items that cost [1,2,3] for 6, or [3,4] for 7 units. The maximum is 3 items.
        /// </summary>
        /// <param name="prices"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static int MaximumToys(List<int> prices, int k)
        {
            var orderedPrices = prices.OrderBy(p => p);
            var sum = 0;
            var count = 0;
            foreach (var price in orderedPrices)
            {
                if (sum + price <= k)
                {
                    sum += price;
                    count++;
                }
                else
                    break;
            }
            return count;
        }
    }
}
