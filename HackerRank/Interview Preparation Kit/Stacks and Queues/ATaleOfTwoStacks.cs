﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class StacksAndQueues
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/ctci-queue-using-two-stacks/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=stacks-queues
        /// A queue is an abstract data type that maintains the order in which elements were added to it, allowing the oldest elements to be removed from the front and new elements to be added to the rear. This is called a First-In-First-Out (FIFO) data structure because the first element added to the queue (i.e., the one that has been waiting the longest) is always the first one to be removed.
        ///
        /// A basic queue has the following operations:
        /// - Enqueue: add a new element to the end of the queue.
        /// - Dequeue: remove the element from the front of the queue and return it.
        /// 
        /// In this challenge, you must first implement a queue using two stacks. Then process q queries, where each query is one of the following 3 types:
        ///
        /// 1 x: Enqueue element x into the end of the queue.
        /// 2: Dequeue the element at the front of the queue.
        /// 3: Print the element at the front of the queue.
        /// For example, a series of queries might be as follows:        
        ///         Query       Lifo        Fifo        Output
        ///         (1,35)      {35}        {35}    
        ///         (1,15)      {35,15}     {15,35}
        ///         (3)                                     35
        ///         (2)         {15}        {15}
        ///         (3)                                     15
        ///         Return an array with the output: [35,15].
        /// </summary>
        /// <param name="queries"></param>
        /// <returns></returns>
        public static IEnumerable<int> ATaleOfTwoStacks(List<List<int>> queries)
        {
            var queue = new Queue<int>();
            var stack = new Stack<int>();

            foreach (var query in queries)
            {
                switch (query[0])
                {
                    case 1: //Add
                        queue.Enqueue(query[1]);
                        stack.Push(query[1]);
                        break;
                    case 2: //Remove
                        queue.Dequeue(); 
                        stack.Pop();
                        break;
                    case 3: //Print First Element
                        yield return queue.Peek();
                        break;
                }
            }
        }
    }
}
