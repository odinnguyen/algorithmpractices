﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class StacksAndQueues
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/balanced-brackets/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=stacks-queues
        /// A bracket is considered to be any one of the following characters: (, ), {, }, [, or ].
        ///
        /// Two brackets are considered to be a matched pair if the an opening bracket(i.e., (, [, or {) occurs to the left of a closing bracket(i.e., ), ], or }) of the exact same type.
        /// There are three types of matched pairs of brackets: [], {}, and().
        ///
        /// A matching pair of brackets is not balanced if the set of brackets it encloses are not matched. 
        /// For example, {[(])} is not balanced because the contents in between { and } are not balanced. 
        /// The pair of square brackets encloses a single, unbalanced opening bracket, (, and the pair of parentheses encloses a single, unbalanced closing square bracket, ].
        ///
        /// By this logic, we say a sequence of brackets is balanced if the following conditions are met:
        /// - It contains no unmatched brackets.
        /// - The subset of brackets enclosed within the confines of a matched pair of brackets is also a matched pair of brackets.
        /// 
        /// Given n strings of brackets, determine whether each sequence of brackets is balanced. If a string is balanced, return true. Otherwise, return false.
        /// </summary>
        /// <param name="s">string</param>
        /// <returns>true if brackets are balanced</returns>
        public static bool IsBalanced(string s)
        {
            var stack = new Stack<char>();
            List<(char Open, char Close)> bracketPairs = new List<(char Open, char Close)> { ('(', ')'), ('{', '}'), ('[', ']') };
            foreach (var c in s)
            {
                if (bracketPairs.Any(p => p.Open == c))
                    stack.Push(c);
                else
                {
                    var matchedBracketPair = bracketPairs.First(p => p.Close == c);
                    if (!stack.Any() || stack.Peek() != matchedBracketPair.Open)
                        return false;
                    else
                        stack.Pop();
                }
            }

            if (!stack.Any())
                return true;
            else
                return false;
        }
    }
}
