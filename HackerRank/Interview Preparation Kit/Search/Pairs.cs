﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class Search
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/pairs/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search
        /// Given an array of unique integers and a target value, 
        /// determine the number of pairs of array elements that have a difference equal to the target value.
        /// 
        /// Example: k = 1, arr = [1,2,3,4]
        /// There are three values that differ by k=1: 2-1=1, 3-2=1, and 4-3=1. Return 3.
        /// </summary>
        /// <param name="k">target value</param>
        /// <param name="arr">arr of unique integers</param>
        /// <returns>number of pairs</returns>
        public static int CountPairs(int k, List<int> arr)
        {
            var hashSet = new HashSet<int>();
            foreach (var number in arr)
                hashSet.Add(number);

            var count = 0;
            foreach (var number in arr)
            {
                if (hashSet.Contains(number + k))
                    count++;

                if (hashSet.Contains(number - k))
                    count++;

                hashSet.Remove(number);
            }
            return count;
        }
    }
}
