﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class Search
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/ctci-ice-cream-parlor/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search
        /// 
        /// Note: 
        /// - Two ice creams having unique IDs i and j may have the same cost (i.e., cost[i]=cost[j]).
        /// - There will always be a unique solution.
        /// =>  From those two conditions, mean when there is an arbitrary i and j where cost[i]=cost[j], the answer will always be i and j. 
        ///     Because if the answer is (i,k), there will be anotehr answer (j,k), which make it not unique solution.
        /// </summary>
        /// <param name="cost"></param>
        /// <param name="money"></param>
        /// <returns>Ids of the two ice-creams</returns>
        public static (int Id1, int Id2) WhatFlavors(List<int> cost, int money)
        {
            var costDictionary = new Dictionary<int, int>();
            for (int i = 0; i < cost.Count; i++)
            {
                var target = money - cost[i];
                if (costDictionary.ContainsKey(target))
                    return (costDictionary[target], i);
                else
                    costDictionary[cost[i]] = i;
            }

            return (-1, -1);
        }
    }
}
