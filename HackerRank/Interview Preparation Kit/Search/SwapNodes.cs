﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    public partial class Search
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/swap-nodes-algo/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search
        /// Swap Left&Right nodes at the depth of multiplicatives of "query"
        /// 
        /// Example: q = 2, swaps node at depth 2,4,6,...,k*q, where k is integer
        /// </summary>
        /// <param name="indexes">Value pair of left right nodes in breadth first search manner</param>
        /// <param name="queries">List of query</param>
        /// <returns>List of in-order traversal search for each query</returns>
        public static List<List<T>> SwapNodes<T>(T rootValue, List<(T Left, T Right)> indexes, List<int> queries)
        {
            TreeNode<T> root = new TreeNode<T>(rootValue);
            root.BuildBinaryTree(indexes.ToQueue());

            var inOrderTraversals = new List<List<T>>();
            foreach (var query in queries)
            {
                SwapNodes(root, query);

                var dfsResult = root.InorderTraversal()
                    .Select(n => n.Data)
                    .ToList();

                inOrderTraversals.Add(dfsResult);
            }

            return inOrderTraversals;
        }

        /// <summary>
        /// Swap children of nodes at the depth of multiplicatives of "query"
        /// 
        /// Example: q = 2, swaps children of node at depth 2,4,6,...,k*q, where k is integer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="root"></param>
        /// <param name="query"></param>
        public static void SwapNodes<T>(TreeNode<T> node, int query, int depth = 1)
        {
            if (depth % query == 0)
                node.Swap();

            if (node.Left != null)
                SwapNodes(node.Left, query, depth + 1);

            if (node.Right != null)
                SwapNodes(node.Right, query, depth + 1);
        }
    }
}
