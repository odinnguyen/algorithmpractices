﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class Arrays
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/2d-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
        /// Given a 6x6 2D Array, arr:
        /// 1 1 1 0 0 0
        /// 0 1 0 0 0 0
        /// 1 1 1 0 0 0
        /// 0 0 0 0 0 0
        /// 0 0 0 0 0 0
        /// 0 0 0 0 0 0
        /// 
        /// An hourglass in A is a subset of values with indices falling in this pattern in arr's graphical representation:
        /// a b c
        ///   d
        /// e f g
        /// 
        /// There are 16 hourglasses in arr. An hourglass sum is the sum of an hourglass' values. 
        /// Calculate the hourglass sum for every hourglass in arr, then print the maximum hourglass sum. The array will always be 6x6.
        /// 
        /// Example: 
        /// arr=
        /// -9 -9 -9  1 1 1 
        ///  0 -9  0  4 3 2
        /// -9 -9 -9  1 2 3
        ///  0  0  8  6 6 0
        ///  0  0  0 -2 0 0
        ///  0  0  1  2 4 0
        ///  
        /// The 16 hourglass sums are:
        /// -63, -34, -9, 12, 
        /// -10,   0, 28, 23, 
        /// -27, -11, -2, 10, 
        ///   9,  17, 25, 18
        ///   
        /// The highest hourglass sum is 28 from the hourglass beginning at row 1, column 2:
        /// 0 4 3
        ///   1
        /// 8 6 6
        /// </summary>
        /// <param name="arr">2D array</param>
        /// <returns>Highest hourglass sum</returns>
        public static int HourglassSum(List<List<int>> arr)
        {
            var max = int.MinValue;
            for (int i = 0; i < arr.Count - 2; i++)
                for (int j = 0; j < arr[i].Count - 2; j++)
                {
                    var sum = arr[i][j] + arr[i][j + 1] + arr[i][j + 2] +
                                        +arr[i + 1][j + 1] +
                            arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2];

                    if (max < sum)
                        max = sum;
                }

            return max;
        }
    }
}
