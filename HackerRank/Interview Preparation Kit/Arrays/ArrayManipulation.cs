﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class Arrays
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/crush/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
        /// Starting with a 1-indexed array of zeros and a list of operations, 
        /// for each operation add a value to each the array element between two given indices, inclusive. 
        /// Once all operations have been performed, return the maximum value in the array.
        /// 
        /// Example: n=10, queries = [[1,5,3],[4,8,7],[6,9,1]]
        /// 
        /// Queries are interpreted as follows:
        /// a b k
        /// 1 5 3
        /// 4 8 7
        /// 6 9 1
        /// 
        /// Add the values of k between the indices a and b inclusive:
        ///
        /// index->	 1 2 3  4  5 6 7 8 9 10
        ///         [0,0,0, 0, 0,0,0,0,0, 0]
        ///         [3,3,3, 3, 3,0,0,0,0, 0]
        ///         [3,3,3,10,10,7,7,7,0, 0]
        ///         [3,3,3,10,10,8,8,8,1, 0]
        /// The largest value is 10 after all operations are performed.
        /// 
        /// Explanation of algorithm:
        /// 
        /// For every input line of a-b-k, you are given the range (a to b) where the values increase by k. 
        /// So instead of keeping track of actual values increasing, just keep track of the rate of change (i.e. a slope) in terms of where the rate started its increase and where it stopped its decrease. 
        /// This is done by adding k to the "a" position of your array and adding -k to the "b+1" position of your array for every input line a-b-k, and that's it. 
        /// "b+1" is used because the increase still applied at "b".
        /// 
        /// The maximum final value is equivalent to the maximum accumulated "slope" starting from the first position, because it is the spot which incremented more than all other places.
        /// Accumulated "slope" means to you add slope changes in position 0 to position 1, then add that to position 2, and so forth, looking for the point where it was the greatest.
        /// </summary>
        /// <param name="n">length of the array</param>
        /// <param name="queries">List of queries</param>
        /// <returns>highest number in the result array</returns>
        public static long ArrayManipulation(int n, List<List<int>> queries)
        {
            long[] array = new long[n + 1];
            foreach (var query in queries)
            {
                int a = query[0];
                int b = query[1];
                int k = query[2];
                array[a - 1] += k;
                array[b] -= k;
            }

            long max = 0;
            long currentHeight = 0;
            for (int i = 0; i < n; i++)
            {
                currentHeight += array[i];
                if (currentHeight > max)
                    max = currentHeight;
            }

            return max;
        }
    }
}
