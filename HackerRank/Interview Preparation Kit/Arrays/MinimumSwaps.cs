﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class Arrays
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/minimum-swaps-2/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
        /// You are given an unordered array consisting of consecutive integers [1, 2, 3, ..., n] without any duplicates. 
        /// You are allowed to swap any two elements. 
        /// Find the minimum number of swaps required to sort the array in ascending order.
        /// </summary>
        /// <param name="list">array</param>
        /// <returns>Minium number of swaps</returns>
        public static int MinimumSwaps(IList<int> list)
        {
            var numberOfSwaps = 0;
            var index = 0;
            while (index < list.Count)
            {
                if (list[index] != index + 1)
                {
                    list.Swap(list[index] - 1, index);
                    numberOfSwaps++;
                }
                else
                    index++;
            }

            return numberOfSwaps;
        }
    }
}
