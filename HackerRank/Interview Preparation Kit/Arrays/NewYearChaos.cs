﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class Arrays
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
        /// It is New Year's Day and people are in line for the Wonderland rollercoaster ride. 
        /// Each person wears a sticker indicating their initial position in the queue from 1 to n. 
        /// Any person can bribe the person directly in front of them to swap positions, but they still wear their original sticker. 
        /// One person can bribe at most two others.
        /// 
        /// Determine the minimum number of bribes that took place to get to a given queue order.
        /// Print the number of bribes, or, if anyone has bribed more than two people, print "Too chaotic".
        /// 
        /// Let's begin:
        /// 1. First, to eliminate the chaotic cases 
        /// - obviously any person who is more than 2 positions ahead of their initial position cannot be there since they could not have bribed more than twice.
        /// 
        /// 2. How can I know how many times have I been bribed if I am the one standing in the queue?
        /// - Every person with a number greater than me who is now standing ahead of me must have bribed me at some point.
        /// - So starting from one position in front of your current position, 
        ///   check through the row in front of you and count the instances of numbers greater than yours. 
        ///   Hence, Instances of numbers greater than you in front of you = Bribe count.
        /// 
        /// 3. Lastly, Do we really have to check everyone in our front?
        /// 
        /// 1 2 3 4 5 6 7 8 9 10
        /// 1 2 3 5 6 7 8 9 10 4 : 6 bribes
        /// 1 2 5 3 6 7 8 9 10 4 : 7 bribes
        /// 1 5 2 3 6 7 8 9 10 4 : 8 bribes BUT 5 has now bribed three people - 4,3,2 so this won't happen.
        /// 
        /// So you only needed to check till one position in front of your original position.
        /// That will be in this instance q[3] - 2 = 4 -2 = 2 (indexing starting from 0, initial position of 4 is 3)
        /// </summary>
        /// <param name="q">queue of people in line.</param>
        /// <returns>number of bribes</returns>
        public static string MinimumBribes(List<int> q)
        {
            var numberOfBribes = 0;
            for (int i = q.Count - 1; i >= 0; --i)
            {
                if (q[i] - (i + 1) > 2)
                    return "Too chaotic";

                for (int j = i - 1; j >= Math.Max(q[i] - 2, 0); --j)
                    if (q[j] > q[i]) numberOfBribes++;
            }

            return numberOfBribes.ToString();
        }
    }
}
