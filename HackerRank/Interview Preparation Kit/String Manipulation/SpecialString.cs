﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class StringManipulation
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/special-palindrome-again/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings&h_r=next-challenge&h_v=zen
        /// A string is said to be a special string if either of two conditions is met:
        /// - All of the characters are the same, e.g.aaa.
        /// - All characters except the middle one are the same, e.g.aadaa.
        /// 
        /// A special substring is any substring of a string which meets one of those criteria.
        /// Given a string, determine how many special substrings can be formed from it.
        /// 
        /// Example: s = mnonopoo has 12 special substring
        /// {m, n, o, n, o, p, o, o, non, ono, opo, oo}
        /// </summary>
        /// <param name="s">a string</param>
        /// <returns>Number of special substring</returns>
        public static long SpecialSubstringCount(string s)
        {
            if (s.Length == 0)
                return 0;

            var start = 0;
            var specialCharacter = s[0];
            long specialSubstringCount = 0;
            while (start < s.Length)
            {
                var end = start;

                //for one character substring
                specialCharacter = s[start];
                specialSubstringCount++;

                //for substrings of all the same characters
                while (end < s.Length - 1 && s[++end] == s[start])
                    specialSubstringCount++;

                //for substring where middle character is different
                var middle = end;
                if (middle != start)
                {
                    end = middle + (middle - start);
                    if (end < s.Length && s.Substring(middle + 1, end - middle).All(c => c == s[start]))
                        specialSubstringCount++;
                }

                start++;
            }

            return specialSubstringCount;
        }
    }
}
