﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    public partial class StringManipulation
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/alternating-characters/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
        /// The string contains only 'A' and 'B'.
        /// Returns the least amount of deletion that will make the string the alternation of 'A' and 'B'. aka ABABABABAB 
        /// </summary>
        /// <param name="s">string with only 'A' and 'B'</param>
        /// <returns>Returns the least amount of deletions  to make alternative string</returns>
        public static int AlternatingCharacters(string s)
        {
            var numberOfDeletions = 0;
            for (int i = 1; i < s.Length; i++) 
                if (s[i] == s[i - 1])
                    numberOfDeletions++;

            return numberOfDeletions;
        }
    }
}
