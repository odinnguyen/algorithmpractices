﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;

    public partial class StringManipulation
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/ctci-making-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
        /// Find the least number of deletions of characters that will make the two strings anagrams
        /// </summary>
        /// <param name="a">string a</param>
        /// <param name="b">string b</param>
        /// <returns>total number of deletion</returns>
        public static int MakeAnagram(string a, string b)
        {
            var dictionaryA = a.ToAlphabetDictionary();
            var dictionaryB = b.ToAlphabetDictionary();

            var totalDeletions = 0;
            for(char c = 'a'; c <= 'z'; c++)
            {
                var countA = dictionaryA.ContainsKey(c) ? dictionaryA[c] : 0;
                var countB = dictionaryB.ContainsKey(c) ? dictionaryB[c] : 0;
                totalDeletions += Math.Abs(countA - countB);
            }

            return totalDeletions;
        }
    }
}
