﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class StringManipulation
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/common-child/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
        /// https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
        /// Generate tracking matrix for Common Subsequence
        /// 
        /// Example: 
        /// 
        /// a="AGCAT", b="GAC"
        /// Matrix:
        /// 	    Ø	A	G	C	A	T
        ///     Ø	0	0	0	0	0	0
        ///     G	0	0	1	1	1	1
        ///     A	0	1	1	1	2	2
        ///     C	0	1	1	2	2	2
        /// </summary>
        /// <param name="a">string a</param>
        /// <param name="b">string b</param>
        /// <returns>(maxtrix of Common Subsequence, array characters represent x-Axis, array characters represent y-Axis)</returns>
        static (int[,] Matrix, char[] ArrayX, char[] ArrayY) GetCommonSubsequenceMatrix(string a, string b)
        {
            //array characters of string a, represent x-Axis
            var x = a.ToCharArray();

            //array characters of string b, represent y-Axis
            var y = b.ToCharArray();

            //Common Subsequence Matrix
            int[,] csm = new int[x.Length + 1, y.Length + 1]; // (a, b).Length + 1

            //Initiate x-Axis
            for (int i = 0; i <= x.Length; i++)
                csm[i, 0] = 0;

            //Initiate y-Axis
            for (int j = 0; j <= y.Length; j++)
                csm[0, j] = 0;

            //Build matrix
            for (int i = 1; i <= x.Length; i++)
                for (int j = 1; j <= y.Length; j++)
                {
                    //Note: matrix[i.j] is represented by x[i-1], y[j-1]. Because first row and first column are all zeros
                    if (x[i - 1] == y[j - 1])
                        csm[i, j] = csm[i - 1, j - 1] + 1;
                    else
                        csm[i, j] = Math.Max(csm[i, j - 1], csm[i - 1, j]);
                }
            return (csm, x, y);
        }

        /// <summary>
        /// https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
        /// Finding the Longest Common Subsequence of two strings using back tracking from (x,y) to (0,0)
        /// This method favour going left horizontally than going up vertically
        /// </summary>
        /// <param name="lcsMatrix">Longest Common Subsequence Matrix</param>
        /// <param name="xArray">array characters represent x-Axis</param>
        /// <param name="yArray">array characters represent y-Axis</param>
        /// <param name="x">x coordination of backtracking starting point</param>
        /// <param name="y">y coordination of backtracking starting point</param>
        /// <returns>Longest Common Subsequence from (x,y) to (0,0)</returns>
        static string Backtrack(int[,] lcsMatrix, char[] xArray, char[] yArray, int x, int y)
        {
            if (x == 0 || y == 0)
                return "";

            //Note: matrix[x.y] is represented by xArray[x-1], yArray[y-1]. Because first row and first column are all zeros
            if (xArray[x - 1] == yArray[y - 1])
                //if current position have the same character, we back track to the previous position of both x and y, aka moving to upper-left
                return Backtrack(lcsMatrix, xArray, yArray, x - 1, y - 1) + xArray[x - 1];

            //if moving left resulting in smaller number, aka common subsequence broken, we will move up.
            if (lcsMatrix[x, y - 1] > lcsMatrix[x - 1, y])
                return Backtrack(lcsMatrix, xArray, yArray, x, y - 1);

            return Backtrack(lcsMatrix, xArray, yArray, x - 1, y);
        }

        /// <summary>
        /// Finding the Longest Common Subsequence of two strings using back tracking
        /// </summary>
        /// <param name="lcsMatrix">Longest Common Subsequence Matrix</param>
        /// <param name="xArray">array characters represent x-Axis</param>
        /// <param name="yArray">array characters represent y-Axis</param>
        /// <returns>Longest Common Subsequence</returns>
        static string Backtrack(int[,] lcsMatrix, char[] xArray, char[] yArray)
        {
            return Backtrack(lcsMatrix, xArray, yArray, xArray.Length, yArray.Length);
        }

        /// <summary>
        /// https://www.hackerrank.com/challenges/common-child/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
        /// A string is said to be a child of a another string if it can be formed by deleting 0 or more characters from the other string. 
        /// Letters cannot be rearranged. 
        /// Given two strings of equal length, what's the longest string that can be constructed such that it is a child of both?
        /// 
        /// Example:
        /// s1 = "ABCD"
        /// s1 = "ABDC"
        /// These strings have two children with maximum length 3, ABC and ABD.They can be formed by eliminating either the D or C from both strings.
        /// Return 3
        /// </summary>
        /// <param name="s1">string 1, all uppper case</param>
        /// <param name="s2">string 2, all uppper case</param>
        /// <returns>Length of the longest common child</returns>
        public static int CommonChild(string s1, string s2)
        {
            var commonSubsequences = GetCommonSubsequenceMatrix(s1, s2);
            var mostCommonString = Backtrack(commonSubsequences.Matrix, commonSubsequences.ArrayX, commonSubsequences.ArrayY);
            return mostCommonString.Length;
        }
    }
}
