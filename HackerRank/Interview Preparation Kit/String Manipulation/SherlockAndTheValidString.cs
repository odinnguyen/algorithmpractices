﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Enums;
    using Phuong.Common.Helpers;
    public partial class StringManipulation
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/sherlock-and-valid-string/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
        /// Sherlock considers a string to be valid if all characters of the string appear the same number of times. 
        /// 
        /// It is also valid if he can remove just ONE character at ONE index in the string, 
        /// and the remaining characters will occur the same number of times. 
        /// 
        /// Given a string , determine if it is valid. If so, return YES, otherwise return NO.
        /// 
        /// Example: 
        /// - s='abc' is valid
        /// - s='abcc' is valid
        /// - s='abccc' is invalid
        /// </summary>
        /// <param name="s">string with only 'A' and 'B'</param>
        /// <returns>Returns the least amount of deletions  to make alternative string</returns>
        public static string IsValid(string s)
        {
            var countDictionary = s.ToCountDictionary();

            if (countDictionary.Keys.Count() == 1)
                return EnumYesAndNo.Yes.ToString().ToUpperInvariant();

            if (countDictionary.Keys.Count() == 2 
                && countDictionary.Keys.Min() == 1
                && countDictionary[countDictionary.Keys.Min()].Count == 1)
                return EnumYesAndNo.Yes.ToString().ToUpperInvariant();


            if (countDictionary.Keys.Count() == 2
                && countDictionary.Keys.Max() == countDictionary.Keys.Min() + 1
                && countDictionary[countDictionary.Keys.Max()].Count == 1)
                return EnumYesAndNo.Yes.ToString().ToUpperInvariant();

            return EnumYesAndNo.No.ToString().ToUpperInvariant();
        }
    }
}
