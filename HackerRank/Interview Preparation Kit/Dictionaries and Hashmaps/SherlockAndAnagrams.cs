﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class DictionariesAndHashmaps
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
        /// Two strings are anagrams of each other if the letters of one string can be rearranged to form the other string. 
        /// Given a string, find the number of pairs of substrings of the string that are anagrams of each other.
        /// </summary>
        /// <param name="s">string</param>
        /// <returns>numberOfAnagrams</returns>
        public static int SherlockAndAnagrams(string s)
        {
            var numberOfAnagrams = 0;
            var anagramsDictionary = new Dictionary<string, int>();
            for (int i = 0; i < s.Length; i++)
                for (int k = 1; k <= s.Length - i; k++)
                {
                    var subStr = s.Substring(i, k);
                    var anagram = new string(subStr.OrderBy(c => c).ToArray());
                    if (anagramsDictionary.ContainsKey(anagram))
                        anagramsDictionary[anagram]++;
                    else
                        anagramsDictionary[anagram] = 1;
                }

            //If there are n occurences of a given anagrams. There will be (n * (n - 1) / 2) pairs of that anagrams
            //Example:
            //n = 4 anagrams of 4 substring A, B, C, D
            //there will be 4*3/2 = 6 pairs of anagrams: (A,B), (A,C), (A,D), (B,C), (B,D), (C,D)
            //Ref:https://en.wikipedia.org/wiki/Arithmetic_progression
            foreach (var key in anagramsDictionary.Keys.Where(k => anagramsDictionary[k] > 1))
            {
                var n = anagramsDictionary[key] - 1;
                numberOfAnagrams += (n + 1) * n / 2;
            }

            return numberOfAnagrams;
        }
    }
}
