﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class DictionariesAndHashmaps
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/two-strings/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
        /// Given two strings, determine if they share a common substring. A substring may be as small as one character.
        /// Example: 
        /// - s1="and", s2="art". These share the common substring a.
        /// - s1="be, s2="cat". These do not share a substring.
        /// </summary>
        /// <param name="s1">string 1</param>
        /// <param name="s2">string 2</param>
        /// <returns>true if have common substring</returns>
        public static bool TwoStrings(string s1, string s2)
        {
            var hash1 = s1.ToHashSet();
            var hash2 = s2.ToHashSet();
            if (hash1.Any(c1 => hash2.Contains(c1)))
                return true;
            else return false;
        }
    }
}
