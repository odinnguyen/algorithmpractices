﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class DictionariesAndHashmaps
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/count-triplets-1/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
        /// You are given an array and you need to find number of tripets of indices (i, j, k) 
        /// such that the elements at those indices are in geometric progression for a given common ratio r and i < j < k  =
        /// Note: r can be 1 or all elements of array can be 1 or Both.
        /// 
        /// Example: arr = [1, 4, 16, 64], r = 4
        /// There are (1,4,16) and (4,16,65) at indices (0,1,2) and (1,2,3). Return 2.
        /// 
        /// Explanation of algorithm:
        /// Let's take an example to undertsand the core concept behind this problem : {1, 3, 3, 9, 3, 27, 81} . Let common ratio = 3.
        /// 1.  We will consider every element to be our middle element during our iteration.
        ///     For any such element a, the number of possible geometric pairs possible are, (no. of a/r on the left of a) x (no.of axr on the right of a).
        /// 2.  Lets take 9 as our element. 9/3 = 3. 9x3 = 27.
        ///     The number of 3s present on left of 9 are 2. The number of 27s present on right of 9 is 1. 
        ///     Hence total no.of geometric pairs possible with 9 as the middle element is 2x1 = 2.
        ///     Do this for all the elements and add them up to get the result.
        /// 3.  We create an occurence map first and call it rightMap. 
        ///     Now for each element, we first decrement it's count by 1 from the rightMap. 
        ///     Now we check for the number of occurences of axr in the rightMap.
        ///   - Example, say the input is {1,1,1,1 } and ratio = 1, rightMap will be [1->4]. 
        ///     Now we need to check the number of times axr = 1 occurs on the right of a. 
        ///     We do this after decrementing the count of current value by 1 in the rightMap. So rightMap becomes [1->3]. 
        ///     3 is the number of times aXr occurs on RHS of first 1
        /// 4.  Now we check for a/r counts in left map. We multiply leftCount and rightCount and add them up for each element. 
        /// 
        /// The whole idea is that, for each element
        ///   - leftMap contains the occurence map of the elements on the left of a
        ///   - rightMap contains the occurence map of the elements on the right of a.
        /// </summary>
        /// <param name="list">List of integers</param>
        /// <param name="r">ratio</param>
        /// <returns>Triplets count</returns>
        public static long CountTriplets(List<long> list, long r)
        {
            Dictionary<long, long> rightMap = GetOccurenceMap(list);
            Dictionary<long, long> leftMap = new Dictionary<long, long>();
            long numberOfGeometricPairs = 0;

            foreach (long val in list)
            {
                long countLeft = 0;
                long countRight = 0;
                long lhs = 0;
                long rhs = val * r;
                if (val % r == 0)
                    lhs = val / r;

                rightMap[val]--;

                if (rightMap.ContainsKey(rhs))
                    countRight = rightMap[rhs];

                if (leftMap.ContainsKey(lhs))
                    countLeft = leftMap[lhs];

                numberOfGeometricPairs += countLeft * countRight;
                InsertIntoMap(leftMap, val);
            }
            return numberOfGeometricPairs;
        }

        /// <summary>
        /// Create occurence map for each value in the set and their counts
        /// </summary>
        /// <param name="list">list of value</param>
        /// <returns>Occurence Map</returns>
        private static Dictionary<long, long> GetOccurenceMap(List<long> list)
        {
            Dictionary<long, long> occurenceMap = new Dictionary<long, long>();
            foreach (long val in list)
                InsertIntoMap(occurenceMap, val);

            return occurenceMap;
        }

        /// <summary>
        /// Insert a value into occurrence map and increase count of that value
        /// </summary>
        /// <param name="occurenceMap">occurenceMap</param>
        /// <param name="val">value</param>
        private static void InsertIntoMap(Dictionary<long, long> occurenceMap, long val)
        {
            if (!occurenceMap.ContainsKey(val))
                occurenceMap[val] = 1;
            else
                occurenceMap[val]++;
        }
    }
}
