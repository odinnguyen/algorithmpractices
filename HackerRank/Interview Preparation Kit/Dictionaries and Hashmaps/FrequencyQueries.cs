﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Interview_Preparation_Kit
{
    using Phuong.Common.Helpers;
    public partial class DictionariesAndHashmaps
    {
        /// <summary>
        /// https://www.hackerrank.com/challenges/frequency-queries/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
        /// You are given q queries. Each query is of the form two integers described below:
        /// 1 - x : Insert x in your data structure.
        /// 2 - y : Delete one occurence of y from your data structure, if present.
        /// 3 - z : Check if any integer is present whose frequency is exactly z.If yes, print 1 else 0.
        /// 
        /// Example: queries =[(1,1),(2,2),(3,2),(1,1),(1,1),(2,1),(3,2)]
        /// The results of each operation are:
        ///         Operation   Array   Output
        ///         (1,1)       [1]
        ///         (2,2)       [1]
        ///         (3,2)                   0
        ///         (1,1)       [1,1]
        ///         (1,1)       [1,1,1]
        ///         (2,1)       [1,1]
        ///         (3,2)                   1
        ///         Return an array with the output: [0,1].
        /// </summary>
        /// <param name="queries"></param>
        /// <returns>List of queries' results</returns>
        public static List<int> FrequencyQueries(List<List<int>> queries)
        {
            var result = new List<int>();
            var dictionary = new Dictionary<int, int>(); //<element, count>
            var dictionaryCount = new Dictionary<int, HashSet<int>>(); //<count, element>
            foreach (var query in queries)
            {
                switch (query[0])
                {
                    case 1:
                        if (dictionary.ContainsKey(query[1]))
                        {
                            dictionaryCount[dictionary[query[1]]].Remove(query[1]);

                            dictionary[query[1]]++;

                            if (!dictionaryCount.ContainsKey(dictionary[query[1]]))
                                dictionaryCount[dictionary[query[1]]] = new HashSet<int>();
                            dictionaryCount[dictionary[query[1]]].Add(query[1]);
                        }
                        else
                        {
                            dictionary[query[1]] = 1;

                            if (!dictionaryCount.ContainsKey(dictionary[query[1]]))
                                dictionaryCount[dictionary[query[1]]] = new HashSet<int>();
                            dictionaryCount[1].Add(query[1]);
                        }
                        break;
                    case 2:
                        if (dictionary.ContainsKey(query[1]) && dictionary[query[1]] > 0)
                        {
                            dictionaryCount[dictionary[query[1]]].Remove(query[1]);
                            dictionary[query[1]]--;

                            if (!dictionaryCount.ContainsKey(dictionary[query[1]]))
                                dictionaryCount[dictionary[query[1]]] = new HashSet<int>();
                            dictionaryCount[dictionary[query[1]]].Add(query[1]);
                        }
                        break;
                    case 3:
                        if (dictionaryCount.ContainsKey(query[1]) && dictionaryCount[query[1]].Count() > 0)
                            result.Add(1);
                        else
                            result.Add(0);
                        break;
                }
            }
            return result;
        }
    }
}
