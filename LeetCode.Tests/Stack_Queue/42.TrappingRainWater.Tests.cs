using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class TrappingRainWaterTests
    {
        void RunTest(TrappingRainWater.ISolution solution)
        {
            int result;

            result = solution.Trap(new int[] { 0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1 });
            Assert.AreEqual(6, result);

            result = solution.Trap(new int[] { 4, 2, 0, 3, 2, 5 });
            Assert.AreEqual(9, result);

            result = solution.Trap(new int[] { 1, 0, 3, 0, 1, 0, 2, 0, 4 });
            Assert.AreEqual(13, result);

            result = solution.Trap(new int[] { });
            Assert.AreEqual(0, result);

            result = solution.Trap(null);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void TrappingRainWater_MySolution()
        {
            var solution = new TrappingRainWater.MySolution();
            RunTest(solution);
        }
    }
}
