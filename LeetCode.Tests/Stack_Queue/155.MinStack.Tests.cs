using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace LeetCode.Tests
{
    using static LeetCode.MinStack;

    [TestClass]
    public class MinStackTests
    {
        void RunTest1(IMinStack<int> minStack)
        {
            minStack.Push(-2);
            minStack.Push(0);
            minStack.Push(-3);
            Assert.AreEqual(-3, minStack.GetMin());
            minStack.Pop();
            Assert.AreEqual(0, minStack.Top());
            Assert.AreEqual(-2, minStack.GetMin());
        }

        void RunTest2(IMinStack<int> minStack)
        {
            var operations = new string[] { "push", "push", "push", "getMin", "push", "push", "push", "push", "push", "top", "push", "push", "getMin", "push", "getMin", "push", "push", "getMin", "push", "top", "push", "getMin", "push", "push", "push", "push", "getMin", "push", "push", "top", "push", "push", "getMin", "pop", "getMin", "push", "push", "getMin", "getMin", "push", "getMin", "pop", "push", "push", "push", "getMin", "push", "getMin", "getMin", "getMin", "pop", "getMin", "push", "push", "getMin", "top", "getMin", "push", "getMin", "getMin", "getMin", "getMin", "push", "getMin", "getMin", "getMin", "push", "getMin", "push", "getMin", "push", "getMin", "getMin", "getMin", "getMin", "pop", "getMin", "push", "getMin", "getMin", "push", "push", "pop", "push", "getMin", "push", "top", "top", "push", "push", "getMin", "pop", "getMin", "push", "top", "push", "getMin", "push", "getMin", "getMin" };
            var oprands = new string("85,-99,67,,-27,61,-97,-27,35,,99,-66,,-89,,4,-70,,52,,54,,94,-41,-75,-32,,5,29,,-78,-74,,,,-58,-44,,,-64,,,-45,-99,-27,,-96,,,,,,26,-58,,,,25,,,,,33,,,,71,,-62,,-78,,,,,,,-30,,,85,-15,,-40,,72,,,18,59,,,,-59,,10,,9,,").Split(',');
            var expected = new int?[] { null, null, null, -99, null, null, null, null, null, 35, null, null, -99, null, -99, null, null, -99, null, 52, null, -99, null, null, null, null, -99, null, null, 29, null, null, -99, null, -99, null, null, -99, -99, null, -99, null, null, null, null, -99, null, -99, -99, -99, null, -99, null, null, -99, -58, -99, null, -99, -99, -99, -99, null, -99, -99, -99, null, -99, null, -99, null, -99, -99, -99, -99, null, -99, null, -99, -99, null, null, null, null, -99, null, 72, 72, null, null, -99, null, -99, null, -59, null, -99, null, -99, -99 };
            
            for (var i = 0; i < operations.Length; i++)
                switch (operations[i]) 
                {
                    case "push":
                        minStack.Push(Convert.ToInt32(oprands[i]));
                        Assert.IsNull(expected[i]);
                        break;
                    case "pop":
                        minStack.Pop();
                        Assert.IsNull(expected[i]);
                        break;
                    case "getMin":
                        var min = minStack.GetMin();
                        Assert.AreEqual(expected[i], min);
                        break;
                    case "top":
                        var top = minStack.Top();
                        Assert.AreEqual(expected[i], top);
                        break;
                };
        }

        [TestMethod]
        public void MinStack_MySolution()
        {
            RunTest1(new MinStack.MySolution.MinStack());
            RunTest2(new MinStack.MySolution.MinStack());
        }
    }
}
