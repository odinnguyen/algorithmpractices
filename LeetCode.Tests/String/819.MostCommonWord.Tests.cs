using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class MostCommonWordTests
    {
        void RunTest(MostCommonWord.ISolution solution)
        {
            var result1 = solution.MostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.", new string[] { "hit" });
            Assert.AreEqual("ball", result1);

            var result2 = solution.MostCommonWord("Bob. hIt, baLl", new string[] { "bob", "hit" });
            Assert.AreEqual("ball", result2);

            var result3 = solution.MostCommonWord("a.", new string[] { });
            Assert.AreEqual("a", result3);
        }

        [TestMethod]
        public void MostCommonWord_MySolution1()
        {
            var solution = new MostCommonWord.MySolution1();
            RunTest(solution);
        }

        [TestMethod]
        public void MostCommonWord_MySolution2()
        {
            var solution = new MostCommonWord.MySolution2();
            RunTest(solution);
        }
    }
}
