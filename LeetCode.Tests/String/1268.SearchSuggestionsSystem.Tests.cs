using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class SearchSuggestionsSystemTests
    {
        void RunTest(SearchSuggestionsSystem.ISolution solution)
        {
            var result1 = solution.SuggestedProducts(new string[] { "mobile", "mouse", "moneypot", "monitor", "mousepad" }, "mouse");
            Assert.IsTrue(result1.IsEqual(
                new List<IList<string>> {
                    new List<string> {"mobile","moneypot","monitor" },
                    new List<string> {"mobile","moneypot","monitor"},
                    new List<string> {"mouse","mousepad"},
                    new List<string> {"mouse","mousepad"},
                    new List<string> {"mouse","mousepad"} }));

            var result2 = solution.SuggestedProducts(new string[] { "havana" }, "havana");
            Assert.IsTrue(result2.IsEqual(
                new List<IList<string>> {
                    new List<string> { "havana" },
                    new List<string> { "havana" },
                    new List<string> { "havana" },
                    new List<string> { "havana" },
                    new List<string> { "havana" },
                    new List<string> { "havana" } }));

            var result3 = solution.SuggestedProducts(new string[] { "bags", "baggage", "banner", "box", "cloths" }, "bags");
            Assert.IsTrue(result3.IsEqual(
                new List<IList<string>> {
                    new List<string> { "baggage","bags","banner"},
                    new List<string> { "baggage","bags","banner" },
                    new List<string> { "baggage","bags"},
                    new List<string> { "bags"} }));

            var result4 = solution.SuggestedProducts(new string[] { "havana" }, "tatiana");
            Assert.IsTrue(result4.IsEqual(
                new List<IList<string>> {
                    new List<string> { },
                    new List<string> { },
                    new List<string> { },
                    new List<string> { },
                    new List<string> { },
                    new List<string> { },
                    new List<string> { } }));
        }

        [TestMethod]
        public void SearchSuggestionsSystem_MySolution()
        {
            var solution = new SearchSuggestionsSystem.MySolution();
            RunTest(solution);
        }
    }
}
