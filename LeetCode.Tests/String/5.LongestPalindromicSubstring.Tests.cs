using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class LongestPalindromicSubstringTests
    {
        void RunTest(LongestPalindromicSubstring.ISolution solution)
        {
            var result1 = solution.LongestPalindrome("babad");
            Assert.AreEqual("bab", result1);

            var result2 = solution.LongestPalindrome("cbbd");
            Assert.AreEqual("bb", result2);

            var result3 = solution.LongestPalindrome("ccc");
            Assert.AreEqual("ccc", result3);
        }

        [TestMethod]
        public void LongestPalindromicSubstring_MySolution()
        {
            var solution = new LongestPalindromicSubstring.MySolution();
            RunTest(solution);
        }

        [TestMethod]
        public void LongestPalindromicSubstring_SlowAlgorithm()
        {
            var solution = new LongestPalindromicSubstring.SlowAlgorithm();
            RunTest(solution);
        }

        [TestMethod]
        public void LongestPalindromicSubstring_ManacherAlgorithm()
        {
            var solution = new LongestPalindromicSubstring.ManacherAlgorithm();
            RunTest(solution);
        }
    }
}
