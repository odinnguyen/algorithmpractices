using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class IntegerToEnglishWordsTests
    {
        void RunTest(IntegerToEnglishWords.ISolution solution)
        {
            var result1 = solution.NumberToWords(123);
            Assert.AreEqual("One Hundred Twenty Three", result1);

            var result2 = solution.NumberToWords(12345);
            Assert.AreEqual("Twelve Thousand Three Hundred Forty Five", result2);

            var result3 = solution.NumberToWords(1234567);
            Assert.AreEqual("One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven", result3);

            var result4 = solution.NumberToWords(1234567891);
            Assert.AreEqual("One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One", result4);

            var result5 = solution.NumberToWords(0);
            Assert.AreEqual("Zero", result5);

            var result6 = solution.NumberToWords(50868);
            Assert.AreEqual("Fifty Thousand Eight Hundred Sixty Eight", result6);

            var result7 = solution.NumberToWords(1000000);
            Assert.AreEqual("One Million", result7);
        }

        [TestMethod]
        public void IntegerToEnglishWords_MySolution()
        {
            var solution = new IntegerToEnglishWords.MySolution();
            RunTest(solution);
        }
    }
}
