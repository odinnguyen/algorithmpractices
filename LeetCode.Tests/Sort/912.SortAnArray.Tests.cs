using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class SortAnArrayTests
    {
        void RunTest(SortAnArray.ISolution solution)
        {
            int[] result;

            result = solution.SortArray(new int[] { 5, 2, 3, 1 });
            Assert.IsTrue(result.IsEqual(new int[] { 1, 2, 3, 5}));

            result = solution.SortArray(new int[] { 5, 1, 1, 2, 0, 0 });
            Assert.IsTrue(result.IsEqual(new int[] { 0, 0, 1, 1, 2, 5 }));
        }

        [TestMethod]
        public void SortAnArray_MySolution()
        {
            var solution = new SortAnArray.MySolution();
            RunTest(solution);
        }
    }
}
