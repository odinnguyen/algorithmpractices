using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class SerializeAndDeserializeBinaryTreeTests
    {
        [TestMethod]
        public void MaxPathSum_MySolution_Serialize()
        {
            var solution = new SerializeAndDeserializeBinaryTree.MySolution();
            var root = BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { 1, 2, 3, null, null, 4, 5});
            var result = solution.Serialize(root);
            Assert.AreEqual("1 2 3   4 5", result);


        }

        [TestMethod]
        public void MaxPathSum_MySolution_Deserialize()
        {
            var solution = new SerializeAndDeserializeBinaryTree.MySolution();
            var root = solution.Deserialize("1 2 3   4 5");
            var result = root.InorderTraversal().Select(n => n.Data).ToList();
            Assert.IsTrue(result.IsEqual(new List<int> { 2, 1, 4, 3, 5 }));
        }
    }
}
