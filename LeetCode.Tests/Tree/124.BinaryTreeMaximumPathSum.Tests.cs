using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class BinaryTreeMaximumPathSumTests
    {
        [TestMethod]
        public void MaxPathSum_MySolution()
        {
            var result = BinaryTreeMaximumPathSum.MySolution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { 1, 2, 3 }));
            Assert.AreEqual(6, result);

            result = BinaryTreeMaximumPathSum.MySolution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { -10, 9, 20, null, null, 15, 7 }));
            Assert.AreEqual(42, result);

            result = BinaryTreeMaximumPathSum.MySolution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { 2, -1 }));
            Assert.AreEqual(2, result);

            result = BinaryTreeMaximumPathSum.MySolution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { -7 }));
            Assert.AreEqual(-7, result);
        }

        [TestMethod]
        public void MaxPathSum_OtherPeopleSolution()
        {
            var solution = new BinaryTreeMaximumPathSum.OtherPeopleSolution();

            var result = solution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { 1, 2, 3 }));
            Assert.AreEqual(6, result);

            result = solution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { -10, 9, 20, null, null, 15, 7 }));
            Assert.AreEqual(42, result);

            result = solution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { 2, -1 }));
            Assert.AreEqual(2, result);

            result = solution.MaxPathSum(BreadthFirstSearchHelpers.BuildBinaryTree(new List<int?> { -7 }));
            Assert.AreEqual(-7, result);
        }
    }
}
