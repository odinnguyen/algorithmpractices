using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class GraphValidTreeTests
    {
        [TestMethod]
        public void GraphValidTree_Solution1()
        {
            var solution = new GraphValidTree.Solution1();

            var result = solution.ValidTree(5, new List<List<int>> { new List<int>{ 0, 1 }, new List<int> { 0, 2 }, new List<int> { 0, 3 }, new List<int> { 1, 4 } });
            Assert.IsTrue(result);

            result = solution.ValidTree(5, new List<List<int>> { new List<int> { 0, 1 }, new List<int> { 1, 2 }, new List<int> { 2, 3 }, new List<int> { 1, 3 }, new List<int> { 1, 4 } });
            Assert.IsFalse(false);
        }

        [TestMethod]
        public void GraphValidTree_Solution2()
        {
            var solution = new GraphValidTree.Solution2();

            var result = solution.ValidTree(5, new List<List<int>> { new List<int> { 0, 1 }, new List<int> { 0, 2 }, new List<int> { 0, 3 }, new List<int> { 1, 4 } });
            Assert.IsTrue(result);

            result = solution.ValidTree(5, new List<List<int>> { new List<int> { 0, 1 }, new List<int> { 1, 2 }, new List<int> { 2, 3 }, new List<int> { 1, 3 }, new List<int> { 1, 4 } });
            Assert.IsFalse(false);
        }
    }
}
