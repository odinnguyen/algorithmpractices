using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class ConstructBinaryTreeFromPreorderAndInorderTraversalTests
    {
        [TestMethod]
        public void BuildTree_MySolution()
        {
            var root = ConstructBinaryTreeFromPreorderAndInorderTraversal.MySolution.BuildTree(
                new int[] { 3, 9, 20, 15, 7 }, new int[] { 9, 3, 15, 20, 7 });

            var nodesBFS = root.BreadthFirstSearch();
            var nodesDataBFS = nodesBFS.Select(n => n.Data).ToList();
            Assert.IsTrue(nodesDataBFS.IsEqual(new List<int> { 3, 9, 20, 15, 7 }));
        }

        [TestMethod]
        public void BuildTree_OtherPeopleSolution()
        {
            var solution = new ConstructBinaryTreeFromPreorderAndInorderTraversal.OtherPeopleSolution();
            var root = solution.BuildTree(
                new int[] { 3, 9, 20, 15, 7 }, new int[] { 9, 3, 15, 20, 7 });

            var nodesBFS = root.BreadthFirstSearch();
            var nodesDataBFS = nodesBFS.Select(n => n.Data).ToList();
            Assert.IsTrue(nodesDataBFS.IsEqual(new List<int> { 3, 9, 20, 15, 7 }));
        }
    }
}
