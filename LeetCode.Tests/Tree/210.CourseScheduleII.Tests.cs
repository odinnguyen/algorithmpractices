using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class CourseScheduleIITests
    {

        [TestMethod]
        public void CourseScheduleII_MySolution()
        {
            var solution = new CourseScheduleII.MySolution();
            var result1 = solution.FindOrder(4, new int[][] { new int[]{ 1, 0 }, new int[] { 2, 0 }, new int[] { 3, 1 }, new int[] { 3, 2 } });
            Assert.IsTrue(result1.IsEqual(new int[] { 0, 2, 1, 3 }));

            var result2 = solution.FindOrder(2, new int[][] { new int[] { 1, 0 } });
            Assert.IsTrue(result2.IsEqual(new int[] { 0, 1 }));

            var result3 = solution.FindOrder(1, new int[][] { });
            Assert.IsTrue(result3.IsEqual(new int[] { 0 }));
        }
    }
}
