using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class MeetingRoomsIITests
    {
        void RunTest(MeetingRoomsII.ISolution solution)
        {
            int result;
            int[][] matrix;

            matrix = new int[][] {
                new int[] { 0, 30 },
                new int[] { 5, 10 },
                new int[] { 15, 20 }
                };
            result = solution.MinMeetingRooms(matrix);
            Assert.AreEqual(2, result);

            matrix = new int[][] {
                new int[] { 2, 7 }
                };
            result = solution.MinMeetingRooms(matrix);
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void MeetingRoomsII_MySolution()
        {
            var solution = new MeetingRoomsII.MySolution();
            RunTest(solution);
        }
    }
}
