using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class Search2DMatrixIITests
    {
        void RunTest(Search2DMatrixII.ISolution solution)
        {
            bool result;
            int[][] matrix;

            matrix = new int[][] {
                new int[] {1, 4, 7, 11, 15 },
                new int[] {2, 5, 8, 12, 19 },
                new int[] {3, 6, 9, 16, 22 },
                new int[] {10, 13, 14, 17, 24 },
                new int[] {18, 21, 23, 26, 30 }
                };

            result = solution.SearchMatrix(matrix, 5);
            Assert.IsTrue(result);

            result = solution.SearchMatrix(matrix, 20);
            Assert.IsFalse(result);

            matrix = new int[][] {
                new int[] { 2, 5 },
                new int[] { 2, 8 },
                new int[] { 7, 9 },
                new int[] { 7, 11 },
                new int[] { 9, 11 }
                };

            result = solution.SearchMatrix(matrix, 7);
            Assert.IsTrue(result);

            matrix = new int[][] {
                new int[] { 1, 2, 3, 4, 5 },
                new int[] { 6, 7, 8, 9, 10 },
                new int[] { 11, 12, 13, 14, 15 },
                new int[] { 16, 17, 18, 19, 20 },
                new int[] { 21, 22, 23, 24, 25 }
                };

            result = solution.SearchMatrix(matrix, 25);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Search2DMatrixII_MySolution1()
        {
            var solution = new Search2DMatrixII.MySolution1();
            RunTest(solution);
        }

        [TestMethod]
        public void Search2DMatrixII_MySolution2()
        {
            var solution = new Search2DMatrixII.MySolution2();
            RunTest(solution);
        }
    }
}
