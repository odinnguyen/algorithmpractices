using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class PathWithMaximumMinimumValueTests
    {
        void RunTest(PathWithMaximumMinimumValue.ISolution solution)
        {
            var matrix1 = new int[,] { { 5, 4, 5 }, { 1, 2, 6 }, { 7, 4, 6 } };
            var result1 = solution.MaximumMinimumPath(matrix1);
            Assert.AreEqual(4, result1);

            var matrix2 = new int[,] { { 2, 2, 1, 2, 2, 2 }, { 1, 2, 2, 2, 1, 2 } };
            var result2 = solution.MaximumMinimumPath(matrix2);
            Assert.AreEqual(2, result2);

            var matrix3 = new int[,] { { 3, 4, 6, 3, 4 }, { 0, 2, 1, 1, 7 }, { 8, 8, 3, 2, 7 }, { 3, 2, 4, 9, 8 }, { 4, 1, 2, 0, 0 }, { 4, 6, 5, 4, 3 } };
            var result3 = solution.MaximumMinimumPath(matrix3);
            Assert.AreEqual(3, result3);

            var matrix4 = new int[,] { { 3, 4, 6, 3, 4 }, { 0, 3, 1, 1, 7 }, { 8, 8, 3, 1, 7 }, { 3, 2, 4, 2, 8 }, { 4, 1, 2, 0, 0 }, { 4, 6, 5, 4, 3 } };
            var result4 = solution.MaximumMinimumPath(matrix4);
            Assert.AreEqual(3, result4);

            var matrix5 = new int[,] { { 3, 4, 6, 3, 4 }, { 0, 3, 1, 1, 7 }, { 8, 1, 3, 1, 7 }, { 3, 2, 4, 2, 8 }, { 4, 1, 2, 0, 0 }, { 4, 6, 5, 4, 3 } };
            var result5 = solution.MaximumMinimumPath(matrix5);
            Assert.AreEqual(2, result5);
        }

        [TestMethod]
        public void PathWithMaximumMinimumValue_MySolution()
        {
            var solution = new PathWithMaximumMinimumValue.MySolution();
            RunTest(solution);
        }

        [TestMethod]
        public void PathWithMaximumMinimumValue_OtherPeopleSolution()
        {
            var solution = new PathWithMaximumMinimumValue.OtherPeopleSolution();
            RunTest(solution);
        }
    }
}
