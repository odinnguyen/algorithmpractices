using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class ReconstructItineraryTests
    {
        [TestMethod]
        public void ReconstructItinerary_MySolution1()
        {
            var solution = new ReconstructItinerary.MySolution1();
            IList<IList<string>> tickets = new List<IList<string>>();
            tickets.Add(new List<string> { "JFK", "SFO" });
            tickets.Add(new List<string> { "JFK", "ATL" });
            tickets.Add(new List<string> { "SFO", "ATL" });
            tickets.Add(new List<string> { "ATL", "JFK" });
            tickets.Add(new List<string> { "ATL", "SFO" });
            var result = solution.FindItinerary(tickets);

            Assert.IsTrue(result.IsEqual(new List<string> { "JFK", "ATL", "JFK", "SFO", "ATL", "SFO" }));
        }

        [TestMethod]
        public void ReconstructItinerary_MySolution2()
        {
            var solution = new ReconstructItinerary.MySolution2();
            IList<IList<string>> tickets = new List<IList<string>>();
            tickets.Add(new List<string> { "JFK", "SFO" });
            tickets.Add(new List<string> { "JFK", "ATL" });
            tickets.Add(new List<string> { "SFO", "ATL" });
            tickets.Add(new List<string> { "ATL", "JFK" });
            tickets.Add(new List<string> { "ATL", "SFO" });
            var result = solution.FindItinerary(tickets);

            Assert.IsTrue(result.IsEqual(new List<string> { "JFK", "ATL", "JFK", "SFO", "ATL", "SFO" }));
        }
    }
}
