using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class TwoSumTests
    {
        [TestMethod]
        public void TwoSum_MySolution()
        {
            var solution = new TwoSum.MySolution();

            var result1 = solution.TwoSum(new int[] { 2, 7, 11, 15 }, 9);
            Assert.IsTrue(result1.IsEqual(new int[] { 0, 1 }));

            var result2 = solution.TwoSum(new int[] { 3, 2, 4 }, 6);
            Assert.IsTrue(result2.IsEqual(new int[] { 1, 2 }));

            var result3 = solution.TwoSum(new int[] { 3, 3 }, 6);
            Assert.IsTrue(result3.IsEqual(new int[] { 0, 1 }));
        }
    }
}
