using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class BestTimeToBuyAndSellStockTests
    {
        void RunTest(BestTimeToBuyAndSellStock.ISolution solution)
        {
            var result1 = solution.MaxProfit(new int[] { 7, 6, 4, 3, 1 });
            Assert.AreEqual(0, result1);
        }

        [TestMethod]
        public void BestTimeToBuyAndSellStock_MySolution()
        {
            var solution = new BestTimeToBuyAndSellStock.MySolution();
            RunTest(solution);
        }
    }
}
