using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class ReverseNodesInKGroupTests
    {
        void RunTest(ReverseNodesInKGroup.ISolution solution)
        {
            var result1 = solution.ReverseKGroup(new int[] { 1, 2, 3, 4, 5 }.ConvertToLinkedList<int, ListNode>(), 2);
            Assert.IsTrue(result1.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 2, 1, 4, 3, 5 }));

            var result2 = solution.ReverseKGroup(new int[] { 1, 2, 3, 4, 5 }.ConvertToLinkedList<int, ListNode>(), 3);
            Assert.IsTrue(result2.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 3, 2, 1, 4, 5 }));

            var result3 = solution.ReverseKGroup(new int[] { 1, 2, 3, 4 }.ConvertToLinkedList<int, ListNode>(), 4);
            Assert.IsTrue(result3.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 4, 3, 2, 1 }));

            var result4 = solution.ReverseKGroup(new int[] { 1, 2, 3, 4 }.ConvertToLinkedList<int, ListNode>(), 1);
            Assert.IsTrue(result4.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 1, 2, 3, 4 }));
        }

        [TestMethod]
        public void ReverseNodesInKGroup_MySolution1()
        {
            var solution = new ReverseNodesInKGroup.MySolution1();
            RunTest(solution);
        }

        [TestMethod]
        public void ReverseNodesInKGroup_MySolution2()
        {
            var solution = new ReverseNodesInKGroup.MySolution2();
            RunTest(solution);
        }
    }
}
