using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;
    using static LeetCode.LinkedList.CopyListWithRandomPointer;

    [TestClass]
    public class CopyListWithRandomPointerTests
    {
        void RunTest(CopyListWithRandomPointer.ISolution solution)
        {
            var node0 = new Node(7);
            var node1 = new Node(13);
            var node2 = new Node(11);
            var node3 = new Node(10);
            var node4 = new Node(1);

            node0.next = node1;
            node1.next = node2;
            node2.next = node3;
            node3.next = node4;

            node1.random = node0;
            node2.random = node4;
            node3.random = node2;
            node4.random = node0;

            var result1 = solution.CopyRandomList(node0);
            Assert.AreEqual(7, result1.val);
            Assert.IsNull(result1.random);

            result1 = result1.next;
            Assert.AreEqual(13, result1.val);
            Assert.AreEqual(7, result1.random.val);
                
            result1 = result1.next;
            Assert.AreEqual(11, result1.val);
            Assert.AreEqual(1, result1.random.val);

            result1 = result1.next;
            Assert.AreEqual(10, result1.val);
            Assert.AreEqual(11, result1.random.val);

            result1 = result1.next;
            Assert.AreEqual(1, result1.val);
            Assert.AreEqual(7, result1.random.val);
        }

        [TestMethod]
        public void CopyListWithRandomPointer_MySolution()
        {
            var solution = new CopyListWithRandomPointer.MySolution();
            RunTest(solution);
        }
    }
}
