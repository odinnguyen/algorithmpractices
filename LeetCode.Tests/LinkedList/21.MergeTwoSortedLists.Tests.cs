using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class MergeTwoSortedListsTests
    {
        void RunTest(MergeTwoSortedLists.ISolution solution)
        {
            var result1 = solution.MergeTwoLists(
                new int[] { 1, 2, 4 }.ConvertToLinkedList<int, ListNode>(),
                new int[] { 1, 3, 4 }.ConvertToLinkedList<int, ListNode>()
                );
            Assert.IsTrue(result1.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 1, 1, 2, 3, 4, 4 }));

            var result2 = solution.MergeTwoLists(
                new int[] { }.ConvertToLinkedList<int, ListNode>(),
                new int[] { }.ConvertToLinkedList<int, ListNode>()
                );
            Assert.IsNull(result2);

            var result3 = solution.MergeTwoLists(
                null,
                new int[] { 0 }.ConvertToLinkedList<int, ListNode>()
                );
            Assert.IsTrue(result3.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 0 }));
        }

        [TestMethod]
        public void MergeTwoSortedLists_MySolution1()
        {
            var solution = new MergeTwoSortedLists.MySolution1();
            RunTest(solution);
        }

        [TestMethod]
        public void MergeTwoSortedLists_MySolution2()
        {
            var solution = new MergeTwoSortedLists.MySolution2();
            RunTest(solution);
        }
    }
}
