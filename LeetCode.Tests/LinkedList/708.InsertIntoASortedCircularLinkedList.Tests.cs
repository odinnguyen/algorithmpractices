using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class InsertIntoASortedCircularLinkedListTests
    {
        void RunTest(InsertIntoASortedCircularLinkedList.ISolution solution)
        {
            ListNode result;

            result = solution.Insert((new int[] { 3, 4, 1 }).ConvertToLinkedList<int, ListNode>(true), 2);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 3, 4, 1, 2 }));

            result = solution.Insert((new int[] { 3, 4, 1, 2 }).ConvertToLinkedList<int, ListNode>(true), 5);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 3, 4, 5, 1, 2 }));

            result = solution.Insert((new int[] { 4, 5, 2, 3 }).ConvertToLinkedList<int, ListNode>(true), 1);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 4, 5, 1, 2, 3 }));

            result = solution.Insert((new int[] { 1 }).ConvertToLinkedList<int, ListNode>(true), 1);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 1, 1 }));

            result = solution.Insert((new int[] { 2 }).ConvertToLinkedList<int, ListNode>(true), 3);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 2, 3 }));

            result = solution.Insert((new int[] { 2 }).ConvertToLinkedList<int, ListNode>(true), 1);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 2, 1 }));

            result = solution.Insert(null, 1);
            Assert.IsTrue(result.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 1 }));
        }

        [TestMethod]
        public void ReverseNodesInKGroup_MySolution()
        {
            var solution = new InsertIntoASortedCircularLinkedList.MySolution();
            RunTest(solution);
        }
    }
}
