using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class MergeKSortedListsTests
    {
        void RunTest(MergeKSortedLists.ISolution solution)
        {
            var result1 = solution.MergeKLists(
                new ListNode[] {
                    new int[] { 1, 4, 5 }.ConvertToLinkedList<int, ListNode>(),
                    new int[] { 1, 3, 4 }.ConvertToLinkedList<int, ListNode>(),
                    new int[] { 2, 6 }.ConvertToLinkedList<int, ListNode>()
            });
            Assert.IsTrue(result1.ToEnumerable<int, ListNode>().ToList().IsEqual(new int[] { 1, 1, 2, 3, 4, 4, 5, 6 }));

            var result2 = solution.MergeKLists(new ListNode[] { });
            Assert.IsNull(result2);

            var result3 = solution.MergeKLists(new ListNode[] { null });
            Assert.IsNull(result3);
        }

        [TestMethod]
        public void MergeKSortedLists_MySolution()
        {
            var solution = new MergeKSortedLists.MySolution();
            RunTest(solution);
        }
    }
}
