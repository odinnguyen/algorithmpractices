using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using LeetCode.LinkedList;
    using Phuong.Common.Helpers;
    using Phuong.Common.LinkedList;

    [TestClass]
    public class NumberOfDistinctIslandsTests
    {
        void RunTest(NumberOfDistinctIslands.ISolution solution)
        {
            int result;
            char[][] matrix;

            matrix = new char[][] {
                new char[] {'1','1','0','0','1'},
                new char[] {'1','0','0','0','0'},
                new char[] {'1','1','0','0','1'},
                new char[] {'0','1','0','1','1'}
                };
            result = solution.NumDistinctIslands(matrix);
            Assert.AreEqual(3, result);

            matrix = new char[][] {
                new char[] {'1','1','1','0','0'},
                new char[] {'1','1','0','0','0'},
                new char[] {'0','0','1','1','1'},
                new char[] {'0','0','1','1','0'}
                };
            result = solution.NumDistinctIslands(matrix);
            Assert.AreEqual(1, result);

            matrix = new char[][] {
                new char[] {'1','1','0','0','0'},
                new char[] {'1','0','1','1','0'},
                new char[] {'1','1','0','1','0'},
                new char[] {'0','0','0','1','1'}
                };
            result = solution.NumDistinctIslands(matrix);
            Assert.AreEqual(2, result);

            matrix = new char[][] {
                new char[] {'1','1','0','0','1'},
                new char[] {'1','0','1','1','0'},
                new char[] {'1','1','0','1','0'},
                new char[] {'0','0','0','1','1'}
                };
            result = solution.NumDistinctIslands(matrix);
            Assert.AreEqual(3, result);

            matrix = new char[][] {
                new char[] {'1','1','0','0','1'},
                new char[] {'1','0','1','1','0'},
                new char[] {'0','1','0','1','0'},
                new char[] {'0','1','1','0','1'}
                };
            result = solution.NumDistinctIslands(matrix);
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void NumberOfDistinctIslands_MySolution()
        {
            var solution = new NumberOfDistinctIslands.MySolution();
            RunTest(solution);
        }
    }
}
