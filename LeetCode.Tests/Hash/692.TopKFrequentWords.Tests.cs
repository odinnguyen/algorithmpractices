using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree.Tests
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    [TestClass]
    public class TopKFrequentWordsTests
    {
        void RunTest(TopKFrequentWords.ISolution solution)
        {
            IList<string> result;

            result = solution.TopKFrequent(new string[] { "i", "love", "leetcode", "i", "love", "coding" }, 2);
            Assert.IsTrue(result.IsEqual(new List<string> { "i", "love" }));

            result = solution.TopKFrequent(new string[] { "the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is" }, 4);
            Assert.IsTrue(result.IsEqual(new List<string> { "the", "is", "sunny", "day" }));
        }

        [TestMethod]
        public void TopKFrequentWords_MySolution()
        {
            var solution = new TopKFrequentWords.MySolution();
            RunTest(solution);
        }
    }
}
