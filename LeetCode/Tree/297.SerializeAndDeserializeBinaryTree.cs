﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Tree
{
    using Phuong.Common.BinaryTree;
    /// <summary>
    /// https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
    /// Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, 
    /// or transmitted across a network connection link to be reconstructed later in the same or another computer environment.
    /// 
    /// Design an algorithm to serialize and deserialize a binary tree.
    /// There is no restriction on how your serialization/deserialization algorithm should work. 
    /// You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.
    /// 
    /// Clarification: The input/output format is the same as how LeetCode serializes a binary tree. 
    /// You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.
    /// </summary>
    public class SerializeAndDeserializeBinaryTree
    {
        /// <summary>
        /// LeetCode Benchmark
        /// Runtime: 200 ms, faster than 27.90% of C# online submissions for Serialize and Deserialize Binary Tree.
        /// Memory Usage: 49.3 MB, less than 15.48% of C# online submissions for Serialize and Deserialize Binary Tree.
        /// </summary>
        public class MySolution
        {
            /// <summary>
            /// Encodes a tree to a single string.
            /// </summary>
            /// <param name="root">root node</param>
            /// <returns>serialized string</returns>
            public string Serialize(TreeNode<int> root)
            {
                return root.Serialize();
            }

            /// <summary>
            /// Decodes your encoded data to tree.
            /// </summary>
            /// <param name="data">serialized string</param>
            /// <returns>root node</returns>
            public TreeNode<int> Deserialize(string data)
            {
                var array = data.Split(' ');
                var list = new List<int?>();

                foreach (var item in array)
                    if (int.TryParse(item, out int parseResult))
                        list.Add(parseResult);
                    else
                        list.Add(null);

                return BreadthFirstSearchHelpers.BuildBinaryTree(list);
            }
        }
    }
}
