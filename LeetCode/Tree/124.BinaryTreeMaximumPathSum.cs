﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Tree
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// https://leetcode.com/problems/binary-tree-maximum-path-sum/
    /// A path in a binary tree is a sequence of nodes where each pair of adjacent nodes in the sequence has an edge connecting them. 
    /// A node can only appear in the sequence at most once. Note that the path does not need to pass through the root.
    /// The path sum of a path is the sum of the node's values in the path.
    /// Given the root of a binary tree, return the maximum path sum of any path.
    /// 
    /// Example 1:
    ///         1
    ///        / \
    ///       2   3
    /// The optimal path is 2 -> 1 -> 3 with a path sum of 2 + 1 + 3 = 6.
    /// 
    /// Example 2:
    ///         -10
    ///         /  \
    ///        9   20
    ///            / \
    ///           15  7
    /// The optimal path is 15 -> 20 -> 7 with a path sum of 15 + 20 + 7 = 42.
    /// 
    /// Constraints:
    /// The number of nodes in the tree is in the range[1, 3 * 104].
    /// -1000 <= Node.val <= 1000
    /// </summary>
    public class BinaryTreeMaximumPathSum
    {
        /// <summary>
        /// LeetCode Benchmark
        /// runtime = 120 ms
        /// memory = 33.1 MB
        /// </summary>
        public class MySolution
        {
            public static int MaxPathSum(TreeNode<int> root)
            {
                return MaxPath(root).MaxCompletePath;
            }

            public static (int MaxPathDown, int MaxCompletePath) MaxPath(TreeNode<int> root)
            {
                if (root == null)
                    return (0, int.MinValue);

                var maxLeft = MaxPath(root.Left);
                var maxRight = MaxPath(root.Right);

                // if the path result in decreasing the total value, ignore it. Aka. consider it is 0
                var maxLeftDown = Math.Max(0, maxLeft.MaxPathDown);
                var maxRightDown = Math.Max(0, maxRight.MaxPathDown);

                var maxValue = MathHelpers.Max(
                    maxLeft.MaxCompletePath,
                    maxRight.MaxCompletePath,
                    maxLeftDown + maxRightDown + root.Data);

                return (MathHelpers.Max(maxLeftDown, maxRightDown) + root.Data, maxValue);
            }
        }

        /// <summary>
        /// LeetCode Benchmark
        /// runtime = 160 ms
        /// memory = 31 MB
        /// </summary>
        public class OtherPeopleSolution
        {
            int maxValue;

            public int MaxPathSum(TreeNode<int> root)
            {
                maxValue = int.MinValue;
                maxPathDown(root);
                return maxValue;
            }

            private int maxPathDown(TreeNode<int> node)
            {
                if (node == null) return 0;
                int left = Math.Max(0, maxPathDown(node.Left));
                int right = Math.Max(0, maxPathDown(node.Right));
                maxValue = Math.Max(maxValue, left + right + node.Data);
                return Math.Max(left, right) + node.Data;
            }
        }
    }
}
