﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Tree
{
    using Phuong.Common.BinaryTree;
    using Phuong.Common.Helpers;

    /// <summary>
    /// https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
    /// Construct Binary Tree from Preorder and Inorder Traversal
    /// 
    /// Given two integer arrays preorder and inorder, construct and return the binary tree. Where:
    /// - preorder is the preorder traversal of a binary tree
    /// - inorder is the inorder traversal of the same tree
    /// 
    /// Example 1:
    ///       3
    ///      / \
    ///     9  20
    ///        / \
    ///       15  7
    /// Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
    /// Output: [3,9,20,null,null,15,7]
    /// 
    /// Example 2:
    /// Input: preorder = [-1], inorder = [-1]
    /// Output: [-1]
    /// 
    /// Constraints:
    /// - 1 <= preorder.length <= 3000
    /// - inorder.length == preorder.length
    /// - -3000 <= preorder[i], inorder[i] <= 3000
    /// - preorder and inorder consist of unique values.
    /// - Each value of inorder also appears in preorder.
    /// - preorder is guaranteed to be the preorder traversal of the tree.
    /// - inorder is guaranteed to be the inorder traversal of the tree.
    /// </summary>
    public class ConstructBinaryTreeFromPreorderAndInorderTraversal
    {
        /// <summary>
        /// Leetcode benchmark
        /// runtime = 348 ms
        /// memory = 29.1 MB
        /// </summary>
        public class MySolution
        {
            public static TreeNode<int> BuildTree(IEnumerable<int> preorder, IEnumerable<int> inorder)
            {
                if (!preorder.Any())
                    return null;

                var rootData = preorder.First();
                var rootIndex = inorder.IndexOf(rootData).Value;

                var preorderLeft = preorder.Skip(1).Take(rootIndex);
                var inorderLeft = inorder.Take(rootIndex);

                var preorderRight = preorder.Skip(rootIndex + 1);
                var inorderRight = inorder.Skip(rootIndex + 1);

                return new TreeNode<int>(
                    rootData,
                    BuildTree(preorderLeft, inorderLeft),
                    BuildTree(preorderRight, inorderRight)
                    );
            }
        }

        /// <summary>
        /// Leetcode benchmark
        /// runtime = 192 ms	
        /// memory = 28.9 MB
        /// </summary>
        public class OtherPeopleSolution
        {
            public TreeNode<int> BuildTree(int[] preorder, int[] inorder)
            {
                return BuildTree(preorder, inorder, 0, preorder.Length - 1, 0, inorder.Length - 1);
            }

            public TreeNode<int> BuildTree(int[] preorder, int[] inorder, int preorderStart, int preorderEnd, int inorderStart, int inorderEnd)
            {
                if (preorder.Length != inorder.Length)
                    throw new Exception("preorder.Length != inorder.Length");

                if (preorder.Length == 0 || preorderStart > preorderEnd)
                    return null;

                var rootData = preorder[preorderStart];
                var inorderRootIndex = inorder.IndexOf(rootData).Value;

                var leftNode = BuildTree(preorder, inorder, preorderStart + 1, preorderStart + inorderRootIndex - inorderStart, inorderStart, inorderRootIndex - 1);
                var rightNode = BuildTree(preorder, inorder, preorderStart + inorderRootIndex - inorderStart + 1, preorderEnd, inorderRootIndex + 1, inorderEnd);

                return new TreeNode<int>(
                    rootData,
                    leftNode,
                    rightNode
                    );
            }
        }
    }
}
