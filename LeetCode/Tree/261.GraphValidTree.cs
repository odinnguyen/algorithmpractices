﻿using Phuong.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Tree
{
    /// <summary>
    /// https://leetcode.com/problems/graph-valid-tree/
    /// Given n nodes labeled from 0 to n-1 and a list of undirected edges (each edge is a pair of nodes), 
    /// write a function to check whether these edges make up a valid tree.
    /// 
    /// Example 1:
    /// Input: n = 5, and edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
    /// Output: true
    /// 
    /// Example 2:
    /// Input: n = 5, and edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
    /// Output: false
    /// 
    /// A tree is a special undirected graph. It satisfy two properties
    /// - It is connected
    /// - It has no cycle.
    /// </summary>
    public class GraphValidTree
    {
        /// <summary>
        /// A valid tree mean (number of uniques edges + 1) = (number of nodes)
        /// </summary>
        public class Solution1
        {
            /// <summary>
            /// Check whether it is a valid tree
            /// </summary>
            /// <param name="n">number of nodes</param>
            /// <param name="edges">list of edges</param>
            /// <returns>true: if valid tree</returns>
            public bool ValidTree(int n, List<List<int>> edges)
            {
                var matrix = new int[n,n];
                var edgeCount = 0;

                //To check for unique edges, we only need to care about the lower-half of diagonal [(0,0) -> (n-1,n-1)]
                foreach (var edge in edges)
                {
                    var src = edge[0];
                    var dest = edge[1];

                    if (matrix[src, dest] == 1 || matrix[dest, src] == 1)
                        continue;
                    else
                    {
                        matrix[src, dest] = matrix[dest, src] = 1;
                        edgeCount++;
                    }
                }                        

                return n == edgeCount + 1;
            }
        }

        /// <summary>
        /// A valid tree mean 
        /// - any node connected to all the nodes
        /// - no node visited twice when doing Breadth First search from any node
        /// </summary>
        public class Solution2
        {
            /// <summary>
            /// Check whether it is a valid tree
            /// </summary>
            /// <param name="n">number of nodes</param>
            /// <param name="edges">list of edges</param>
            /// <returns>true: if valid tree</returns>
            public bool ValidTree(int n, List<List<int>> edges)
            {
                if (n == 0)
                    return false;

                List<HashSet<int>> adjacentList = new List<HashSet<int>>();
                for (var i = 0; i < n; i++)
                    adjacentList.Add(new HashSet<int>());

                //To check for unique edges, we only need to care about the lower-half of diagonal [(0,0) -> (n-1,n-1)]
                foreach (var edge in edges)
                {
                    var src = edge[0];
                    var dest = edge[1];

                    adjacentList[src].Add(dest);
                    adjacentList[dest].Add(src);
                }

                //List of all nodes not visited yet
                var visited = new bool[n];

                var queue = new Queue<int>();
                queue.Enqueue(0);//Let's start from node 0
                while (queue.Any())
                {
                    var node = queue.Dequeue();

                    //found loop
                    if (visited[node])
                        return false;
                    
                    visited[node] = true;
                    foreach (var adjacentNode in adjacentList[node])
                    {
                        queue.Enqueue(adjacentNode);
                        adjacentList[adjacentNode].Remove(node);
                    }
                }

                return visited.All(v => v == true);
            }
        }
    }
}
