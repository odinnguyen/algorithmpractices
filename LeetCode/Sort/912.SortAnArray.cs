﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/sort-an-array/
    /// Given an array of integers nums, sort the array in ascending order.
    /// 
    /// Example 1:
    /// Input: nums = [5,2,3,1]
    /// Output: [1,2,3,5]
    /// 
    /// Example 2:
    /// Input: nums = [5,1,1,2,0,0]
    /// Output: [0,0,1,1,2,5]
    /// 
    /// Constraints:
    /// 1 <= nums.length <= 5 * 10^4
    /// -5 * 10^4 <= nums[i] <= 5 * 10^4
    /// </summary>
    public class SortAnArray
    {
        public interface ISolution
        {
            int[] SortArray(int[] nums);
        }

        /// <summary>
        /// Runtime: 476 ms, faster than 14.62% of C# online submissions for Sort an Array.
        /// Memory Usage: 45.4 MB, less than 35.39% of C# online submissions for Sort an Array.
        /// </summary>
        public class MySolution: ISolution
        {
            public int[] SortArray(int[] nums)
            {
                return SortArray(nums, 0, nums.Length - 1);
            }

            public int[] SortArray(int[] nums, int start, int end)
            {
                var mid = (end + start) / 2;

                if (start < mid) SortArray(nums, start, mid);
                if (mid + 1 < end) SortArray(nums, mid + 1, end);

                var curr = start;
                var currA = 0;
                var currB = 0;

                var arrayA = nums.Skip(start).Take(mid - start + 1).ToArray();
                var arrayB = nums.Skip(mid + 1).Take(end - mid).ToArray();
                while (curr <= end)
                {
                    if (currA == arrayA.Length)
                        nums[curr] = arrayB[currB++];

                    else if (currB == arrayB.Length)
                        nums[curr] = arrayA[currA++];

                    else if (arrayA[currA] < arrayB[currB])
                        nums[curr] = arrayA[currA++];

                    else
                        nums[curr] = arrayB[currB++];

                    curr++;
                }

                return nums;
            }
        }
    }
}
