﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/number-of-distinct-islands/
    /// Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of DISTINCT islands.
    /// An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.
    /// An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
    /// You may assume all four edges of the grid are all surrounded by water.
    /// 
    /// Example 1:
    /// Input: grid = [
    ///   ['1','1','1','0','0']
    ///   ['1','1','0','0','0']
    ///   ['0','0','1','1','1']
    ///   ['0','0','1','1','0']
    /// ]
    /// Output: 1
    /// 
    /// Example 2:
    /// Input: grid = [
    ///   ['1','1','0','0','0']
    ///   ['1','0','1','1','0']
    ///   ['1','1','0','1','0']
    ///   ['0','0','0','1','1']
    /// ]
    /// Output: 2
    /// 
    /// Example 3:
    ///   ['1','1','0','0','1']
    ///   ['1','0','1','1','0']
    ///   ['0','1','0','1','0']
    ///   ['0','1','1','0','1']
    /// ]
    /// Output: 4
    /// 
    /// Constraints:
    /// m == grid.length
    /// n == grid[i].length
    /// 1 <= m, n <= 300
    /// grid[i][j] is '0' or '1'.
    /// 
    /// CAUTION: this is char[][] not int[][]
    /// </summary>
    public class NumberOfDistinctIslands
    {
        public interface ISolution
        {
            int NumDistinctIslands(char[][] grid);
        }

        public class MySolution : ISolution
        {
            const char ISLAND_CHAR = '1';
            static readonly new List<(int X, int Y)> DIRECTIONS = new List<(int X, int Y)> { (0, -1), (0, 1), (-1, 0), (1, 0) };

            public int NumDistinctIslands(char[][] grid)
            {
                var landTiles = new List<(int X, int Y)>();

                for (int x = 0; x < grid.Length; x++)
                    for (int y = 0; y < grid[0].Length; y++)
                        if (grid[x][y] == ISLAND_CHAR)
                            landTiles.Add((x, y));

                var distinctIslandsSignature = new HashSet<string>();
                while (landTiles.Any())
                {
                    var startTile = landTiles.First();
                    landTiles.Remove(startTile);

                    var queue = new Queue<(int X, int Y)>();
                    queue.Enqueue(startTile);
                    var signature = new StringBuilder();
                    var round = 0;
                    while (queue.Any())
                    {
                        var tile = queue.Dequeue();
                        foreach (var direction in DIRECTIONS)
                        {
                            (int X, int Y) adjacentTile = (tile.X + direction.X, tile.Y + direction.Y);

                            if (adjacentTile.X < 0 || adjacentTile.X >= grid.Length)
                                continue;

                            if (adjacentTile.Y < 0 || adjacentTile.Y >= grid[0].Length)
                                continue;

                            if (grid[adjacentTile.X][adjacentTile.Y] == ISLAND_CHAR && landTiles.Contains(adjacentTile))
                            {
                                queue.Enqueue(adjacentTile);
                                signature.Append($"({round+direction.X},{round+direction.Y})");
                                landTiles.Remove(adjacentTile);
                            }
                        }
                        round++;
                    }

                    //Add island's signature into hash set
                    distinctIslandsSignature.Add(signature.ToString());
                }

                return distinctIslandsSignature.Count();
            }
        }
    }
}
