﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/top-k-frequent-words/
    /// Given a non-empty list of words, return the k most frequent elements.
    /// Your answer should be sorted by frequency from highest to lowest.If two words have the same frequency, then the word with the lower alphabetical order comes first.
    /// 
    /// Example 1:
    /// Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
    /// Output: ["i", "love"]
    /// Explanation: "i" and "love" are the two most frequent words.
    /// Note that "i" comes before "love" due to a lower alphabetical order.
    /// 
    /// Example 2:
    /// Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
    /// Output: ["the", "is", "sunny", "day"]
    /// Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
    /// with the number of occurrence being 4, 3, 2 and 1 respectively.
    /// 
    /// Note:
    /// You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
    /// Input words contain only lowercase letters.
    /// 
    /// Follow up:
    /// Try to solve it in O(n log k) time and O(n) extra space.
    /// </summary>
    public class TopKFrequentWords
    {
        public interface ISolution
        {
            /// <summary>
            /// Find most common words
            /// </summary>
            /// <param name="words">array of words</param>
            /// <param name="k">number of most common words</param>
            /// <returns>list k most common words</returns>
            IList<string> TopKFrequent(string[] words, int k);
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 268 ms, faster than 78.51% of C# online submissions for Top K Frequent Words.
        /// Memory Usage: 34.2 MB, less than 80.90% of C# online submissions for Top K Frequent Words.
        /// </summary>
        public class MySolution : ISolution
        {
            public IList<string> TopKFrequent(string[] words, int k)
            {
                var dictionary = new Dictionary<string, int>();
                for(var i = 0; i < words.Length; i++)
                {
                    if (!dictionary.ContainsKey(words[i]))
                        dictionary[words[i]] = 1;
                    else dictionary[words[i]]++;
                }

                return dictionary.Keys
                    .OrderByDescending(k => dictionary[k])
                    .ThenBy(k => k)
                    .Take(k)
                    .ToList();
            }
        }
    }
}
