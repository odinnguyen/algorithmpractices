﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/trapping-rain-water/
    /// Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it can trap after raining.
    /// 
    /// Example 1:
    /// 4|               _
    /// 3|       _      | |_   _
    /// 2|   _  | |_   _| | |_| |_
    /// 1|  | | | | | | | | | | | |
    /// 
    /// Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
    /// Output: 6
    /// Explanation:The above elevation map is represented by array[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]. 
    ///             In this case, 6 units of rain water are being trapped.
    /// 
    /// Example 2:
    /// Input: height = [4, 2, 0, 3, 2, 5]
    /// Output: 9
    /// 
    /// Constraints:
    /// n == height.length
    /// 0 <= n <= 3 * 10^4
    /// 0 <= height[i] <= 10^5
    /// </summary>
    public class TrappingRainWater
    {
        public interface ISolution
        {
            int Trap(int[] height);
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 100 ms, faster than 30.81% of C# online submissions for Trapping Rain Water.
        /// Memory Usage: 25.7 MB, less than 80.33% of C# online submissions for Trapping Rain Water.
        /// </summary>
        public class MySolution : ISolution
        {
            public int Trap(int[] height)
            {
                if (height == null || !height.Any())
                    return 0;

                //Find highest left bound of each point
                var leftBound = new int[height.Length];
                leftBound[0] = 0;
                for (var i = 1; i < height.Length; i++)
                {
                    if (height[i - 1] > leftBound[i - 1])
                        leftBound[i] = height[i - 1];
                    else
                        leftBound[i] = leftBound[i - 1];
                }

                //Find highest right bound of each point
                var rightBound = new int[height.Length];
                rightBound[height.Length - 1] = 0;
                for (var i = height.Length - 2; i >= 0; i--)
                {
                    if (height[i + 1] > rightBound[i + 1])
                        rightBound[i] = height[i + 1];
                    else
                        rightBound[i] = rightBound[i + 1];
                }

                int waterTrapped = 0;

                //water trapped at this point if both left bound and right bound higher than this point
                for (var i = 0; i < height.Length; i++)
                {
                    var upperbound = Math.Min(leftBound[i], rightBound[i]);

                    if (upperbound > height[i])
                        waterTrapped += upperbound - height[i];
                }

                return waterTrapped;
            }
        }
    }
}
