﻿using Phuong.Common.Stack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/min-stack/
    /// Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
    /// Implement the MinStack class:
    /// MinStack() initializes the stack object.
    /// void push(val) pushes the element val onto the stack.
    /// void pop() removes the element on the top of the stack.
    /// int top() gets the top element of the stack.
    /// int getMin() retrieves the minimum element in the stack.
    /// 
    /// Example 1:
    /// Input
    /// ["MinStack", "push", "push", "push", "getMin", "pop", "top", "getMin"]
    /// [[],[-2],[0],[-3],[],[],[],[]]
    /// 
    /// Output
    /// [null, null, null, null, -3, null, 0, -2]
    /// 
    /// Explanation
    /// MinStack minStack = new MinStack();
    /// minStack.push(-2);
    /// minStack.push(0);
    /// minStack.push(-3);
    /// minStack.getMin(); // return -3
    /// minStack.pop();
    /// minStack.top();    // return 0
    /// minStack.getMin(); // return -2
    /// 
    /// Constraints:
    /// -2^31 <= val <= 2^31 - 1
    /// Methods pop, top and getMin operations will always be called on non-empty stacks.
    /// At most 3 * 10^4 calls will be made to push, pop, top, and getMin.
    /// </summary>
    public class MinStack
    {
        public interface IMinStack<T>
            where T : IComparable
        {
            /// <summary>
            /// pushes the element val onto the stack.
            /// </summary>
            /// <param name="val">element</param>
            void Push(T val);

            /// <summary>
            /// removes the element on the top of the stack
            /// </summary>
            void Pop();

            /// <summary>
            /// gets the top element of the stack
            /// </summary>
            /// <returns>top element of the stack</returns>
            T Top();

            /// <summary>
            /// retrieves the minimum element in the stack
            /// </summary>
            /// <returns>minimum element in the stack</returns>
            T GetMin();
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 128 ms, faster than 82.09% of C# online submissions for Min Stack.
        /// Memory Usage: 35.6 MB, less than 64.61% of C# online submissions for Min Stack.
        /// </summary>
        public class MySolution
        {
            public class MinStack : OrderedStack<int>, IMinStack<int>
            {
                /// <summary>
                /// gets the top element of the stack
                /// </summary>
                /// <returns>top element of the stack</returns>
                public int Top()
                {
                    return this.Peek();
                }

                /// <summary>
                /// removes the element on the top of the stack
                /// </summary>
                void IMinStack<int>.Pop()
                {
                    base.Pop();
                }
            }
        }
    }
}
