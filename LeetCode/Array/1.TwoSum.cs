﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/two-sum/
    /// Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
    /// You may assume that each input would have exactly one solution, and you may not use the same element twice.
    /// You can return the answer in any order.
    /// 
    /// Example 1:
    /// Input: nums = [2, 7, 11, 15], target = 9
    /// Output: [0,1]
    /// Output: Because nums[0] + nums[1] == 9, we return [0, 1].
    /// 
    /// Example 2:
    /// Input: nums = [3, 2, 4], target = 6
    /// Output: [1,2]
    /// 
    /// Example 3:
    /// Input: nums = [3, 3], target = 6
    /// Output: [0,1]
    /// 
    /// Constraints:
    /// 2 <= nums.length <= 10^4
    /// -10^9 <= nums[i] <= 10^9
    /// -10^9 <= target <= 10^9
    /// Only one valid answer exists.
    /// </summary>
    public class TwoSum
    {
        /// <summary>
        /// LeetCode BenchMark
        /// Runtime: 244 ms, faster than 54.25% of C# online submissions for Two Sum.
        /// Memory Usage: 36.7 MB, less than 5.38% of C# online submissions for Two Sum.
        /// </summary>
        public class MySolution
        {
            public int[] TwoSum(int[] nums, int target)
            {
                var dictionary = new Dictionary<int, List<int>>();
                for (int i = 0; i < nums.Length; i++)
                {
                    if (!dictionary.ContainsKey(nums[i]))
                        dictionary[nums[i]] = new List<int>();

                    dictionary[nums[i]].Add(i);
                }

                for (int i = 0; i < nums.Length; i++)
                {
                    var num1 = nums[i];
                    var num2 = target - num1;
                    if (dictionary.ContainsKey(num2) && dictionary[num2].Exists(index => index != i))
                        return new int[] { i, dictionary[num2].First(index => index != i) };
                }

                return null;
            }
        }
    }
}
