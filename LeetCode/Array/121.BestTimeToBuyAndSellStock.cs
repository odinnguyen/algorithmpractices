﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
    /// You are given an array prices where prices[i] is the price of a given stock on the ith day.
    /// You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
    /// Return the maximum profit you can achieve from this transaction.If you cannot achieve any profit, return 0.
    /// 
    /// Example 1:
    /// Input: prices = [7, 1, 5, 3, 6, 4]
    /// Output: 5
    /// Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
    /// Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.
    /// 
    /// Example 2:
    /// Input: prices = [7,6,4,3,1]
    /// Output: 0
    /// Explanation: In this case, no transactions are done and the max profit = 0.
    /// 
    /// Constraints:
    /// - 1 <= prices.length <= 10^5
    /// - 0 <= prices[i] <= 10^4
    /// </summary>
    public class BestTimeToBuyAndSellStock
    {
        public interface ISolution
        {
            int MaxProfit(int[] prices);
        }

        /// <summary>
        /// LeetCode Benchmark:
        /// Runtime: 552 ms, faster than 5.03% of C# online submissions for Best Time to Buy and Sell Stock.
        /// Memory Usage: 44.3 MB, less than 45.76% of C# online submissions for Best Time to Buy and Sell Stock.
        /// </summary>
        public class MySolution : ISolution
        {
            public int MaxProfit(int[] prices)
            {
                if (prices.Length < 2) return 0;

                int minleft = prices[0], profit = 0;
                for(int i = 1; i < prices.Length; i++)
                {
                    if (prices[i] - minleft > profit) profit = prices[i] - minleft;
                    if (minleft > prices[i]) minleft = prices[i];
                }

                return profit;
            }
        }
    }
}
