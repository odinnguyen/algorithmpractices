﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    using Phuong.Common.Helpers;
    /// <summary>
    /// https://leetcode.com/problems/3sum/
    /// Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.
    /// Notice that the solution set must not contain duplicate triplets.
    /// 
    /// Example 1:
    /// Input: nums = [-1, 0, 1, 2, -1, -4]
    /// Output: [[-1,-1,2], [-1,0,1]]
    /// 
    /// Example 2:
    /// Input: nums = []
    /// Output: []
    /// 
    /// Example 3:
    /// Input: nums = [0]
    /// Output: []
    /// 
    /// Example 4:
    /// Input: nums = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    /// Output: [[0,0,0]]
    /// 
    /// Constraints:
    /// 0 <= nums.length <= 3000
    /// -10^5 <= nums[i] <= 10^5
    /// </summary>
    public class ThreeSum
    {
        public interface ISolution
        {
            IList<IList<int>> ThreeSum(int[] nums);
        }
        /// <summary>
        /// LeetCode Benchmark
        /// Runtime: 496 ms, faster than 27.52% of C# online submissions for 3Sum.
        /// Memory Usage: 38.4 MB, less than 14.53% of C# online submissions for 3Sum.
        /// </summary>
        public class MySolution : ISolution
        {
            public IList<IList<int>> ThreeSum(int[] nums)
            {
                var leftSet = new HashSet<int>();
                var rightSet = new Dictionary<int, List<int>>();

                for (var i = 0; i < nums.Length; i++)
                {
                    if (!rightSet.ContainsKey(nums[i]))
                        rightSet[nums[i]] = new List<int>();

                    rightSet[nums[i]].Add(i);
                }

                var result = new HashSet<(int Small, int Median, int Large)>();//use hashset to avoid duplicate
                for (var i = 0; i < nums.Length; i++)
                {
                    //remove nums[i] from the right list
                    rightSet[nums[i]].Remove(i);

                    foreach (var leftNum in leftSet)
                    {
                        var rightNum = 0 - nums[i] - leftNum;

                        //We only count one occurence of rightNum to avoid duplicate
                        if (rightSet.ContainsKey(rightNum) && rightSet[rightNum].Any())
                        {
                            var small = MathHelpers.Min(leftNum, nums[i], rightNum);
                            var large = MathHelpers.Max(leftNum, nums[i], rightNum);
                            result.Add((small, 0 - small - large, large));
                        }
                    }

                    //We dont add duplicate to the left, cause that will cause duplicate triplet results
                    leftSet.Add(nums[i]);
                }

                return result.Select(item => new List<int> { item.Small, item.Median, item.Large } as IList<int>).ToList();
            }
        }

        /// <summary>
        /// LeetCode Benchmark:
        /// Runtime: 612 ms, faster than 19.29% of C# online submissions for 3Sum.
        /// Memory Usage: 36.4 MB, less than 61.99% of C# online submissions for 3Sum.
        /// </summary>
        public class OtherPeopleSolution : ISolution
        {
            public IList<IList<int>> ThreeSum(int[] nums)
            {
                nums.MergeSort();
                IList<IList<int>> res = new List<IList<int>>();
                for (int i = 0; i < nums.Length - 2; i++)
                {
                    if (i == 0 || (i > 0 && nums[i] != nums[i - 1]))
                    {
                        int lo = i + 1, hi = nums.Length - 1, sum = 0 - nums[i];
                        while (lo < hi)
                        {
                            if (nums[lo] + nums[hi] == sum)
                            {
                                res.Add(new List<int> { nums[i], nums[lo], nums[hi] });
                                while (lo < hi && nums[lo] == nums[lo + 1]) lo++;
                                while (lo < hi && nums[hi] == nums[hi - 1]) hi--;
                                lo++; hi--;
                            }
                            else if (nums[lo] + nums[hi] < sum) lo++;
                            else hi--;
                        }
                    }
                }
                return res;
            }
        }
    }
}
