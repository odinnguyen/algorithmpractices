﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/search-a-2d-matrix-ii/
    /// Write an efficient algorithm that searches for a target value in an m x n integer matrix. The matrix has the following properties:
    /// Integers in each row are sorted in ascending from left to right.
    /// Integers in each column are sorted in ascending from top to bottom.
    /// 
    /// Example 1:
    /// 1   4   7   11  15
    /// 2   5   8   12  19
    /// 3   6   9   16  22
    /// 10  13  14  17  24
    /// 18  21  23  26  30
    /// 
    /// Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 5
    /// Output: true
    /// 
    /// Example 2:
    /// 1   4   7   11  15
    /// 2   5   8   12  19
    /// 3   6   9   16  22
    /// 10  13  14  17  24
    /// 18  21  23  26  30
    /// 
    /// Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 20
    /// Output: false
    /// 
    /// Example 3:
    /// Input: matrix = [[2,5],[2,8],[7,9],[7,11],[9,11]], target = 7
    /// Output: true
    /// </summary>
    public class Search2DMatrixII
    {
        public interface ISolution
        {
            bool SearchMatrix(int[][] matrix, int target);
        }

        /// <summary>
        /// LeetCode benchmark
        /// Runtime: 360 ms, faster than 26.01% of C# online submissions for Search a 2D Matrix II.
        /// Memory Usage: 31.6 MB, less than 16.63% of C# online submissions for Search a 2D Matrix II.
        /// </summary>
        public class MySolution1 : ISolution
        {
            public bool SearchMatrix(int[][] matrix, int target)
            {
                return SearchMatrix(matrix, target, 0, matrix[0].Length - 1);
            }

            public bool SearchMatrix(int[][] matrix, int target, int startX, int endY)
            {
                //Escape condition
                if (startX >= matrix.Length) return false;
                if (endY < 0) return false;

                var row = startX;
                var col = 0;

                //Go to left from the first row until we meet a cell.value > target
                //Due to the matrix property, anything from that column onward will be greater than target
                //We can ignore that column onward
                for (; col <= endY; col++)
                    if (matrix[row][col] == target)
                        return true;
                    else if (matrix[row][col] > target)
                        break;

                if (col == 0)
                    return false;

                //column where cell of first row < target
                col = col - 1;

                endY = col - 1;//End column boundaries, incase we don't find anything from the boundary outward

                //Go down from the first row until we meet a cell.value > target
                //Due to the matrix property, anything from that row upward will be less than target
                //We can ignore from that row upward
                for (; row < matrix.Length; row++)
                    if (matrix[row][col] == target)
                        return true;
                    else if (matrix[row][col] > target)
                        break;

                if (row == matrix.Length)
                    return false;

                startX = row + 1;//start row boundaries, incase we don't find anything from the boundary upward

                //Go left from current position (row, col) try to find target
                for (; col >= 0; col--)
                    if (matrix[row][col] == target)
                        return true;

                //Since we can't find anything, we will start again the same search algorithm within the new boundary
                return SearchMatrix(matrix, target, startX, endY);
            }
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 2004 ms, faster than 5.12% of C# online submissions for Search a 2D Matrix II.
        /// Memory Usage: 31.3 MB, less than 67.80% of C# online submissions for Search a 2D Matrix II.
        /// </summary>
        public class MySolution2 : ISolution
        {
            public bool SearchMatrix(int[][] matrix, int target)
            {
                return SearchMatrix(matrix, target, 0, matrix.Length - 1, 0, matrix[0].Length - 1);
            }

            public bool SearchMatrix(int[][] matrix, int target, int startX, int endX, int startY, int endY)
            {
                //Escape condition
                if (startX > endX) return false;
                if (startY > endY) return false;

                var row = startX;
                var col = startY;

                //Go to left from the first row until we meet a cell.value > target
                //Due to the matrix property, anything from that column onward will be greater than target
                //We can ignore that column onward
                for (; col <= endY; col++)
                    if (matrix[startX][col] == target)
                        return true;
                    else if (matrix[startX][col] > target)
                        break;

                if (col == startY)
                    return false;

                //Go down from the first row until we meet a cell.value > target
                //Due to the matrix property, anything from that row upward will be less than target
                //We can ignore from that row upward
                for (; row <= endX; row++)
                    if (matrix[row][startY] == target)
                        return true;
                    else if (matrix[row][startY] > target)
                        break;

                if (row == startX)
                    return false;

                //Since we can't find anything, we will start again the same search algorithm within the new boundary
                return SearchMatrix(matrix, target, startX + 1, row - 1, startY + 1, col - 1);
            }
        }
    }
}
