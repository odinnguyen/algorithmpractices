﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/meeting-rooms-ii/
    /// Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), 
    /// find the minimum number of conference rooms required.
    /// 
    /// Example 1:
    /// Input: intervals = [(0, 30),(5, 10),(15, 20)]
    /// Output: 2
    /// Explanation:
    /// We need two meeting rooms
    /// room1: (0,30)
    /// room2: (5,10),(15,20)
    /// 
    /// Example 2:
    /// Input: intervals = [(2, 7)]
    /// Output: 1
    /// Explanation: 
    /// Only need one meeting room
    /// </summary>
    public class MeetingRoomsII
    {
        public interface ISolution
        {
            /// <summary>
            /// Find minimum meeting rooms needed
            /// </summary>
            /// <param name="intervals">List of interval</param>
            /// <returns>number meeting rooms needed</returns>
            int MinMeetingRooms(int[][] intervals);
        }

        /// <summary>
        /// For every interval (si, ei), the number of room needed for that period increase by 1. 
        /// 
        /// So instead of keeping track the number of rooms needed for each entire interval exhaustly, 
        /// just keep track of the rate of change (i.e. a slope) in terms of where the rate started its increase and where it stopped its decrease. 
        /// 
        /// This is done by adding 1 to the "si" position of your array and adding -1 to the "ei+1" position of your array for every interval (si, ei). 
        /// "ei+1" is used because the increase still applied at "ei".
        /// 
        /// The maximum final value is equivalent to the maximum accumulated "slope" starting from the first position, because it is the spot which incremented more than all other places.
        /// Accumulated "slope" means to you add slope changes in position 0 to position 1, then add that to position 2, and so forth, looking for the point where it was the greatest.
        /// </summary>
        public class MySolution : ISolution
        {
            public int MinMeetingRooms(int[][] intervals)
            {
                
                int start = int.MaxValue;//The earliest meeting start
                int end = int.MinValue;//The latest meeting end

                for (var i = 0; i < intervals.Length; i++)
                {
                    if (intervals[i][0] < start)
                        start = intervals[i][0];

                    if (intervals[i][1] > end)
                        end = intervals[i][1];
                }

                //array[0] corresponding to the earliest meeting start,
                //array[end - start + 1] corresponding to the latest meeting end
                //where array[end - start + 2] for conclusion (discussed in the algorithm)
                var array = new int[end - start + 2];

                //Adding slope changes
                for (var i = 0; i < intervals.Length; i++)
                {
                    var normalizedStart = intervals[i][0] - start;
                    var normalizedEnd = intervals[i][1] - start;

                    array[normalizedStart]++;
                    array[normalizedEnd + 1]--;
                }

                //Calculate maxRoomsNeeded from array of slope changes
                var maxRoomsNeeded = 0;
                var roomsNeed = 0;
                for (var i = 0; i < array.Length; i++)
                {
                    roomsNeed += array[i];
                    if (roomsNeed > maxRoomsNeeded)
                        maxRoomsNeeded = roomsNeed;
                }

                return maxRoomsNeeded;
            }
        }
    }
}
