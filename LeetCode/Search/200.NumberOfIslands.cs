﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/number-of-islands/
    /// Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.
    /// An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
    /// You may assume all four edges of the grid are all surrounded by water.
    /// 
    /// Example 1:
    /// Input: grid = [
    ///   ['1','1','1','1','0'],
    ///   ['1','1','0','1','0'],
    ///   ['1','1','0','0','0'],
    ///   ['0','0','0','0','0']
    /// ]
    /// Output: 1
    /// 
    /// Example 2:
    /// Input: grid = [
    ///   ['1','1','0','0','0'],
    ///   ['1','1','0','0','0'],
    ///   ['0','0','1','0','0'],
    ///   ['0','0','0','1','1']
    /// ]
    /// Output: 3
    /// 
    /// Constraints:
    /// m == grid.length
    /// n == grid[i].length
    /// 1 <= m, n <= 300
    /// grid[i][j] is '0' or '1'.
    /// 
    /// CAUTION: this is char[][] not int[][]
    /// </summary>
    public class NumberOfIslands
    {
        public interface ISolution
        {
            int NumIslands(char[][] grid);
        }

        /// <summary>
        /// Use Breadth First Search
        /// 
        /// LeetCode benchmark:
        /// Runtime: 124 ms, faster than 14.68% of C# online submissions for Number of Islands.
        /// Memory Usage: 30.5 MB, less than 5.08% of C# online submissions for Number of Islands.
        /// </summary>
        public class MySolution : ISolution
        {
            const char ISLAND_CHAR = '1';
            static readonly new List<(int X, int Y)> DIRECTIONS = new List<(int X, int Y)> { (0, -1), (0, 1), (-1, 0), (1, 0) };

            public int NumIslands(char[][] grid)
            {
                var hashLandTiles = new HashSet<(int X, int Y)>();

                for (int x = 0; x < grid.Length; x++)
                    for (int y = 0; y < grid[0].Length; y++)
                        if (grid[x][y] == ISLAND_CHAR)
                            hashLandTiles.Add((x, y));

                var islandsCount = 0;
                while (hashLandTiles.Any())
                {
                    var startTile = hashLandTiles.First();
                    hashLandTiles.Remove(startTile);

                    var queue = new Queue<(int X, int Y)>();
                    queue.Enqueue(startTile);
                    while (queue.Any())
                    {
                        var tile = queue.Dequeue();
                        foreach (var direction in DIRECTIONS)
                        {
                            (int X, int Y) adjacentTile = (tile.X + direction.X, tile.Y + direction.Y);

                            if (adjacentTile.X < 0 || adjacentTile.X >= grid.Length)
                                continue;

                            if (adjacentTile.Y < 0 || adjacentTile.Y >= grid[0].Length)
                                continue;

                            if (grid[adjacentTile.X][adjacentTile.Y] == ISLAND_CHAR && hashLandTiles.Contains(adjacentTile))
                            {
                                queue.Enqueue(adjacentTile);
                                hashLandTiles.Remove(adjacentTile);
                            }
                        }
                    }

                    //Increase count after remove all tiles of one island
                    islandsCount++;
                }

                return islandsCount;
            }
        }
    }
}
