﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    using LeetCode.LinkedList;

    /// <summary>
    /// https://leetcode.com/problems/reverse-nodes-in-k-group/
    /// Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.
    /// k is a positive integer and is less than or equal to the length of the linked list.
    /// If the number of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.
    /// You may not alter the values in the list's nodes, only nodes themselves may be changed.
    /// 
    /// Example 1:
    /// Input: head = [1,2,3,4,5], k = 2
    /// Output: [2,1,4,3,5]
    /// 
    /// Example 2:
    /// Input: head = [1,2,3,4,5], k = 3
    /// Output: [3,2,1,4,5]
    /// 
    /// Example 3:
    /// Input: head = [1,2,3,4,5], k = 1
    /// Output: [1,2,3,4,5]
    /// 
    /// Example 4:
    /// Input: head = [1], k = 1
    /// Output: [1]
    /// 
    /// Constraints:
    /// The number of nodes in the list is in the range sz.
    /// 1 <= sz <= 5000
    /// 0 <= Node.val <= 1000
    /// 1 <= k <= sz
    /// </summary>
    public class ReverseNodesInKGroup
    {
        public interface ISolution
        {
            ListNode ReverseKGroup(ListNode head, int k);
        }

        /// <summary>
        /// LeetCode Benchmark
        /// Runtime: 132 ms, faster than 8.57% of C# online submissions for Reverse Nodes in k-Group.
        /// Memory Usage: 27.1 MB, less than 14.76% of C# online submissions for Reverse Nodes in k-Group.
        /// </summary>
        public class MySolution1 : ISolution
        {
            public ListNode ReverseKGroup(ListNode head, int k)
            {
                var list = new List<ListNode> { head };
                var curr = head;
                while (curr.next != null)
                {
                    curr = curr.next;
                    list.Add(curr);
                }

                for (var i = 0; i + k <= list.Count; i += k)
                    for (var j = 0; j < k/2; j++)
                    {
                        var temp = list[i + (k - j - 1)].val;
                        list[i + (k - j - 1)].val = list[i + j].val;
                        list[i + j].val = temp;
                    }

                return head;
            }
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 120 ms, faster than 11.19% of C# online submissions for Reverse Nodes in k-Group.
        /// Memory Usage: 26.7 MB, less than 65.00% of C# online submissions for Reverse Nodes in k-Group.
        /// </summary>
        public class MySolution2 : ISolution
        {
            public ListNode ReverseKGroup(ListNode head, int k)
            {
                if (head == null)
                    return null;

                var count = 1;
                var curr = head;
                while (curr.next != null && count < k)
                {
                    count++;
                    curr = curr.next;
                }

                if (count < k)
                    return head;

                var prev = head;
                curr = prev.next;
                var next = curr != null ? curr.next : null;
                for (var  i = 0; i < k - 1; i++)
                {
                    curr.next = prev;
                    prev = curr;
                    curr = next;
                    next = curr != null ? curr.next : null;
                }

                head.next = ReverseKGroup(curr, k);
                return prev;
            }
        }
    }
}
