﻿using LeetCode.LinkedList;
using Phuong.Common.LinkedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/insert-into-a-sorted-circular-linked-list/
    /// Given a node from a cyclic linked list which is sorted in ascending order, 
    /// write a function to insert a value into the list such that it remains a cyclic sorted list. 
    /// 
    /// The given node can be a reference to any single node in the list, and may not be necessarily the smallest value in the cyclic list.
    /// 
    /// If there are multiple suitable places for insertion, you may choose any place to insert the new value.
    /// After the insertion, the cyclic list should remain sorted.
    /// 
    /// If the list is empty (i.e., given node is null), you should create a new single cyclic list and return the reference to that single node.
    /// Otherwise, you should return the original given node.
    /// 
    /// The following example may help you understand the problem better:
    /// Example 1: 
    /// 3 --> 4 --> 1 --> 3
    /// Input: insert 2, head is 3
    /// Output:  3 --> 4 --> 1 --> 2 --> 3
    /// </summary>
    public class InsertIntoASortedCircularLinkedList
    {
        public interface ISolution
        {
            ListNode Insert(ListNode head, int insertVal);
        }

        public class MySolution : ISolution
        {
            public ListNode Insert(ListNode head, int insertVal)
            {
                if (head == null)
                {
                    head = new ListNode(insertVal, null);
                    head.next = head;
                    return head;
                }

                var curr = head;
                ListNode maxNode = head;
                do
                {
                    if (curr.val < insertVal && curr.next.val >= insertVal)
                    {
                        curr.next = new ListNode(insertVal, curr.next);
                        return head;
                    }

                    if (curr.val > curr.next.val)
                        maxNode = curr;

                    curr = curr.next;
                } while (curr != head);

                //if reach here, either
                //- All node.val > insertVal
                //- All node.val < insertVal
                //- All node.val = insertVal
                maxNode.next = new ListNode(insertVal, maxNode.next);

                return head;
            }
        }
    }
}
