﻿using LeetCode.LinkedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/merge-k-sorted-lists/
    /// You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.
    /// Merge all the linked-lists into one sorted linked-list and return it.
    /// 
    /// Example 1:
    /// Input: lists = [[1,4,5],[1,3,4],[2,6]]
    /// Output: [1,1,2,3,4,4,5,6]
    /// Explanation: The linked-lists are:
    /// [
    ///     1->4->5,
    ///     1->3->4,
    ///     2->6
    /// ]
    /// merging them into one sorted list:
    /// 1->1->2->3->4->4->5->6
    /// 
    /// Example 2:
    /// Input: lists = []
    /// Output: []
    /// 
    /// Example 3:
    /// Input: lists = [[]]
    /// Output: []
    /// 
    /// Constraints:
    /// k == lists.length
    /// 0 <= k <= 10^4
    /// 0 <= lists[i].length <= 500
    /// -10^4 <= lists[i][j] <= 10^4
    /// lists[i] is sorted in ascending order.
    /// The sum of lists[i].length won't exceed 10^4.
    /// </summary>
    public class MergeKSortedLists
    {
        public interface ISolution
        {
            ListNode MergeKLists(ListNode[] lists);
        }

        /// <summary>
        /// LeetCode benchmark
        /// Runtime: 108 ms, faster than 88.12% of C# online submissions for Merge k Sorted Lists.
        /// Memory Usage: 30.7 MB, less than 12.06% of C# online submissions for Merge k Sorted Lists.
        /// </summary>
        public class MySolution : ISolution
        {
            public ListNode MergeKLists(ListNode[] lists)
            {
                if (lists.Length == 0)
                    return null;

                return MergeKLists(lists, 0, lists.Length - 1);
            }

            public ListNode MergeKLists(ListNode[] lists, int start, int end)
            {
                if (start == end)
                    return lists[start];

                var mid = start + (end - start) / 2;
                var left = MergeKLists(lists, start, mid);
                var right = mid + 1 <= end ? MergeKLists(lists, mid + 1, end) : null;
                return MergeTwoLists(left, right);
            }

            /// <summary>
            /// Duplicate of MergeTwoSortedLists's solution
            /// </summary>
            /// <param name="l1">list 1 head</param>
            /// <param name="l2">list 2 head</param>
            /// <returns>merged list</returns>
            public ListNode MergeTwoLists(ListNode l1, ListNode l2)
            {
                if (l1 == null && l2 == null) return null;
                if (l1 == null) return l2;
                if (l2 == null) return l1;

                if (l2.val > l1.val)
                {
                    l1.next = MergeTwoLists(l1.next, l2);
                    return l1;
                }
                else
                {
                    l2.next = MergeTwoLists(l1, l2.next);
                    return l2;
                }
            }
        }
    }
}
