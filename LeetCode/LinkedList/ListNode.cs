﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.LinkedList
{
    using Phuong.Common.LinkedList;

    public class ListNode : ListNode<int>
    {
        public int val
        {
            get { return Value; }
            set { Value = value; }
        }

        public ListNode next
        {
            get { return Next as ListNode; }
            set { Next = value; }
        }

        public ListNode()
            : base()
        {
        }

        public ListNode(int val = 0, ListNode next = null)
            : base(val, next)
        {
        }
    }
}
