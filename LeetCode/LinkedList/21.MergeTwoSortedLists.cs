﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    using LeetCode.LinkedList;
    using Phuong.Common.LinkedList;

    /// <summary>
    /// https://leetcode.com/problems/merge-two-sorted-lists/
    /// Merge two sorted linked lists and return it as a sorted list. The list should be made by splicing together the nodes of the first two lists.
    /// 
    /// Example 1:
    /// Input: l1 = [1,2,4], l2 = [1,3,4]
    /// Output: [1,1,2,3,4,4]
    /// 
    /// Example 2:
    /// Input: l1 = [], l2 = []
    /// Output: []
    /// 
    /// Example 3:
    /// Input: l1 = [], l2 = [0]
    /// Output: [0]
    /// 
    /// Constraints:
    /// The number of nodes in both lists is in the range[0, 50].
    /// -100 <= Node.val <= 100
    /// Both l1 and l2 are sorted in non-decreasing order.
    /// </summary>
    public class MergeTwoSortedLists
    {
        public interface ISolution
        {
            ListNode MergeTwoLists(ListNode l1, ListNode l2);
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 96 ms, faster than 47.15% of C# online submissions for Merge Two Sorted Lists.
        /// Memory Usage: 26.3 MB, less than 61.89% of C# online submissions for Merge Two Sorted Lists.
        /// </summary>
        public class MySolution1 : ISolution
        {
            public static ListNode MergesTwoLists(ListNode l1, ListNode l2)
            {
                if (l1 == null && l2 == null) return null;
                if (l1 == null) return l2;
                if (l2 == null) return l1;

                if (l2.val > l1.val)
                {
                    l1.next = MergesTwoLists(l1.next, l2);
                    return l1;
                }
                else
                {
                    l2.next = MergesTwoLists(l1, l2.next);
                    return l2;
                }
            }

            public ListNode MergeTwoLists(ListNode l1, ListNode l2)
            {
                return MergesTwoLists(l1, l2);
            }
        }

        /// <summary>
        /// Basically the same as MySolution1, just use the common library instead
        /// </summary>
        public class MySolution2 : ISolution
        {
            public ListNode MergeTwoLists(ListNode l1, ListNode l2)
            {
                return LinkedListHelpers.MergeTwoSorted<int, ListNode>(l1, l2);
            }
        }
    }
}
