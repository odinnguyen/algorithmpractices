﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/most-common-word/
    /// Given a string paragraph and a string array of the banned words banned, return the most frequent word that is not banned. 
    /// It is guaranteed there is at least one word that is not banned, and that the answer is unique.
    /// The words in paragraph are case-insensitive and the answer should be returned in lowercase.
    /// 
    /// Example 1:
    /// Input: paragraph = "Bob hit a ball, the hit BALL flew far after it was hit.", banned = ["hit"]
    /// Output: "ball"
    /// Explanation: 
    /// "hit" occurs 3 times, but it is a banned word.
    /// "ball" occurs twice(and no other word does), so it is the most frequent non-banned word in the paragraph.
    /// Note that words in the paragraph are not case sensitive,
    /// that punctuation is ignored (even if adjacent to words, such as "ball,"), 
    /// and that "hit" isn't the answer even though it occurs more because it is banned.
    /// 
    /// Example 2:
    /// Input: paragraph = "a.", banned = []
    /// Output: "a"
    /// 
    /// Constraints:
    /// 1 <= paragraph.length <= 1000
    /// paragraph consists of English letters, space ' ', or one of the symbols: "!?',;.".
    /// 0 <= banned.length <= 100
    /// 1 <= banned[i].length <= 10
    /// banned[i] consists of only lowercase English letters.
    /// </summary>
    public class MostCommonWord
    {
        public interface ISolution
        {
            string MostCommonWord(string paragraph, string[] banned);
        }

        /// <summary>
        /// Leetcode benchmark:
        /// Runtime: 276 ms, faster than 5.18% of C# online submissions for Most Common Word.
        /// Memory Usage: 29.9 MB, less than 7.12% of C# online submissions for Most Common Word.
        /// </summary>
        public class MySolution1 : ISolution
        {
            public string MostCommonWord(string paragraph, string[] banned)
            {
                var dictionary = new Dictionary<string, int>();
                var hashBanned = banned.ToHashSet();

                var regEx = new Regex("(\\w+)");
                (string Word, int Count) max = ("", 0);
                foreach(Match match in regEx.Matches(paragraph))
                {
                    var word = match.Value.ToLowerInvariant();
                    if (hashBanned.Contains(word))
                        continue;

                    if (!dictionary.ContainsKey(word))
                        dictionary[word] = 0;
                    dictionary[word]++;

                    if (dictionary[word] > max.Count)
                        max = (word, dictionary[word]);
                }

                return max.Word;
            }
        }

        /// <summary>
        /// Leetcode benchmark:
        /// Runtime: 252 ms, faster than 5.18% of C# online submissions for Most Common Word.
        /// Memory Usage: 26.5 MB, less than 48.22% of C# online submissions for Most Common Word.
        /// </summary>
        public class MySolution2 : ISolution
        {
            public string MostCommonWord(string paragraph, string[] banned)
            {
                var dictionary = new Dictionary<string, int>();
                var hashBanned = banned.ToHashSet();

                var arr = paragraph.ToArray();
                for (var i = 0; i < arr.Length; i++)
                    if (('a' > arr[i] || arr[i] > 'z') && ('A' > arr[i] || arr[i] > 'Z'))
                        arr[i] = ' ';

                var words = new string(arr).Split().Select(w => w.ToLower()).Where(w => !string.IsNullOrWhiteSpace(w));
                (string Word, int Count) max = ("", 0);
                foreach (var word in words)
                {
                    if (hashBanned.Contains(word))
                        continue;

                    if (!dictionary.ContainsKey(word))
                        dictionary[word] = 0;
                    dictionary[word]++;

                    if (dictionary[word] > max.Count)
                        max = (word, dictionary[word]);
                }
                return max.Word;
            }
        }
    }
}
