﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/longest-palindromic-substring/
    /// Given a string s, return the longest palindromic substring in s.
    /// 
    /// Example 1:
    /// Input: s = "babad"
    /// Output: "bab"
    /// Note: "aba" is also a valid answer.
    /// 
    /// Example 2:
    /// Input: s = "cbbd"
    /// Output: "bb"
    /// 
    /// Example 3:
    /// Input: s = "a"
    /// Output: "a"
    /// 
    /// Example 4:
    /// Input: s = "ac"
    /// Output: "a"
    /// 
    /// Constraints:
    /// - 1 <= s.length <= 1000
    /// - s consist of only digits and English letters (lower-case and/or upper-case),
    /// </summary>
    public class LongestPalindromicSubstring
    {
        public interface ISolution
        {
            string LongestPalindrome(string s);
        }

        /// <summary>
        /// LeetCode benchmark:
        /// Runtime: 128 ms, faster than 55.80% of C# online submissions for Longest Palindromic Substring.
        /// Memory Usage: 24.9 MB, less than 58.57% of C# online submissions for Longest Palindromic Substring.
        /// </summary>
        public class MySolution : ISolution
        {
            public string LongestPalindrome(string s)
            {
                (int Start, int End, int Length) max = (0, 0, 1);
                var center = 0.5;
                while (center < s.Length)
                {
                    var floor = Convert.ToInt32(Math.Floor(center - 0.5));
                    var ceiling = Convert.ToInt32(Math.Ceiling(center + 0.5));
                    var palindromeLength = center % 1 == 0 ? 1 : 0;
                    while(floor >=0 && ceiling < s.Length && s[floor] == s[ceiling])
                    {
                        palindromeLength += 2;

                        //prepare for next cycle
                        floor--;
                        ceiling++;
                    }

                    if (palindromeLength > max.Length)
                        //if condition met, mean it goes through the above while loop at least once
                        max = (floor + 1, ceiling - 1, palindromeLength);

                    center += 0.5;
                }

                return s.Substring(max.Start, max.End - max.Start + 1);
            }
        }

        /// <summary>
        /// https://en.wikipedia.org/wiki/Longest_palindromic_substring
        /// It looks at each character as the center of a palindrome and loops to determine the largest palindrome with that center.
        /// The loop at the center of the function only works for palindromes where the length is an odd number.
        /// The function works for even-length palindromes by modifying the input string. 
        /// The character '|' is inserted between every character in the inputs string, and at both ends.
        /// So the input "book" becomes "|b|o|o|k|". The even-length palindrome "oo" in "book" becomes the odd-length palindrome "|o|o|".
        /// 
        /// LeetCode benchmark:
        /// Runtime: 132 ms, faster than 55.08% of C# online submissions for Longest Palindromic Substring.
        /// Memory Usage: 25.9 MB, less than 55.85% of C# online submissions for Longest Palindromic Substring.
        /// </summary>
        public class SlowAlgorithm : ISolution
        {
            public string LongestPalindrome(string s)
            {
                var S = new char[s.Length*2 + 1];
                var index = 0;
                while(index < S.Length)
                {
                    if(index % 2 == 0)
                        S[index] = '|';
                    else
                        S[index] = s[index/2];

                    index++;
                }
                int[] PalindromeRadii = new int[S.Length]; // The radius of the longest palindrome centered on each place in S'
                // note: length(S') = length(PalindromeRadii) = 2 × length(S) + 1

                (int Index, int Radius) max = (0, 0);
                var center = 0;
                while (center < S.Length)
                {
                    // Determine the longest palindrome starting at Center-Radius and going to Center+Radius
                    var radius = 0;
                    while (center - (radius + 1) >= 0 && center+(radius + 1) < S.Length && S[center - (radius + 1)] == S[center+(radius+1)]) 
                        radius++;

                    // Save the radius of the longest palindrome in the array
                    PalindromeRadii[center] = radius;
                    if (radius > max.Radius)
                        max = (center, radius);

                    center++;
                }

                var longest_palindrome_in_S = 2 * PalindromeRadii.Max() + 1;
                var longest_palindrome_in_s = (longest_palindrome_in_S - 1) / 2;
                return new string(S).Substring(max.Index - max.Radius, 2 * max.Radius + 1).Replace("|","");
            }
        }

        /// <summary>
        /// https://en.wikipedia.org/wiki/Longest_palindromic_substring
        /// The algorithm is faster than the previous algorithm, Slow Algorithm. It exploits when a palindrome happens inside another palindrome.
        /// 
        /// For example, consider the input string "abacaba". 
        /// By the time it gets to the "c", Manacher's algorithm will have identified the length of every palindrome centered on the letters before the "c". 
        /// At the "c", it runs a loop to identify the largest palindrome centered on the "c": "abacaba". 
        /// With that knowledge, everything after the "c" looks like the reflection of everything before the "c". 
        /// The "a" after the "c" has the same longest palindrome as the "a" before the "c". 
        /// Similarly, the "b" after the "c" has a longest palindrome that is at least the length of the longest palindrome centered on the "b" before the "c". 
        /// There are some special cases to consider, but that trick speeds up the computation dramatically.
        /// 
        /// LeetCode benchmark:
        /// Runtime: 164 ms, faster than 52.21% of C# online submissions for Longest Palindromic Substring.
        /// Memory Usage: 25.7 MB, less than 56.01% of C# online submissions for Longest Palindromic Substring.
        /// </summary>
        public class ManacherAlgorithm : ISolution
        {
            public string LongestPalindrome(string s)
            {
                var S = new char[s.Length * 2 + 1];
                var index = 0;
                while (index < S.Length)
                {
                    if (index % 2 == 0)
                        S[index] = '|';
                    else
                        S[index] = s[index / 2];

                    index++;
                }
                int[] PalindromeRadii = new int[S.Length]; // The radius of the longest palindrome centered on each place in S'
                                                           // note: length(S') = length(PalindromeRadii) = 2 × length(S) + 1

                (int Index, int Radius) max = (0, 0);
                var center = 0;
                var radius = 0;
                while (center < S.Length) 
                {
                    // At the start of the loop, Radius is already set to a lower-bound for the longest radius.
                    // In the first iteration, Radius is 0, but it can be higher.

                    // Determine the longest palindrome starting at Center-Radius and going to Center+Radius
                    while (center - (radius + 1) >= 0 && center+(radius + 1) < S.Length && S[center - (radius + 1)] == S[center+(radius+1)]) 
                        radius++;

                    // Save the radius of the longest palindrome in the array
                    PalindromeRadii[center] = radius;
                    if (radius > max.Radius)
                        max = (center, radius);

                    // Below, Center is incremented.
                    // If any precomputed values can be reused, they are.
                    // Also, Radius may be set to a value greater than 0

                    var oldCenter = center;
                    var oldRadius = radius;
                    center++;
                    // Radius's default value will be 0, if we reach the end of the following loop. 
                    radius = 0; 
                    while (center <= oldCenter + oldRadius) 
                    {
                        // Because Center lies inside the old palindrome and every character inside
                        // a palindrome has a "mirrored" character reflected across its center, we
                        // can use the data that was precomputed for the Center's mirrored point. 
                        var mirroredCenter = oldCenter - (center - oldCenter);
                        var maxMirroredRadius = oldCenter + oldRadius - center;
                        if (PalindromeRadii[mirroredCenter] < maxMirroredRadius) 
                        {
                            PalindromeRadii[center] = PalindromeRadii[mirroredCenter];
                            if (PalindromeRadii[mirroredCenter] > max.Radius)
                                max = (center, PalindromeRadii[mirroredCenter]);
                            center++;
                        }   
                        else if (PalindromeRadii[mirroredCenter] > maxMirroredRadius) {
                            PalindromeRadii[center] = maxMirroredRadius;
                            if (maxMirroredRadius > max.Radius)
                                max = (center, maxMirroredRadius);
                            center++;
                        }   
                        else { // PalindromeRadii[MirroredCenter] = MaxMirroredRadius
                            radius = maxMirroredRadius;
                            break;  // exit while loop early
                        }   
                    }      
                }

                var longest_palindrome_in_S = 2 * PalindromeRadii.Max() + 1;
                var longest_palindrome_in_s = (longest_palindrome_in_S - 1) / 2;
                return new string(S).Substring(max.Index - max.Radius, 2 * max.Radius + 1).Replace("|", "");
            }
        }
    }
}
