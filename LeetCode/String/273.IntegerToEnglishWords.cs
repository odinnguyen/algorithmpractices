﻿using Phuong.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    /// <summary>
    /// https://leetcode.com/problems/integer-to-english-words/
    /// Convert a non-negative integer num to its English words representation.
    /// 
    /// Example 1:
    /// Input: num = 123
    /// Output: "One Hundred Twenty Three"
    /// 
    /// Example 2:
    /// Input: num = 12345
    /// Output: "Twelve Thousand Three Hundred Forty Five"
    /// 
    /// Example 3:
    /// Input: num = 1234567
    /// Output: "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
    /// 
    /// Example 4:
    /// Input: num = 1234567891
    /// Output: "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One"
    /// 
    /// Constraints: 0 <= num <= 2^31 - 1
    /// </summary>
    public class IntegerToEnglishWords
    {
        public interface ISolution
        {
            string NumberToWords(int num);
        }

        public class MySolution : ISolution
        {
            /// <summary>
            /// LeetCode Benchmark
            /// Runtime: 76 ms, faster than 94.93% of C# online submissions for Integer to English Words.
            /// Memory Usage: 24.7 MB, less than 35.83% of C# online submissions for Integer to English Words.
            /// </summary>
            /// <param name="num">integer number</param>
            /// <returns>English words representation</returns>
            public string NumberToWords(int num)
            {
                return num.ToEnglishString();
            }
        }
    }
}
