﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Tree
{
    /// <summary>
    /// https://leetcode.com/problems/reconstruct-itinerary/
    /// You are given a list of airline tickets where tickets[i] = [from, to] represent the departure and the arrival airports of one flight. 
    /// Reconstruct the itinerary in order and return it.
    /// 
    /// All of the tickets belong to a man who departs from "JFK", thus, the itinerary must begin with "JFK". 
    /// If there are multiple valid itineraries, you should return the itinerary that has the smallest lexical order when read as a single string.
    /// 
    /// For example, the itinerary["JFK", "LGA"] has a smaller lexical order than["JFK", "LGB"].
    /// You may assume all tickets form at least one valid itinerary.You must use all the tickets once and only once.
    /// 
    /// Example 1:
    /// Input: tickets = [["MUC","LHR"],["JFK","MUC"],["SFO","SJC"],["LHR","SFO"]]
    /// Output: ["JFK","MUC","LHR","SFO","SJC"]
    /// 
    /// Example 2:
    /// Input: tickets = [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
    /// Output: ["JFK","ATL","JFK","SFO","ATL","SFO"]
    /// Explanation: Another possible reconstruction is ["JFK","SFO","ATL","JFK","ATL","SFO"] but it is larger in lexical order.
    /// 
    /// Constraints:
    /// - 1 <= tickets.length <= 300
    /// - tickets[i].length == 2
    /// - tickets[i].from.length == 3
    /// - tickets[i].to.length == 3
    /// - tickets[i].from and tickets[i].to(i) consist of uppercase English letters.
    /// - tickets[i].from != tickets[i].to(i)
    /// 
    /// Analyze:
    /// This is Eulerian Trail problem: https://en.wikipedia.org/wiki/Eulerian_path
    /// For the existence of Eulerian trails it is necessary that zero or two vertices have an odd degree; 
    /// - if "JFK" has odd degree, odd number of edges connect to it, the other only node which also has odd degree will be the destination
    /// - if "JFK" has even degree, the itinerary must start and end at "JFK"
    /// </summary>
    public class ReconstructItinerary
    {
        /// <summary>
        /// Use Depth First Search starting with the next destination larger in lexical order
        /// 
        /// LeetCode Benchmark
        /// Runtime: 668 ms, faster than 5.03% of C# online submissions for Reconstruct Itinerary.
        /// Memory Usage: 34.8 MB, less than 19.55% of C# online submissions for Reconstruct Itinerary.
        /// </summary>
        public class MySolution1
        {
            public IList<string> FindItinerary(IList<IList<string>> tickets)
            {
                return FindItinerary("JFK", tickets, 0, tickets.Count);
            }

            public IList<string> FindItinerary(string root, IList<IList<string>> tickets, int depth, int totalTickets)
            {
                foreach(var ticket in tickets.Where(t => t[0] == root).OrderBy(t => t[1]).ToArray())
                {
                    tickets.Remove(ticket);
                    var itinerary = FindItinerary(ticket[1], tickets, depth + 1, totalTickets);
                    if (depth + itinerary?.Count() == totalTickets)
                    {
                        itinerary.Insert(0, root);
                        return itinerary;
                    }
                    tickets.Add(ticket);
                }
                return new List<string> { root };
            }
        }

        /// <summary>
        /// Use Depth First Search starting with the next destination larger in lexical order
        /// Use dictionary to decrease runtime
        /// 
        /// LeetCode Benchmark
        /// Runtime: 468 ms, faster than 5.03% of C# online submissions for Reconstruct Itinerary.
        /// Memory Usage: 34.9 MB, less than 11.17% of C# online submissions for Reconstruct Itinerary.
        /// </summary>
        public class MySolution2
        {
            public IList<string> FindItinerary(IList<IList<string>> tickets)
            {
                var mapOut = new Dictionary<string, IList<string>>();
                foreach (var ticket in tickets)
                {
                    if (!mapOut.ContainsKey(ticket[0]))
                        mapOut[ticket[0]] = new List<string>();
                    mapOut[ticket[0]].Add(ticket[1]);
                }

                return FindItinerary("JFK", mapOut, 0, tickets.Count);
            }

            public IList<string> FindItinerary(string origin, Dictionary<string, IList<string>> mapOut, int depth, int totalTickets)
            {
                if (mapOut.ContainsKey(origin))
                    foreach (var destination in mapOut[origin].OrderBy(d => d).ToArray())
                    {
                        mapOut[origin].Remove(destination);
                        var itinerary = FindItinerary(destination, mapOut, depth + 1, totalTickets);
                        if (depth + itinerary?.Count() == totalTickets)
                        {
                            itinerary.Insert(0, origin);
                            return itinerary;
                        }
                        mapOut[origin].Add(destination);
                    }
                return new List<string> { origin };
            }
        }
    }
}
