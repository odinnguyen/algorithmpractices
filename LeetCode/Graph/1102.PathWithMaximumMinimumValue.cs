﻿using Phuong.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Tree
{
    /// <summary>
    /// https://leetcode.com/problems/path-with-maximum-minimum-value/
    /// Given a matrix of integers A with R rows and C columns, find the maximum score of a path starting at [0,0] and ending at [R-1,C-1].
    /// The score of a path is the minimum value in that path. For example, the value of the path 8 -> 4 -> 5 -> 9 is 4.
    /// A path moves some number of times from one visited cell to any neighbouring unvisited cell in one of the 4 cardinal directions (north, east, west, south).
    /// 
    /// Example 1:
    /// 5 -- 4 -- 5
    ///           |
    /// 1    2    6
    ///           |
    /// 7    4    6
    /// Input: [[5,4,5],[1,2,6],[7,4,6]]
    /// Output: 4
    /// Explanation: The path with the maximum score is connected with lines.
    /// 
    /// Example 2:
    /// 2 -- 2    1    2 -- 2 -- 2
    ///      |         |         |
    /// 1    2 -- 2 -- 2    1    2
    /// Input: [[2,2,1,2,2,2],[1,2,2,2,1,2]]
    /// Output: 2
    /// 
    /// Example 3:
    /// 3 -- 4 -- 6 -- 3 -- 4
    ///                     |
    /// 0    2    1    1    7
    ///                     |
    /// 8 -- 8 -- 3    2    7
    /// |         |         |
    /// 3    2    4 -- 9 -- 8
    /// |
    /// 4    1    2    0    0
    /// |
    /// 4 -- 6 -- 5 -- 4 -- 3
    /// Input: [[3,4,6,3,4],[0,2,1,1,7],[8,8,3,2,7],[3,2,4,9,8],[4,1,2,0,0],[4,6,5,4,3]]
    /// Output: 3
    /// 
    /// Example 4:
    /// 3 -- 4    6    3    4
    ///      |               
    /// 0    3    1    1    7
    ///      |               
    /// 8 -- 8    3    1    7
    /// |                  
    /// 3    2    4    2    8
    /// |
    /// 4    1    2    0    0
    /// |
    /// 4 -- 6 -- 5 -- 4 -- 3
    /// Input: [[3,4,6,3,4],[0,3,1,1,7],[8,8,3,1,7],[3,2,4,2,8],[4,1,2,0,0],[4,6,5,4,3]]
    /// Output: 3
    /// 
    /// Example 5:
    /// 3 -- 4 -- 6 -- 3 -- 4
    ///                     |
    /// 0    3    1    1    7
    ///                     |
    /// 8    1    3    1    7
    ///                     |
    /// 3 -- 2 -- 4 -- 2 -- 8
    /// |
    /// 4    1    2    0    0
    /// |
    /// 4 -- 6 -- 5 -- 4 -- 3
    /// Input: [[3,4,6,3,4],[0,3,1,1,7],[8,1,3,1,7],[3,2,4,2,8],[4,1,2,0,0],[4,6,5,4,3]]
    /// Output: 2
    /// </summary>
    public class PathWithMaximumMinimumValue
    {
        public interface ISolution
        {
            int MaximumMinimumPath(int[,] matrix);
        }

        /// <summary>
        /// 
        /// </summary>
        public class MySolution : ISolution
        {
            /// <summary>
            /// Custom comparer to compare nodes' scores
            /// </summary>
            class CustomeComparer : Comparer<(int X, int Y, int Score)>
            {
                /// <summary>
                /// Compare x and y
                /// </summary>
                /// <param name="x"></param>
                /// <param name="y"></param>
                /// <returns>
                /// - positive nunber of x > y
                /// - 0 if x == y
                /// - negetive numver if y > x
                /// </returns>
                public override int Compare((int X, int Y, int Score) x, (int X, int Y, int Score) y)
                {
                    return x.Score - y.Score;
                }
            }

            public int MaximumMinimumPath(int[,] matrix)
            {
                var xLength = matrix.GetLength(0);
                var yLength = matrix.GetLength(1);

                //declaration
                var directions = new List<(int X, int Y)> { (-1, 0), (1, 0), (0, -1), (0, 1) };
                var visited = new bool[xLength, yLength];
                var scores = new int[xLength, yLength];
                var possibleDestinations = new List<(int X, int Y, int Score)>();

                //initialization
                scores[0, 0] = matrix[0, 0];
                possibleDestinations.Add((0, 0, matrix[0, 0]));

                var comparer = new CustomeComparer();

                //computation
                while (possibleDestinations.Any())
                {
                    var origin = possibleDestinations.Poll(comparer);

                    if (origin.X == xLength - 1 && origin.Y == yLength - 1)
                        break;

                    foreach(var direction in directions)
                    {
                        var destinationX = origin.X + direction.X;
                        var destinationY = origin.Y + direction.Y;

                        //Skip if out of bound or visited
                        if (destinationX < 0 || destinationX > xLength - 1) continue;
                        if (destinationY < 0 || destinationY > yLength - 1) continue;
                        if (visited[destinationX, destinationY]) continue;

                        //score next point is the min value of (previous point's score, next point value)
                        scores[destinationX, destinationY] = Math.Min(scores[origin.X, origin.Y], matrix[destinationX, destinationY]); //<-- IMPORTANT
                        possibleDestinations.Add((destinationX, destinationY, scores[destinationX, destinationY]));
                    }

                    visited[origin.X, origin.Y] = true;
                }

                return scores[xLength - 1, yLength - 1];
            }
        }

        public class OtherPeopleSolution : ISolution
        {
            class PriorityQueue<T> : List<T>
            {
                private IComparer<T> Comparer { get; set; }
                public PriorityQueue(IComparer<T> comparer)
                {
                    Comparer = comparer;
                }

                public T Poll()
                {
                    var max = this.First();
                    var maxIndex = 0;
                    for (var i = 1; i < this.Count; i++)
                        if (Comparer.Compare(this[i], max) > 0)
                        {
                            max = this[i];
                            maxIndex = i;
                        }

                    this.RemoveAt(maxIndex);
                    return max;
                }
            }

            class CustomComparer : Comparer<int[]>
            {
                public override int Compare(int[] x, int[] y)
                {
                    if (x[2] != y[2])
                        return x[2] - y[2];
                    else if (x[0] != y[0])
                        return x[0] - y[0];
                    else
                        return x[1] - y[1];
                }
            }

            public int MaximumMinimumPath(int[,] A)
            {
                const int WHITE = 0;
                const int GRAY = 1;
                const int BLACK = 2;
                int rows = A.GetLength(0), columns = A.GetLength(1);

                var colors = new int[rows, columns];
                var values = new int[rows, columns];
                int[][] directions = { new int[] { -1, 0 }, new int[] { 1, 0 }, new int[] { 0, -1 }, new int[] { 0, 1 } };

                var priorityQueue = new PriorityQueue<int[]>(new CustomComparer());

                colors[0, 0] = GRAY;
                values[0, 0] = A[0, 0];
                priorityQueue.Add(new int[] { 0, 0, values[0, 0] });
                while (priorityQueue.Any())
                {
                    int[] cell = priorityQueue.Poll();
                    int row = cell[0], column = cell[1], value = cell[2];
                    foreach (var direction in directions)
                    {
                        int newRow = row + direction[0], newColumn = column + direction[1];
                        if (newRow >= 0 && newRow < rows && newColumn >= 0 && newColumn < columns && colors[newRow, newColumn] == WHITE)
                        {
                            colors[newRow, newColumn] = GRAY;
                            values[newRow, newColumn] = Math.Min(value, A[newRow, newColumn]);
                            priorityQueue.Add(new int[] { newRow, newColumn, values[newRow, newColumn] });
                        }
                    }
                    colors[row, column] = BLACK;
                }
                return values[rows - 1, columns - 1];
            }
        }
    }
}
